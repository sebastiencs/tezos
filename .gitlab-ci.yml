---

variables:
  ## Please update `scripts/version.sh` accordingly
  GIT_STRATEGY: fetch
  GIT_DEPTH: "1"
  GET_SOURCES_ATTEMPTS: "2"
  ARTIFACT_DOWNLOAD_ATTEMPTS: "2"

stages:
  - build
  - test
  - publish

############################################################
## Stage: build (only MR)                                 ##
############################################################

.build_template: &build_definition
  stage: build
  only:
    - tags
  tags:
    - gitlab-org

build:zcash-params:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:ubuntu20
  script:
    - make build-deps
    # zcash-params are generated here install_build_deps.rust.sh, by downloading from Tezos opam repository
    - cp _opam/share/zcash-params/sapling-spend.params sapling-spend.params
    - cp _opam/share/zcash-params/sapling-output.params sapling-output.params
    - sha256sum sapling-spend.params | cut -d " " -f 1 | xargs echo -n > sapling-spend.params.sha256
    - sha256sum sapling-output.params | cut -d " " -f 1 | xargs echo -n > sapling-output.params.sha256
  artifacts:
    paths:
      - sapling-spend.params
      - sapling-spend.params.sha256
      - sapling-output.params
      - sapling-output.params.sha256
    expire_in: 1 day

build:ubuntu16:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:ubuntu16
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-ubuntu16.so
    - gzip libtezos-ffi-ubuntu16.so
    - sha256sum libtezos-ffi-ubuntu16.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-ubuntu16.so.gz.sha256
    - mv tezos-client tezos-client-ubuntu16
    - gzip tezos-client-ubuntu16
    - sha256sum tezos-client-ubuntu16.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-ubuntu16.gz.sha256
    - mv tezos-admin-client tezos-admin-client-ubuntu16
    - gzip tezos-admin-client-ubuntu16
    - sha256sum tezos-admin-client-ubuntu16.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-ubuntu16.gz.sha256
    # ocaml headers
    - target_dir=$(pwd) && cd `opam config exec -- ocamlopt -where` && tar -czvf ocaml-headers.tar.gz caml && mv ocaml-headers.tar.gz $target_dir
    - cd $target_dir && mv ocaml-headers.tar.gz libtezos-ffi-headers.tar.gz
    - sha256sum libtezos-ffi-headers.tar.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-headers.tar.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-ubuntu16.so.gz
      - libtezos-ffi-ubuntu16.so.gz.sha256
      - tezos-client-ubuntu16.gz
      - tezos-client-ubuntu16.gz.sha256
      - tezos-admin-client-ubuntu16.gz
      - tezos-admin-client-ubuntu16.gz.sha256
      - libtezos-ffi-headers.tar.gz
      - libtezos-ffi-headers.tar.gz.sha256
    expire_in: 1 day

build:ubuntu18:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:ubuntu18
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-ubuntu18.so
    - gzip libtezos-ffi-ubuntu18.so
    - sha256sum libtezos-ffi-ubuntu18.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-ubuntu18.so.gz.sha256
    - mv tezos-client tezos-client-ubuntu18
    - gzip tezos-client-ubuntu18
    - sha256sum tezos-client-ubuntu18.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-ubuntu18.gz.sha256
    - mv tezos-admin-client tezos-admin-client-ubuntu18
    - gzip tezos-admin-client-ubuntu18
    - sha256sum tezos-admin-client-ubuntu18.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-ubuntu18.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-ubuntu18.so.gz
      - libtezos-ffi-ubuntu18.so.gz.sha256
      - tezos-client-ubuntu18.gz
      - tezos-client-ubuntu18.gz.sha256
      - tezos-admin-client-ubuntu18.gz
      - tezos-admin-client-ubuntu18.gz.sha256
    expire_in: 1 day

build:ubuntu19:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:ubuntu19
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-ubuntu19.so
    - gzip libtezos-ffi-ubuntu19.so
    - sha256sum libtezos-ffi-ubuntu19.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-ubuntu19.so.gz.sha256
    - mv tezos-client tezos-client-ubuntu19
    - gzip tezos-client-ubuntu19
    - sha256sum tezos-client-ubuntu19.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-ubuntu19.gz.sha256
    - mv tezos-admin-client tezos-admin-client-ubuntu19
    - gzip tezos-admin-client-ubuntu19
    - sha256sum tezos-admin-client-ubuntu19.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-ubuntu19.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-ubuntu19.so.gz
      - libtezos-ffi-ubuntu19.so.gz.sha256
      - tezos-client-ubuntu19.gz
      - tezos-client-ubuntu19.gz.sha256
      - tezos-admin-client-ubuntu19.gz
      - tezos-admin-client-ubuntu19.gz.sha256
    expire_in: 1 day

build:ubuntu20:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:ubuntu20
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-ubuntu20.so
    - gzip libtezos-ffi-ubuntu20.so
    - sha256sum libtezos-ffi-ubuntu20.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-ubuntu20.so.gz.sha256
    - mv tezos-client tezos-client-ubuntu20
    - gzip tezos-client-ubuntu20
    - sha256sum tezos-client-ubuntu20.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-ubuntu20.gz.sha256
    - mv tezos-admin-client tezos-admin-client-ubuntu20
    - gzip tezos-admin-client-ubuntu20
    - sha256sum tezos-admin-client-ubuntu20.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-ubuntu20.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-ubuntu20.so.gz
      - libtezos-ffi-ubuntu20.so.gz.sha256
      - tezos-client-ubuntu20.gz
      - tezos-client-ubuntu20.gz.sha256
      - tezos-admin-client-ubuntu20.gz
      - tezos-admin-client-ubuntu20.gz.sha256
    expire_in: 1 day

build:ubuntu21:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:ubuntu21
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-ubuntu21.so
    - gzip libtezos-ffi-ubuntu21.so
    - sha256sum libtezos-ffi-ubuntu21.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-ubuntu21.so.gz.sha256
    - mv tezos-client tezos-client-ubuntu21
    - gzip tezos-client-ubuntu21
    - sha256sum tezos-client-ubuntu21.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-ubuntu21.gz.sha256
    - mv tezos-admin-client tezos-admin-client-ubuntu21
    - gzip tezos-admin-client-ubuntu21
    - sha256sum tezos-admin-client-ubuntu21.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-ubuntu21.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-ubuntu21.so.gz
      - libtezos-ffi-ubuntu21.so.gz.sha256
      - tezos-client-ubuntu21.gz
      - tezos-client-ubuntu21.gz.sha256
      - tezos-admin-client-ubuntu21.gz
      - tezos-admin-client-ubuntu21.gz.sha256
    expire_in: 1 day

build:debian9:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:debian9
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-debian9.so
    - gzip libtezos-ffi-debian9.so
    - sha256sum libtezos-ffi-debian9.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-debian9.so.gz.sha256
    - mv tezos-client tezos-client-debian9
    - gzip tezos-client-debian9
    - sha256sum tezos-client-debian9.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-debian9.gz.sha256
    - mv tezos-admin-client tezos-admin-client-debian9
    - gzip tezos-admin-client-debian9
    - sha256sum tezos-admin-client-debian9.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-debian9.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-debian9.so.gz
      - libtezos-ffi-debian9.so.gz.sha256
      - tezos-client-debian9.gz
      - tezos-client-debian9.gz.sha256
      - tezos-admin-client-debian9.gz
      - tezos-admin-client-debian9.gz.sha256
    expire_in: 1 day

build:debian10:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:debian10
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-debian10.so
    - gzip libtezos-ffi-debian10.so
    - sha256sum libtezos-ffi-debian10.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-debian10.so.gz.sha256
    - mv tezos-client tezos-client-debian10
    - gzip tezos-client-debian10
    - sha256sum tezos-client-debian10.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-debian10.gz.sha256
    - mv tezos-admin-client tezos-admin-client-debian10
    - gzip tezos-admin-client-debian10
    - sha256sum tezos-admin-client-debian10.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-debian10.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-debian10.so.gz
      - libtezos-ffi-debian10.so.gz.sha256
      - tezos-client-debian10.gz
      - tezos-client-debian10.gz.sha256
      - tezos-admin-client-debian10.gz
      - tezos-admin-client-debian10.gz.sha256
    expire_in: 1 day

build:opensuse15.1:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:opensuse15.1
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-opensuse15.1.so
    - gzip libtezos-ffi-opensuse15.1.so
    - sha256sum libtezos-ffi-opensuse15.1.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-opensuse15.1.so.gz.sha256
    - mv tezos-client tezos-client-opensuse15.1
    - gzip tezos-client-opensuse15.1
    - sha256sum tezos-client-opensuse15.1.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-opensuse15.1.gz.sha256
    - mv tezos-admin-client tezos-admin-client-opensuse15.1
    - gzip tezos-admin-client-opensuse15.1
    - sha256sum tezos-admin-client-opensuse15.1.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-opensuse15.1.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-opensuse15.1.so.gz
      - libtezos-ffi-opensuse15.1.so.gz.sha256
      - tezos-client-opensuse15.1.gz
      - tezos-client-opensuse15.1.gz.sha256
      - tezos-admin-client-opensuse15.1.gz
      - tezos-admin-client-opensuse15.1.gz.sha256
    expire_in: 1 day

#build:opensuse_tumbleweed:
#  <<: *build_definition
#  image: tezedge/tezos-opam-builder:opensuse_tumbleweed
#  script:
#    - make build-deps
#    - opam config exec -- make tezedge-ffi-release
#    - mv libtezos-ffi.so libtezos-ffi-opensuse_tumbleweed.so
#    - gzip libtezos-ffi-opensuse_tumbleweed.so
#    - sha256sum libtezos-ffi-opensuse_tumbleweed.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-opensuse_tumbleweed.so.gz.sha256
#    - mv tezos-client tezos-client-opensuse_tumbleweed
#    - gzip tezos-client-opensuse_tumbleweed
#    - sha256sum tezos-client-opensuse_tumbleweed.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-opensuse_tumbleweed.gz.sha256
#    - mv tezos-admin-client tezos-admin-client-opensuse_tumbleweed
#    - gzip tezos-admin-client-opensuse_tumbleweed
#    - sha256sum tezos-admin-client-opensuse_tumbleweed.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-opensuse_tumbleweed.gz.sha256
#  artifacts:
#    paths:
#      - libtezos-ffi-opensuse_tumbleweed.so.gz
#      - libtezos-ffi-opensuse_tumbleweed.so.gz.sha256
#      - tezos-client-opensuse_tumbleweed.gz
#      - tezos-client-opensuse_tumbleweed.gz.sha256
#      - tezos-admin-client-opensuse_tumbleweed.gz
#      - tezos-admin-client-opensuse_tumbleweed.gz.sha256
#    expire_in: 1 day

#build:centos7:
#  <<: *build_definition
#  image: tezedge/tezos-opam-builder:centos7
#  script:
#    - make build-deps
#    - opam config exec -- make tezedge-ffi-release
#    - mv libtezos-ffi.so libtezos-ffi-centos7.so
#    - gzip libtezos-ffi-centos7.so
#    - sha256sum libtezos-ffi-centos7.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-centos7.so.gz.sha256
#    - mv tezos-client tezos-client-centos7
#    - gzip tezos-client-centos7
#    - sha256sum tezos-client-centos7.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-centos7.gz.sha256
#    - mv tezos-admin-client tezos-admin-client-centos7
#    - gzip tezos-admin-client-centos7
#    - sha256sum tezos-admin-client-centos7.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-centos7.gz.sha256
#  artifacts:
#    paths:
#      - libtezos-ffi-centos7.so.gz
#      - libtezos-ffi-centos7.so.gz.sha256
#      - tezos-client-centos7.gz
#      - tezos-client-centos7.gz.sha256
#      - tezos-admin-client-centos7.gz
#      - tezos-admin-client-centos7.gz.sha256
#    expire_in: 1 day

build:centos8:
  <<: *build_definition
  image: tezedge/tezos-opam-builder:centos8
  script:
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - mv libtezos-ffi.so libtezos-ffi-centos8.so
    - gzip libtezos-ffi-centos8.so
    - sha256sum libtezos-ffi-centos8.so.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-centos8.so.gz.sha256
    - mv tezos-client tezos-client-centos8
    - gzip tezos-client-centos8
    - sha256sum tezos-client-centos8.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-centos8.gz.sha256
    - mv tezos-admin-client tezos-admin-client-centos8
    - gzip tezos-admin-client-centos8
    - sha256sum tezos-admin-client-centos8.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-centos8.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-centos8.so.gz
      - libtezos-ffi-centos8.so.gz.sha256
      - tezos-client-centos8.gz
      - tezos-client-centos8.gz.sha256
      - tezos-admin-client-centos8.gz
      - tezos-admin-client-centos8.gz.sha256
    expire_in: 1 day

build:macos:
  stage: build
  only:
    - tags
  tags:
    - macos
  variables:
    MACOSX_DEPLOYMENT_TARGET: "10.13"
  script:
    - export CFLAGS="-Wno-missing-braces" # so that bls12-381-unix doesn't fail to install
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps
    - opam config exec -- make tezedge-ffi-release
    - otool -L libtezos-ffi.so
    - mv libtezos-ffi.so libtezos-ffi-macos.dylib
    - gzip libtezos-ffi-macos.dylib
    - shasum -a256 libtezos-ffi-macos.dylib.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-macos.dylib.gz.sha256
    - mv tezos-client tezos-client-macos
    - gzip tezos-client-macos
    - shasum -a256 tezos-client-macos.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-macos.gz.sha256
    - mv tezos-admin-client tezos-admin-client-macos
    - gzip tezos-admin-client-macos
    - shasum -a256 tezos-admin-client-macos.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-macos.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-macos.dylib.gz
      - libtezos-ffi-macos.dylib.gz.sha256
      - tezos-client-macos.gz
      - tezos-client-macos.gz.sha256
      - tezos-admin-client-macos.gz
      - tezos-admin-client-macos.gz.sha256
    expire_in: 1 day

build:macos-m1:
  stage: build
  only:
    - tags
  tags:
    - macos-m1
  variables:
    MACOSX_DEPLOYMENT_TARGET: "11.0"
  script:
    - export CFLAGS="-Wno-missing-braces" # so that bls12-381-unix doesn't fail to install
    - export BLST_PORTABLE=yes # To compile without ADX
    - make build-deps || true; opam install conf-rust --assume-depexts; make build-deps
    - opam config exec -- make tezedge-ffi-release
    - otool -L libtezos-ffi.so
    - mv libtezos-ffi.so libtezos-ffi-macos-m1.dylib
    - gzip libtezos-ffi-macos-m1.dylib
    - shasum -a256 libtezos-ffi-macos-m1.dylib.gz | cut -d " " -f 1 | xargs echo -n > libtezos-ffi-macos-m1.dylib.gz.sha256
    - mv tezos-client tezos-client-macos-m1
    - gzip tezos-client-macos-m1
    - shasum -a256 tezos-client-macos-m1.gz | cut -d " " -f 1 | xargs echo -n > tezos-client-macos-m1.gz.sha256
    - mv tezos-admin-client tezos-admin-client-macos-m1
    - gzip tezos-admin-client-macos-m1
    - shasum -a256 tezos-admin-client-macos-m1.gz | cut -d " " -f 1 | xargs echo -n > tezos-admin-client-macos-m1.gz.sha256
  artifacts:
    paths:
      - libtezos-ffi-macos-m1.dylib.gz
      - libtezos-ffi-macos-m1.dylib.gz.sha256
      - tezos-client-macos-m1.gz
      - tezos-client-macos-m1.gz.sha256
      - tezos-admin-client-macos-m1.gz
      - tezos-admin-client-macos-m1.gz.sha256
    expire_in: 1 day

############################################################
## Stage: publish                                         ##
############################################################

publish:tezos-ffi-release:
  stage: publish
  only:
    - tags
  image: cfmanteiga/alpine-bash-curl-jq
  script:
    - ./scripts/ci/gitlab_create_release.sh "Tezos FFI $CI_COMMIT_TAG" "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "./docs/releases/description.md" "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "sapling-spend.params"     "./sapling-spend.params"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "sapling-spend.params.sha256"     "./sapling-spend.params.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "sapling-output.params"     "./sapling-output.params"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "sapling-output.params.sha256"     "./sapling-output.params.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu16.so.gz"     "./libtezos-ffi-ubuntu16.so.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu16.so.gz.sha256"     "./libtezos-ffi-ubuntu16.so.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu16.gz"     "./tezos-client-ubuntu16.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu16.gz.sha256"     "./tezos-client-ubuntu16.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu16.gz"     "./tezos-admin-client-ubuntu16.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu16.gz.sha256"     "./tezos-admin-client-ubuntu16.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-headers.tar.gz"     "./libtezos-ffi-headers.tar.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-headers.tar.gz.sha256"     "./libtezos-ffi-headers.tar.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu18.so.gz"     "./libtezos-ffi-ubuntu18.so.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu18.so.gz.sha256"     "./libtezos-ffi-ubuntu18.so.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu18.gz"     "./tezos-client-ubuntu18.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu18.gz.sha256"     "./tezos-client-ubuntu18.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu18.gz"     "./tezos-admin-client-ubuntu18.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu18.gz.sha256"     "./tezos-admin-client-ubuntu18.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu19.so.gz"     "./libtezos-ffi-ubuntu19.so.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu19.so.gz.sha256"     "./libtezos-ffi-ubuntu19.so.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu19.gz"     "./tezos-client-ubuntu19.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu19.gz.sha256"     "./tezos-client-ubuntu19.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu19.gz"     "./tezos-admin-client-ubuntu19.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu19.gz.sha256"     "./tezos-admin-client-ubuntu19.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu20.so.gz"     "./libtezos-ffi-ubuntu20.so.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu20.so.gz.sha256"     "./libtezos-ffi-ubuntu20.so.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu20.gz"     "./tezos-client-ubuntu20.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu20.gz.sha256"     "./tezos-client-ubuntu20.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu20.gz"     "./tezos-admin-client-ubuntu20.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu20.gz.sha256"     "./tezos-admin-client-ubuntu20.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu21.so.gz"     "./libtezos-ffi-ubuntu21.so.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-ubuntu21.so.gz.sha256"     "./libtezos-ffi-ubuntu21.so.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu21.gz"     "./tezos-client-ubuntu21.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-ubuntu21.gz.sha256"     "./tezos-client-ubuntu21.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu21.gz"     "./tezos-admin-client-ubuntu21.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-ubuntu21.gz.sha256"     "./tezos-admin-client-ubuntu21.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-debian9.so.gz"      "./libtezos-ffi-debian9.so.gz"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-debian9.so.gz.sha256"      "./libtezos-ffi-debian9.so.gz.sha256"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-debian9.gz"     "./tezos-client-debian9.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-debian9.gz.sha256"     "./tezos-client-debian9.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-debian9.gz"     "./tezos-admin-client-debian9.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-debian9.gz.sha256"     "./tezos-admin-client-debian9.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-debian10.so.gz"     "./libtezos-ffi-debian10.so.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-debian10.so.gz.sha256"     "./libtezos-ffi-debian10.so.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-debian10.gz"     "./tezos-client-debian10.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-debian10.gz.sha256"     "./tezos-client-debian10.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-debian10.gz"     "./tezos-admin-client-debian10.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-debian10.gz.sha256"     "./tezos-admin-client-debian10.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-opensuse15.1.so.gz" "./libtezos-ffi-opensuse15.1.so.gz" "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-opensuse15.1.so.gz.sha256" "./libtezos-ffi-opensuse15.1.so.gz.sha256" "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-opensuse15.1.gz"     "./tezos-client-opensuse15.1.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-opensuse15.1.gz.sha256"     "./tezos-client-opensuse15.1.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-opensuse15.1.gz"     "./tezos-admin-client-opensuse15.1.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-opensuse15.1.gz.sha256"     "./tezos-admin-client-opensuse15.1.gz.sha256"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-opensuse_tumbleweed.so.gz" "./libtezos-ffi-opensuse_tumbleweed.so.gz" "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-opensuse_tumbleweed.so.gz.sha256" "./libtezos-ffi-opensuse_tumbleweed.so.gz.sha256" "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-opensuse_tumbleweed.gz"     "./tezos-client-opensuse_tumbleweed.gz"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-opensuse_tumbleweed.gz.sha256"     "./tezos-client-opensuse_tumbleweed.gz.sha256"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-opensuse_tumbleweed.gz"     "./tezos-admin-client-opensuse_tumbleweed.gz"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-opensuse_tumbleweed.gz.sha256"     "./tezos-admin-client-opensuse_tumbleweed.gz.sha256"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-centos7.so.gz"      "./libtezos-ffi-centos7.so.gz"      "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-centos7.so.gz.sha256"      "./libtezos-ffi-centos7.so.gz.sha256"      "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-centos7.gz"     "./tezos-client-centos7.gz"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-centos7.gz.sha256"     "./tezos-client-centos7.gz.sha256"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-centos7.gz"     "./tezos-admin-client-centos7.gz"     "$PRIVATE_TOKEN"
#    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-centos7.gz.sha256"     "./tezos-admin-client-centos7.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-centos8.so.gz"      "./libtezos-ffi-centos8.so.gz"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-centos8.so.gz.sha256"      "./libtezos-ffi-centos8.so.gz.sha256"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-centos8.gz"     "./tezos-client-centos8.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-centos8.gz.sha256"     "./tezos-client-centos8.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-centos8.gz"     "./tezos-admin-client-centos8.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-centos8.gz.sha256"     "./tezos-admin-client-centos8.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-macos.dylib.gz"      "./libtezos-ffi-macos.dylib.gz"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-macos.dylib.gz.sha256"      "./libtezos-ffi-macos.dylib.gz.sha256"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-macos.gz"     "./tezos-client-macos.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-macos.gz.sha256"     "./tezos-client-macos.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-macos.gz"     "./tezos-admin-client-macos.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-macos.gz.sha256"     "./tezos-admin-client-macos.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-macos-m1.dylib.gz"      "./libtezos-ffi-macos-m1.dylib.gz"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "libtezos-ffi-macos-m1.dylib.gz.sha256"      "./libtezos-ffi-macos-m1.dylib.gz.sha256"      "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-macos-m1.gz"     "./tezos-client-macos-m1.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-client-macos-m1.gz.sha256"     "./tezos-client-macos-m1.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-macos-m1.gz"     "./tezos-admin-client-macos-m1.gz"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "tezos-admin-client-macos-m1.gz.sha256"     "./tezos-admin-client-macos-m1.gz.sha256"     "$PRIVATE_TOKEN"
    - ./scripts/ci/gitlab_upload_release_file_summary.sh "$CI_COMMIT_TAG" "$CI_PROJECT_ID" "$CI_PROJECT_URL" "$PRIVATE_TOKEN" "120"
  tags:
    - gitlab-org
#!/usr/bin/env bash
TAG_NAME="$1"
PROJECT_ID="$2"
PROJECT_URL="$3"
PRIVATE_TOKEN="$4"
ARTIFACTS_SUMMARY_FILE_NAME="libtezos-ffi-distribution-summary.json"
ARTIFACTS_SUMMARY_FILE_PATH="./libtezos-ffi-distribution-summary.json"
RELEASE_FILE_NAME=$ARTIFACTS_SUMMARY_FILE_NAME
ITEMS_PER_PAGE="$5"

if [ "$4" == "" ]; then
    echo "Missing parameter! Parameters are TAG_NAME, PROJECT_ID, PROJECT_URL and PRIVATE_TOKEN.";
    exit 1;
fi

# Download list of artifacts as summary file
ARTIFACTS_SUMMARY_JSON=$(curl --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases/${TAG_NAME}/assets/links?per_page=${ITEMS_PER_PAGE}")
echo -n $ARTIFACTS_SUMMARY_JSON | jq ". |= sort_by(.name) |  map(.git_tag |= \"$TAG_NAME\")" > $ARTIFACTS_SUMMARY_FILE_NAME

# Upload release summary file
RELEASE_FILE_URL=$(curl --request POST --header "Private-Token: ${PRIVATE_TOKEN}" --form "file=@${ARTIFACTS_SUMMARY_FILE_PATH}" "https://gitlab.com/api/v4/projects/${PROJECT_ID}/uploads" | jq -jr ".url")
RELEASE_FILE_DOWNLOAD_LINK="${PROJECT_URL}${RELEASE_FILE_URL}"
echo "Release artifacts summary file download link -- $RELEASE_FILE_DOWNLOAD_LINK"

curl --request POST \
     --header "Private-Token: ${PRIVATE_TOKEN}" \
     --data name="${RELEASE_FILE_NAME}" \
     --data url="${RELEASE_FILE_DOWNLOAD_LINK}" \
     "https://gitlab.com/api/v4/projects/${PROJECT_ID}/releases/${TAG_NAME}/assets/links"

exit 0

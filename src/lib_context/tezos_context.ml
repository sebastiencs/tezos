module Context = Tezos_tezedge_context.Xcontext
module Irmin_context = Tezos_tezedge_context.Icontext
module Tezedge_context = Tezos_tezedge_context.Tcontext
module Context_dump = Tezos_context_original.Context_dump

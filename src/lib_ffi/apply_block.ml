open Tezos_base__TzPervasives
open Tezos_protocol_updater
open Tezos_shell_services
open Ffi_errors
open Ffi_logger
open Ffi_utils

type apply_block_request = {
  chain_id : Chain_id.t;
  block_header : Block_header.t;
  pred_header : Block_header.t;
  max_operations_ttl : int;
  operations : Operation.t list list;
  (* context associated to the predecessor block *)
  predecessor_block_metadata_hash : Block_metadata_hash.t option;
  (* hash of block header metadata of the predecessor block *)
  predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option;
}

type forking_testchain_data = {
  forking_block_hash : Block_hash.t;
  test_chain_id : Chain_id.t;
}

let forking_testchain_data_encoding =
  let open Data_encoding in
  def
    "forking_testchain_data_encoding"
    ~title:"forking_testchain_data_encoding"
    ~description:"forking_testchain_data_encoding"
  @@ conv
       (fun {forking_block_hash; test_chain_id} ->
         (forking_block_hash, test_chain_id))
       (fun (forking_block_hash, test_chain_id) ->
         {forking_block_hash; test_chain_id})
       (obj2
          (req "forking_block_hash" Block_hash.encoding)
          (req "test_chain_id" Chain_id.encoding))

(** Frozen snapshot of roll owners for a given cycle *)
type cycle_rolls_owner_snapshot = {
  cycle : int;  (** cycle for which this data belongs *)
  seed_bytes : bytes;  (** randomness seed byte encoded *)
  rolls_data : (bytes * int list) list;
      (** snapshot data as a list of (public key * slots),
          with public key encoded in binary form *)
  last_roll : int32;  (** last roll number *)
}

type apply_block_timestamps = {
  mutable apply_start_t : float;
  mutable operations_decoding_start_t : float;
  mutable operations_decoding_end_t : float;
  mutable operations_application_timestamps : (float * float) list list;
  mutable operations_metadata_encoding_start_t : float;
  mutable operations_metadata_encoding_end_t : float;
  mutable begin_application_start_t : float;
  mutable begin_application_end_t : float;
  mutable finalize_block_start_t : float;
  mutable finalize_block_end_t : float;
  mutable collect_new_rolls_owner_snapshots_start_t : float;
  mutable collect_new_rolls_owner_snapshots_end_t : float;
  mutable commit_start_t : float;
  mutable commit_end_t : float;
  mutable apply_end_t : float;
}

let empty_apply_block_timestamps () =
  {
    apply_start_t = 0.0;
    operations_decoding_start_t = 0.0;
    operations_decoding_end_t = 0.0;
    operations_application_timestamps = [];
    operations_metadata_encoding_start_t = 0.0;
    operations_metadata_encoding_end_t = 0.0;
    begin_application_start_t = 0.0;
    begin_application_end_t = 0.0;
    finalize_block_start_t = 0.0;
    finalize_block_end_t = 0.0;
    collect_new_rolls_owner_snapshots_start_t = 0.0;
    collect_new_rolls_owner_snapshots_end_t = 0.0;
    commit_start_t = 0.0;
    commit_end_t = 0.0;
    apply_end_t = 0.0;
  }

type apply_block_response = {
  validation_result_message : string;
  context_hash : Context_hash.t;
  protocol_hash : Protocol_hash.t;
  next_protocol_hash : Protocol_hash.t;
  block_header_proto_json : string;
  block_header_metadata_bytes : bytes;
  operations_metadata_bytes : bytes list list;
  max_operations_ttl : int;
  last_allowed_fork_level : int32;
  forking_testchain : bool;
  forking_testchain_data : forking_testchain_data option;
  block_metadata_hash : Block_metadata_hash.t option;
  ops_metadata_hashes : Operation_metadata_hash.t list list option;
  (* Note: this is calculated from ops_metadata_hashes - we need this in request *)
  ops_metadata_hash : Operation_metadata_list_list_hash.t option;
  cycle_rolls_owner_snapshots : cycle_rolls_owner_snapshot list;
  new_protocol_constants_json : string option;
  new_cycle_eras_json : string option;
  commit_time : float;
  execution_timestamps : apply_block_timestamps;
}

type apply_block_result = {
  response : apply_block_response;
  cache : Environment_context.Context.cache;
}

(* TODO - TE-210: temporary solution until checkpoints have been implemented *)
(* Since we cannot access the cycle position directly from the header,
   the solution is to use the encoding definition to conver it into a
   JSON representation and get the value from there *)
let extract_level_info block_metadata_json field =
  try
    Some
      (Json_query.query [`Field "level_info"; `Field field] block_metadata_json)
  with Not_found -> (
    try
      Some (Json_query.query [`Field "level"; `Field field] block_metadata_json)
    with Not_found -> None)

(* Extracts current cycle position from block metadata *)
let extract_cycle_position encoding block_metadata =
  let block_metadata_json =
    Data_encoding.Json.construct encoding block_metadata
  in
  match extract_level_info block_metadata_json "cycle_position" with
  | Some (`Float n) -> Some (int_of_float n)
  | _ -> None

(* Extracts current cycle from block metadata *)
let extract_cycle encoding block_metadata =
  let block_metadata_json =
    Data_encoding.Json.construct encoding block_metadata
  in
  match extract_level_info block_metadata_json "cycle" with
  | Some (`Float n) -> Some (int_of_float n)
  | _ -> None

(* Decodes a roll as an int32 for the specified protocol *)
let protocol_roll_to_int32
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) roll =
  let open Data_encoding.Binary in
  match Tezos_crypto.Protocol_hash.to_b58check Proto.hash with
  | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY" ->
      return
        Tezos_protocol_001_PtCJ7pwo.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt" ->
      return
        Tezos_protocol_002_PsYLVpVv.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP" ->
      return
        Tezos_protocol_003_PsddFKi3.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd" ->
      return
        Tezos_protocol_004_Pt24m4xi.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU" ->
      return
        Tezos_protocol_005_PsBABY5H.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS" ->
      return
        Tezos_protocol_005_PsBabyM1.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb" ->
      return
        Tezos_protocol_006_PsCARTHA.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo" ->
      return
        Tezos_protocol_007_PsDELPH1.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq" ->
      return
        Tezos_protocol_008_PtEdoTez.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA" ->
      return
        Tezos_protocol_008_PtEdo2Zk.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i" ->
      return
        Tezos_protocol_009_PsFLoren.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
      return
        Tezos_protocol_010_PtGRANAD.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx" ->
      return
        Tezos_protocol_011_PtHangz2.Protocol.Roll_repr.(
          of_bytes_exn encoding roll |> to_int32)
  | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A" ->
      (* No rolls after proto 011 *)
      return (Int32.of_int 0)
  | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
      return (Int32.of_int 0)
  | other -> failwith ("Protocol not supported: " ^ other)

let accumulate_rolls_data depth key tree acc =
  match (key, depth) with
  | ([roll_num], 1) | ([_; _; roll_num], 3) -> (
      Context.Tree.to_value tree >>= function
      | None -> Lwt.return acc
      | Some value ->
          (* FIXME: what if it cannot be parsed? *)
          let roll_num = int_of_string roll_num in
          let public_key =
            Data_encoding.Binary.of_bytes_exn
              Signature.Public_key.encoding
              value
          in
          Lwt.return ((public_key, roll_num) :: acc))
  | _ -> Lwt.return acc

(* Group keys with the roll nums assigned to them so that:
   [(pk1, num1); (pk2, num3); ...] results in:
   [(pk, [num1; num2; ...]); (pk2, [...]); ...] *)
let group_rolls_data rolls_data =
  let compare (lkey, _) (rkey, _) = Signature.Public_key.compare lkey rkey in
  let sorted = List.fast_sort compare rolls_data in
  let group_adjacent acc data =
    match (acc, data) with
    | ([], (next_pk, num)) -> [(next_pk, [num])]
    | ((curr_pk, curr_nums) :: acc, (next_pk, num)) ->
        if Signature.Public_key.equal curr_pk next_pk then
          (curr_pk, num :: curr_nums) :: acc
        else (next_pk, [num]) :: (curr_pk, curr_nums) :: acc
  in
  List.fold_left group_adjacent [] sorted

let unwrap_value name value =
  match value with
  | Some x -> return x
  | None -> failwith ("no value in: " ^ name)

(* Before protocol H 3 nesting levels were used, in H the context flattening
   happened and now the immediate children are the values we want *)
let roll_snapshots_depth
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) =
  match Protocol_hash.to_b58check Proto.hash with
  | "Ps6mwMrF2ER2s51cp9yYpjDcuzQjsc2yAz8bQsRgdaRxw4Fk95H"
  | "PtBMwNZT94N7gXKw4i273CKcSaBrrBnqnt3RATExNKr9KNX2USV"
  | "PtYuensgYBb3G3x1hLLbCmcav8ue8Kyd2khADcL5LsT5R1hcXex"
  | "ProtoGenesisGenesisGenesisGenesisGenesisGenesk612im"
  | "Ps9mPmXaRzmzk35gbAYNCAw6UXdE2qoABTHbN2oEEc1qM7CwT9P"
  | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
  | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
  | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
  | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
  | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
  | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
  | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
  | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
  | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
  | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
  | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i"
  | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
      3
  | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx"
  | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A"
  | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
      1
  | other ->
      Printf.eprintf
        "[WARNING] `roll_snapshots_depth` is not aware of protocol %s, \
         defaulting to 3\n\
         %!"
        other ;
      3
(* TODO: should default to 1, because it is what future protocols will use *)

let collect_rolls_data_for_cycle
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context cycle
    =
  let cycle_key = string_of_int cycle in
  Context.find context ["cycle"; cycle_key; "roll_snapshot"]
  >>= unwrap_value "roll_snapshot"
  >>=? fun roll_snapshot ->
  Context.find context ["cycle"; cycle_key; "random_seed"]
  >>= unwrap_value "random_seed"
  >>=? fun random_seed ->
  (* FIXME: what if the encoding changes? can that happen? *)
  let roll_snapshot =
    Data_encoding.Binary.of_bytes_exn Data_encoding.uint16 roll_snapshot
  in
  Context.find
    context
    ["cycle"; cycle_key; "last_roll"; string_of_int roll_snapshot]
  >>= unwrap_value "last_roll"
  >>=? fun last_roll ->
  protocol_roll_to_int32 (module Proto) last_roll >>=? fun last_roll ->
  let depth = roll_snapshots_depth (module Proto) in
  Context.fold
    context
    ["rolls"; "owner"; "snapshot"; cycle_key; string_of_int roll_snapshot]
    ~order:`Undefined
    ~depth:(`Eq depth)
    ~init:[]
    ~f:(accumulate_rolls_data depth)
  >>= fun rolls_data ->
  let rolls_data = group_rolls_data rolls_data in
  let rolls_data =
    List.map
      (fun (pk, data) ->
        ( Data_encoding.Binary.to_bytes_exn Signature.Public_key.encoding pk,
          data ))
      rolls_data
  in
  return {cycle; seed_bytes = random_seed; rolls_data; last_roll}

let protocol_constants_json
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context =
  let of_bytes_exn = Data_encoding.Binary.of_bytes_exn in
  let construct_json = Data_encoding.Json.construct in
  match Tezos_crypto.Protocol_hash.to_b58check Proto.hash with
  | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY" ->
      let open Tezos_protocol_001_PtCJ7pwo.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Parameters_repr.constants_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt" ->
      let open Tezos_protocol_002_PsYLVpVv.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Parameters_repr.constants_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP" ->
      let open Tezos_protocol_003_PsddFKi3.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Parameters_repr.constants_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd" ->
      let open Tezos_protocol_004_Pt24m4xi.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Parameters_repr.constants_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU" ->
      let open Tezos_protocol_005_PsBABY5H.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS" ->
      let open Tezos_protocol_005_PsBabyM1.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb" ->
      let open Tezos_protocol_006_PsCARTHA.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo" ->
      let open Tezos_protocol_007_PsDELPH1.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq" ->
      let open Tezos_protocol_008_PtEdoTez.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA" ->
      let open Tezos_protocol_008_PtEdo2Zk.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i" ->
      let open Tezos_protocol_009_PsFLoren.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
      let open Tezos_protocol_010_PtGRANAD.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.{fixed; parametric} in
      return @@ construct_json Constants_repr.encoding constants
  | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx" ->
      let open Tezos_protocol_011_PtHangz2.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.all parametric in
      return @@ construct_json Constants_repr.encoding constants
  | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A" ->
      let open Tezos_protocol_012_Psithaca.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.all parametric in
      return @@ construct_json Constants_repr.encoding constants
  | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
      let open Tezos_protocol_alpha.Protocol in
      Context.find context ["v1"; "constants"] >>= unwrap_value "constants"
      >>=? fun constants ->
      let parametric =
        of_bytes_exn Constants_repr.parametric_encoding constants
      in
      let constants = Constants_repr.all parametric in
      return @@ construct_json Constants_repr.encoding constants
  | other -> failwith ("Protocol not supported: " ^ other)

let protocol_cycle_eras_json
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context =
  let of_bytes_exn = Data_encoding.Binary.of_bytes_exn in
  let construct_json = Data_encoding.Json.construct in
  match Tezos_crypto.Protocol_hash.to_b58check Proto.hash with
  | "PtCJ7pwoxe8JasnHY8YonnLYjcVHmhiARPJvqcC6VfHT5s8k8sY"
  | "PsYLVpVvgbLhAhoqAkMFUo6gudkJ9weNXhUYCiLDzcUpFpkk8Wt"
  | "PsddFKi32cMJ2qPjf43Qv5GDWLDPZb3T3bF6fLKiF5HtvHNU7aP"
  | "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd"
  | "PsBABY5HQTSkA4297zNHfsZNKtxULfL18y95qb3m53QJiXGmrbU"
  | "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS"
  | "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb"
  | "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo"
  | "PtEdoTezd3RHSC31mpxxo1npxFjoWWcFgQtxapi51Z8TLu6v6Uq"
  | "PtEdo2ZkT9oKpimTah6x2embF25oss54njMuPzkJTEi5RqfdZFA"
  | "PsFLorenaUUuikDWvMDr6fGBRG8kt3e3D3fHoXK1j1BFRxeSH4i" ->
      return_none
  | "PtGRANADsDU8R9daYKAgWnQYAJ64omN1o3KMGVCykShA97vQbvV" ->
      let open Tezos_protocol_010_PtGRANAD.Protocol in
      Context.find context ["v1"; "cycle_eras"] >>= unwrap_value "cycle_eras"
      >>=? fun cycle_eras ->
      let cycle_eras = of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras in
      return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
  | "PtHangz2aRngywmSRGGvrcTyMbbdpWdpFKuS4uMWxg2RaH9i1qx" ->
      let open Tezos_protocol_011_PtHangz2.Protocol in
      Context.find context ["v1"; "cycle_eras"] >>= unwrap_value "cycle_eras"
      >>=? fun cycle_eras ->
      let cycle_eras = of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras in
      return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
  | "Psithaca2MLRFYargivpo7YvUr7wUDqyxrdhC5CQq78mRvimz6A" ->
      let open Tezos_protocol_012_Psithaca.Protocol in
      Context.find context ["v1"; "cycle_eras"] >>= unwrap_value "cycle_eras"
      >>=? fun cycle_eras ->
      let cycle_eras = of_bytes_exn Level_repr.cycle_eras_encoding cycle_eras in
      return_some @@ construct_json Level_repr.cycle_eras_encoding cycle_eras
  | "ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK" ->
      (* TODO - TE-706: should come from alpha, but somehow it is not up to date *)
      return_none
  | other -> failwith ("Protocol not supported: " ^ other)

let protocol_preserved_cycles
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context =
  protocol_constants_json (module Proto) context >|=? fun constants_json ->
  Ezjsonm.get_int (Json_query.query [`Field "preserved_cycles"] constants_json)

let rec ( --> ) i j =
  (* [i; i+1; ...; j] *)
  if Compare.Int.(i > j) then [] else i :: (succ i --> j)

let rec collect_cycles_rolls_data
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context cycles
    acc =
  match cycles with
  | [] -> return acc
  | cycle :: cycles ->
      collect_rolls_data_for_cycle (module Proto) context cycle >>=? fun data ->
      collect_cycles_rolls_data
        (module Proto : Tezos_protocol_updater.Registered_protocol.T)
        context
        cycles
        (data :: acc)

let protocols_without_rolls = [Tezos_protocol_012_Psithaca.Protocol.hash]

(* Startinug with 012 Ithaca, rolls data in snapshots has been substituted by stakes data *)
let protocol_has_rolls protocol_hash =
  not
  @@ List.mem ~equal:Protocol_hash.equal protocol_hash protocols_without_rolls

let collect_new_rolls_owner_snapshots
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context
    next_cycle next_cycle_position level =
  match (next_cycle, next_cycle_position, level) with
  | _ when not (protocol_has_rolls Proto.hash) -> return []
  | (Some next_cycle, Some 0, _) ->
      (* Only at position 0 of a cycle we collect the new data *)
      protocol_preserved_cycles (module Proto) context
      >>=? fun preserved_cycles ->
      let new_frozen_cycle = next_cycle + preserved_cycles in
      collect_rolls_data_for_cycle (module Proto) context new_frozen_cycle
      >>=? fun data -> return [data]
  | (_, _, 1l) ->
      (* Special handling for level=1 to collect the data computed during storage init *)
      protocol_preserved_cycles (module Proto) context
      >>=? fun preserved_cycles ->
      collect_cycles_rolls_data
        (module Proto)
        context
        (0 --> preserved_cycles)
        []
      >>=? fun data -> return data
  | (_, None, level) when level > 1l ->
      Printf.eprintf
        "WARNING: cannot collect rolls without knowing the cycle position\n%!" ;
      return []
  | _ -> return []

let collect_protocol_constants
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context =
  protocol_constants_json (module Proto) context >|=? fun constants_json ->
  Data_encoding.Json.to_string constants_json

let collect_protocol_cycle_eras_json
    (module Proto : Tezos_protocol_updater.Registered_protocol.T) context =
  protocol_cycle_eras_json (module Proto) context >|=? fun cycle_eras_json ->
  Option.map Data_encoding.Json.to_string cycle_eras_json

(* Make sure that the resulting context hash will be the expected one.
   If not, there may be a problem with the cache and we have to signal
   this mismatch so that the cache can be discarded and block application
   retried. *)
let assert_result_context_hash context validation_result block_header =
  let context_hash =
    Context.hash
      ~time:block_header.Block_header.shell.timestamp
      ?message:validation_result.Environment_context.message
      context
  in
  if Context_hash.equal context_hash block_header.shell.context then return_unit
  else
    fail
      (Context_hash_result_mismatch
         {
           actual = Context_hash.to_b58check context_hash;
           expected = Context_hash.to_b58check block_header.shell.context;
           cache = Option.is_some !Protocol_cache.cache;
         })

let apply_and_commit ~cache (context_index : Context.index)
    (chain_id : Chain_id.t) (block_header_hash : Block_hash.t)
    (block_header : Block_header.t) (operations : Operation.t list list)
    (predecessor_block_header : Block_header.t) (max_operations_ttl : int)
    (enable_testchain : bool)
    (predecessor_block_metadata_hash : Block_metadata_hash.t option)
    (predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option)
    =
  let execution_timestamps = empty_apply_block_timestamps () in
  execution_timestamps.apply_start_t <- Unix.gettimeofday () ;
  let predecessor_context_hash = predecessor_block_header.shell.context in
  Context.Timings.mark_block (Some block_header_hash) ;
  (* checkout predecesor context and get protocol from predecessor's context *)
  (Context.checkout context_index predecessor_context_hash >>= function
   | Some predecessor_block_context -> return predecessor_block_context
   | None ->
       fail
         (Unknown_predecessor_context
            (Context_hash.to_b58check predecessor_context_hash)))
  >>=? fun predecessor_block_context ->
  Context.get_protocol predecessor_block_context
  >>= fun protocol_hash_from_context ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Apply_block] Resolved predecessor protocol from context: %s"
        (Protocol_hash.to_b58check protocol_hash_from_context)) ;
  (* protocol instance *)
  (match Registered_protocol.get protocol_hash_from_context with
  | None ->
      fail
        (Apply_error
           ("Unavailable_protocol "
           ^ Protocol_hash.to_b58check protocol_hash_from_context))
  | Some proto -> return proto)
  >>=? fun (module Proto_by_predecessor) ->
  Context.Timings.mark_protocol Proto_by_predecessor.hash ;
  (* prepare block_header and evaluate protocol_data *)
  let block_header_by_protocol =
    ({
       shell = block_header.shell;
       protocol_data =
         decode_from_mbytes
           Proto_by_predecessor.block_header_data_encoding
           block_header.protocol_data;
     }
      : Proto_by_predecessor.block_header)
  in
  (* copied from Tezos_validation.Block_validation.parse_operations which is not exposed *)
  (* parse_operations and evalaute operation's protocol data*)
  let parse_operations block_hash operations =
    let invalid_block = Block_validator_errors.invalid_block block_hash in
    List.mapi_es
      (fun pass ->
        List.map_es (fun op ->
            let op_hash = Operation.hash op in
            match
              Data_encoding.Binary.of_bytes_opt
                Proto_by_predecessor.operation_data_encoding
                op.Operation.proto
            with
            | None -> fail (invalid_block (Cannot_parse_operation op_hash))
            | Some protocol_data ->
                let op =
                  {Proto_by_predecessor.shell = op.shell; protocol_data}
                in
                let allowed_pass = Proto_by_predecessor.acceptable_passes op in
                fail_unless
                  (List.mem ~equal:Int.equal pass allowed_pass)
                  (invalid_block
                     (Unallowed_pass {operation = op_hash; pass; allowed_pass}))
                >>=? fun () -> return (op, op_hash)))
      operations
  in
  (* TODO:TE-36 check_block_header *)
  (* TODO:TE-36 parse_block_header *)
  (* TOOD:TE-36 check_operation_quota *)
  Ffi_block_validation.update_testchain_status
    predecessor_block_context
    ~predecessor_hash:block_header.shell.predecessor
    block_header.shell.timestamp
  >>= fun predecessor_block_context ->
  execution_timestamps.operations_decoding_start_t <- Unix.gettimeofday () ;
  ( parse_operations block_header_hash operations >>=? fun operations ->
    execution_timestamps.operations_decoding_end_t <- Unix.gettimeofday () ;
    (match predecessor_block_metadata_hash with
    | None -> Lwt.return predecessor_block_context
    | Some hash ->
        Context.add_predecessor_block_metadata_hash
          predecessor_block_context
          hash)
    >>= fun predecessor_block_context ->
    (match predecessor_ops_metadata_hash with
    | None -> Lwt.return predecessor_block_context
    | Some hash ->
        Context.add_predecessor_ops_metadata_hash predecessor_block_context hash)
    >>= fun predecessor_block_context ->
    (* block_validation.apply *)
    let predecessor_block_context =
      Shell_context.wrap_disk_context predecessor_block_context
    in
    execution_timestamps.begin_application_start_t <- Unix.gettimeofday () ;
    Proto_by_predecessor.begin_application
      ~chain_id
      ~predecessor_context:predecessor_block_context
      ~predecessor_timestamp:predecessor_block_header.shell.timestamp
      ~predecessor_fitness:predecessor_block_header.shell.fitness
      block_header_by_protocol
      ~cache
    >>=? fun state ->
    execution_timestamps.begin_application_end_t <- Unix.gettimeofday () ;
    List.fold_left_es
      (fun (state, acc, timings_acc) ops ->
        List.fold_left_es
          (fun (state, acc, timings_acc) (op, op_hash) ->
            (* mark context_action for opperation *)
            Context.Timings.mark_operation (Some op_hash) ;
            let apply_operation_start_t = Unix.gettimeofday () in
            Proto_by_predecessor.apply_operation state op
            >>=? fun (state, op_metadata) ->
            let apply_operation_end_t = Unix.gettimeofday () in
            return
              ( state,
                op_metadata :: acc,
                (apply_operation_start_t, apply_operation_end_t) :: timings_acc
              ))
          (state, [], [])
          ops
        >>=? fun (state, ops_metadata, timings) ->
        return
          (state, List.rev ops_metadata :: acc, List.rev timings :: timings_acc))
      (state, [], [])
      operations
    >>=? fun (state, ops_metadata, ops_apply_timestamps) ->
    execution_timestamps.operations_application_timestamps <-
      ops_apply_timestamps ;
    Context.Timings.mark_operation None ;
    let ops_metadata = List.rev ops_metadata in
    execution_timestamps.finalize_block_start_t <- Unix.gettimeofday () ;
    Proto_by_predecessor.finalize_block state (Some block_header.shell)
    >>=? fun (validation_result, block_data) ->
    execution_timestamps.finalize_block_end_t <- Unix.gettimeofday () ;
    return (validation_result, block_data, ops_metadata) )
  >>=? fun (validation_result, block_metadata, ops_metadata) ->
  (* TODO: looks like a bug, when protocol is changed, this message is lost somewhere in NewProto.init *)
  let validation_result_message = validation_result.message in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Apply_block] Block finalized: validation_result.message: %s"
        (Option.value ~default:"-empty-" validation_result_message)) ;
  let context = Shell_context.unwrap_disk_context validation_result.context in
  Ffi_block_validation.is_testchain_forking context >>= fun forking_testchain ->
  (* check protocol change *)
  Ffi_block_validation.may_patch_protocol
    ~user_activated_upgrades:!Ffi_block_validation.forced_protocol_upgrades
    ~user_activated_protocol_overrides:
      !Ffi_block_validation.voted_protocol_overrides
    ~level:block_header.shell.level
    validation_result
  >>= fun validation_result ->
  let context = Shell_context.unwrap_disk_context validation_result.context in
  Context.get_protocol context >>= fun new_protocol ->
  let (expected_proto_level, need_to_change_proto) =
    if Protocol_hash.equal new_protocol Proto_by_predecessor.hash then
      (predecessor_block_header.shell.proto_level, false)
    else ((predecessor_block_header.shell.proto_level + 1) mod 256, true)
  in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Apply_block] Change.protocol detection: need_to_change_proto: %s, \
         expected_proto_level: %d, old: %s -> new: %s"
        (string_of_bool need_to_change_proto)
        expected_proto_level
        (Protocol_hash.to_b58check protocol_hash_from_context)
        (Protocol_hash.to_b58check new_protocol)) ;
  match Registered_protocol.get new_protocol with
  | None ->
      (*fail (Unavailable_protocol { block = block_hash ; protocol = new_protocol })*)
      fail
        (Apply_error
           ("Unavailable_protocol " ^ Protocol_hash.to_b58check new_protocol))
  | Some (module NewProto) ->
      (if need_to_change_proto then (
       (* Initialize the new protocol context and obtain the new constants encoded as JSON *)
       ffi_log ~msg:(fun () -> Printf.sprintf "[Apply_block] NewProto.init") ;
       NewProto.init validation_result.context block_header.shell
       >>=? fun validation_result ->
       let new_proto_context =
         Shell_context.unwrap_disk_context validation_result.context
       in
       collect_protocol_cycle_eras_json (module NewProto) new_proto_context
       >>=? fun new_cycle_eras_json ->
       collect_protocol_constants (module NewProto) new_proto_context
       >|=? fun new_constants ->
       ( validation_result,
         NewProto.environment_version,
         Some new_constants,
         new_cycle_eras_json ))
      else
        return
          ( validation_result,
            Proto_by_predecessor.environment_version,
            None,
            None ))
      >>=? fun ( validation_result,
                 new_protocol_env_version,
                 new_protocol_constants_json,
                 new_cycle_eras_json ) ->
      let max_operations_ttl =
        max
          0
          (min (max_operations_ttl + 1) validation_result.max_operations_ttl)
      in
      let validation_result = {validation_result with max_operations_ttl} in
      let cycle_position =
        extract_cycle_position
          Proto_by_predecessor.block_header_metadata_encoding
          block_metadata
      in
      let cycle =
        extract_cycle
          Proto_by_predecessor.block_header_metadata_encoding
          block_metadata
      in
      (* First notify of the application before the commit, with no hash. *)
      Context.block_applied
        context_index
        None
        block_header.shell.level
        cycle_position ;
      (* data from protocol apply *)
      let block_metadata =
        Data_encoding.Binary.to_bytes_exn
          Proto_by_predecessor.block_header_metadata_encoding
          block_metadata
      in
      execution_timestamps.operations_metadata_encoding_start_t <-
        Unix.gettimeofday () ;
      let ops_metadata =
        List.map
          (List.map
             (Data_encoding.Binary.to_bytes_exn
                Proto_by_predecessor.operation_receipt_encoding))
          ops_metadata
      in
      execution_timestamps.operations_metadata_encoding_end_t <-
        Unix.gettimeofday () ;
      let (Context {cache; _}) = validation_result.context in
      (* commit result to context *)
      let context =
        Shell_context.unwrap_disk_context validation_result.context
      in
      (* If a cycle has ended, collect rolls data used by the `*_rights` RPCs *)
      execution_timestamps.collect_new_rolls_owner_snapshots_start_t <-
        Unix.gettimeofday () ;
      collect_new_rolls_owner_snapshots
        (module NewProto)
        context
        cycle
        cycle_position
        block_header.shell.level
      >>=? fun cycle_rolls_owner_snapshots ->
      execution_timestamps.collect_new_rolls_owner_snapshots_end_t <-
        Unix.gettimeofday () ;
      (match new_protocol_env_version with
      | Protocol.V0 -> return (None, None)
      | Protocol.V1 | Protocol.V2 | Protocol.V3 | Protocol.V4 ->
          return
            ( Some
                (List.map
                   (List.map (fun r -> Operation_metadata_hash.hash_bytes [r]))
                   ops_metadata),
              Some (Block_metadata_hash.hash_bytes [block_metadata]) ))
      >>=? fun (ops_metadata_hashes, block_metadata_hash) ->
      ffi_log ~msg:(fun () -> Printf.sprintf "[Apply_block] Context.commit") ;
      assert_result_context_hash context validation_result block_header
      >>=? fun () ->
      execution_timestamps.commit_start_t <- Unix.gettimeofday () ;
      Context.commit
        ~time:block_header.shell.timestamp
        ?message:validation_result.message
        context
      >>= fun context_hash ->
      execution_timestamps.commit_end_t <- Unix.gettimeofday () ;
      let commit_time =
        execution_timestamps.commit_end_t -. execution_timestamps.commit_start_t
      in
      (* TODO - TE-210: this block-applied hook is temporary until checkpoints get implemented *)
      (* Send the notification again but with the hash this time. *)
      Context.block_applied
        context_index
        (Some context_hash)
        block_header.shell.level
        cycle_position ;
      Context.Timings.mark_block None ;
      (* If the protocol changed, mark it already so that it is accounted for in the next checkout *)
      if not (Protocol_hash.equal Proto_by_predecessor.hash NewProto.hash) then
        Context.Timings.mark_protocol NewProto.hash ;
      ffi_log ~msg:(fun () ->
          Printf.sprintf
            "[Apply_block] Context.commit new context_hash: %s"
            (Context_hash.to_b58check context_hash)) ;
      (* Dump context diff after commit *)
      (* Context.dump_diff_tree predecessor_block_context context >>= fun log -> *)
      (* print_endline ("\n\n DUMP: \n" ^ log); *)
      let validation_store =
        ({
           context_hash;
           message = validation_result.message;
           max_operations_ttl = validation_result.max_operations_ttl;
           last_allowed_fork_level = validation_result.last_allowed_fork_level;
           timestamp = block_header.shell.timestamp (* FIXME: this timestamp? *);
         }
          : Tezos_validation.Block_validation.validation_store)
      in
      let block_header_proto_json =
        to_json
          ~newline:false
          ~minify:true
          Proto_by_predecessor.block_header_data_encoding
          block_header_by_protocol.protocol_data
      in

      (* originally, here we can add data to ocaml storage *)
      (* Distributed_db.commit_block *)
      (*     chain_db *)
      (*     block_header_hash block_header block_metadata *)
      (*      operations ops_metadata *)
      (*      validation_store *)
      (*      ~forking_testchain:forking_testchain *)

      (* originally, this is done by Distributed_db.commit_block -> state.ml *)
      let forking_testchain_data : forking_testchain_data option =
        if forking_testchain then
          let test_chain_genesis =
            Context.compute_testchain_genesis block_header_hash
          in
          let test_chain_id =
            Context.compute_testchain_chain_id test_chain_genesis
          in
          Some
            ({forking_block_hash = test_chain_genesis; test_chain_id}
              : forking_testchain_data)
        else None
      in
      (* according to chain_validator.on_request *)
      Ffi_chain_validator.on_success_block_validated enable_testchain
      >>= fun _ ->
      execution_timestamps.apply_end_t <- Unix.gettimeofday () ;
      let response =
        {
          validation_result_message =
            Option.value ~default:"" validation_store.message;
          context_hash = validation_store.context_hash;
          protocol_hash = Proto_by_predecessor.hash;
          next_protocol_hash = NewProto.hash;
          block_header_proto_json;
          block_header_metadata_bytes = block_metadata;
          operations_metadata_bytes = ops_metadata;
          max_operations_ttl = validation_store.max_operations_ttl;
          last_allowed_fork_level = validation_store.last_allowed_fork_level;
          forking_testchain;
          forking_testchain_data;
          block_metadata_hash;
          ops_metadata_hashes;
          ops_metadata_hash =
            Option.map
              (fun hashes ->
                List.map Operation_metadata_list_hash.compute hashes
                |> Operation_metadata_list_list_hash.compute)
              ops_metadata_hashes;
          cycle_rolls_owner_snapshots;
          new_protocol_constants_json;
          new_cycle_eras_json;
          commit_time;
          execution_timestamps;
        }
      in
      Lwt.return_ok {response; cache}

(* Wrapper for `apply_and_commit` with `cached_result` handling *)
let apply_and_commit ?(cached_result : (apply_block_result * Context.t) option)
    ~cache (context_index : Context.index) (chain_id : Chain_id.t)
    (block_header_hash : Block_hash.t) (block_header : Block_header.t)
    (operations : Operation.t list list)
    (predecessor_block_header : Block_header.t) (max_operations_ttl : int)
    (enable_testchain : bool)
    (predecessor_block_metadata_hash : Block_metadata_hash.t option)
    (predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option)
    =
  match cached_result with
  | Some (({response; _} as cached_result), context)
    when Context_hash.equal response.context_hash block_header.shell.context
         (* FIXME: needs timestamp in result *)
         (*&& Time.Protocol.equal
              result.validation_store.timestamp
              block_header.shell.timestamp*) ->
      Validation_events.(emit using_preapply_result block_header_hash)
      >>= fun () ->
      Context.commit
        ~time:block_header.shell.timestamp
        ~message:response.validation_result_message
        context
      >>= fun context_hash ->
      assert (Context_hash.equal context_hash response.context_hash) ;
      return cached_result
  | Some _ | None -> (
      apply_and_commit
        ~cache
        context_index
        chain_id
        block_header_hash
        block_header
        operations
        predecessor_block_header
        max_operations_ttl
        enable_testchain
        predecessor_block_metadata_hash
        predecessor_ops_metadata_hash
      >>= fun res ->
      match res with
      | Error (Context_hash_result_mismatch _ :: _) ->
          (* We clean the cache on failure, because the failure may be caused
             by a bug in the cache *)
          Protocol_cache.cache := None ;
          Lwt.return res
      | Ok _ | Error _ -> Lwt.return res)

let apply (block_header_hash : Block_hash.t)
    (apply_block_request : apply_block_request) =
  let {
    chain_id;
    block_header;
    operations;
    pred_header;
    max_operations_ttl;
    predecessor_block_metadata_hash;
    predecessor_ops_metadata_hash;
  } =
    apply_block_request
  in
  assert_complete_operations block_header operations >>=? fun () ->
  (* check received data correctness *)
  assert_predecessor_ok block_header pred_header >>=? fun () ->
  assert_operations_hash_ok block_header operations >>=? fun () ->
  let storage = Storage_facade.global_storage in
  Storage_facade.context_index storage >>=? fun context_index ->
  let context_index = Context.Timings.wrap context_index in
  let enable_testchain = storage.enable_testchain in
  (* First time we will load the cache, and then we will inherit the cache from
     the previous block *)
  let cache =
    match !Protocol_cache.cache with
    | None -> `Force_load
    | Some cache -> `Inherited (cache, block_header.shell.context)
  in
  (* TODO: cached_result *)
  apply_and_commit
    ~cache
    context_index
    chain_id
    block_header_hash
    block_header
    operations
    pred_header
    max_operations_ttl
    enable_testchain
    predecessor_block_metadata_hash
    predecessor_ops_metadata_hash
  >>=? fun result ->
  Protocol_cache.cache :=
    Some
      Environment_context.Context.
        {cache = result.cache; context_hash = result.response.context_hash} ;
  Lwt.return_ok result

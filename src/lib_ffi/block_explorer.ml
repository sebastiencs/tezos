open Tezos_base__TzPervasives
open Tezos_protocol_updater
open Ffi_utils

type commit_genesis_result_data = {
  block_header_proto_json : string;
  block_header_proto_metadata_bytes : bytes;
  operations_proto_metadata_bytes : bytes list list;
}

type commit_genesis_result_data_request = {
  context_hash : Context_hash.t;
  chain_id : Chain_id.t;
  protocol_hash : Protocol_hash.t;
  genesis_max_operations_ttl : int;
}

(* header protocol metadata *)
(* block metadata *)
(* see block_directory.ml: let metadata block = *)
(* this function does not use storage, but just converts datas *)
let metadata_to_json_without_storage metadata max_operations_ttl
    (module Proto : Tezos_shell_services.Block_services.PROTO)
    (module Next_proto : Registered_protocol.T) =
  let module Block_services =
    Tezos_shell_services.Block_services.Make (Proto) (Next_proto)
  in
  (*    State.Block.metadata block*)
  (*    >>=? fun metadata ->*)
  let protocol_data =
    Data_encoding.Binary.of_bytes_exn
      Proto.block_header_metadata_encoding
      metadata
  in
  (* NOTE: fixed value, support for this was removed in florence, so we always return Not_running *)
  let test_chain_status = Tezos_base__Test_chain_status.Not_running in
  (*    State.Block.test_chain block*)
  (*    State.Block.max_operations_ttl block*)
  (*    >>=? fun max_operations_ttl ->*)
  let json =
    to_json
      ~newline:false
      ~minify:true
      Block_services.block_metadata_encoding
      {
        Block_services.protocol_data;
        test_chain_status;
        max_operations_ttl;
        max_operation_data_length = Next_proto.max_operation_data_length;
        max_block_header_length = Next_proto.max_block_length;
        operation_list_quota =
          List.map
            (fun {Tezos_protocol_environment.max_size; max_op} ->
              {Tezos_shell_services.Block_services.max_size; max_op})
            Next_proto.validation_passes;
      }
  in
  Lwt.return_ok json

(* Because there is no limit currently in TezEdge, we mark big values here *)
let adjust_operation_metadata bytes =
  let open Tezos_validation.Block_validation in
  (*let limit =
      Option.value ~default:10_000_000 default_operation_metadata_size_limit
    in
    if Bytes.length bytes > limit then Too_large_metadata else*)
  (* TODO: we don't limit anything for now, revise this once support has been added
     in the Rust shell*)
  Metadata bytes

(* operations *)
(* see block_directory.ml: let operations block = *)
(* this function does not use storage, but just converts datas *)
let operations_to_json_without_storage chain_id all_operations
    all_operations_metadata_bytes
    (module Proto : Tezos_shell_services.Block_services.PROTO)
    (module Next_proto : Registered_protocol.T) =
  let module Block_services =
    Tezos_shell_services.Block_services.Make (Proto) (Next_proto)
  in
  (* Function used to try to encode the result with metadata included *)
  let convert_with_metadata chain_id (op : Operation.t) metadata :
      Block_services.operation =
    let protocol_data =
      Data_encoding.Binary.of_bytes_exn Proto.operation_data_encoding op.proto
    in
    let receipt =
      let open Tezos_validation.Block_validation in
      match metadata with
      | Metadata bytes ->
          Block_services.Receipt
            (Data_encoding.Binary.of_bytes_exn
               Proto.operation_receipt_encoding
               bytes)
      | Too_large_metadata -> Too_large
    in
    {
      Block_services.chain_id;
      hash = Operation.hash op;
      shell = op.shell;
      protocol_data;
      receipt;
    }
  in
  (* Fallback function for when encoding with metadata fails (too much metadata) *)
  let convert_without_metadata chain_id (op : Operation.t) =
    let protocol_data =
      Data_encoding.Binary.of_bytes_exn Proto.operation_data_encoding op.proto
    in
    {
      Block_services.chain_id;
      hash = Operation.hash op;
      shell = op.shell;
      protocol_data;
      receipt = Empty;
    }
  in
  (*    State.Block.all_operations block*)
  (*    >>= fun ops ->*)
  (*    State.Block.all_operations_metadata block*)
  (*    >>= fun metadata ->*)
  (*    let chain_id = State.Block.chain_id block in*)
  let all_operations_metadata =
    List.map (List.map adjust_operation_metadata) all_operations_metadata_bytes
  in
  Lwt.catch
    (fun () ->
      List.map2_e
        ~when_different_lengths:()
        (List.map2 ~when_different_lengths:() (convert_with_metadata chain_id))
        all_operations
        all_operations_metadata
      |> function
      | Ok v -> return v
      | Error () -> raise Not_found)
    (fun exn ->
      (* Failed, encode without metadata *)
      Printf.eprintf
        "WARNING: failure when encoding operations metadata JSON, trying \
         without metadata %s\n\
         %!"
        (Printexc.to_string exn) ;
      return
        (List.map (List.map (convert_without_metadata chain_id)) all_operations))
  >>=? fun ops_data ->
  let json =
    to_json
      ~newline:false
      ~minify:true
      (Data_encoding.list
         (Data_encoding.list Block_services.operation_encoding))
      ops_data
  in
  return json

(* before genesis protocol there is z zero protocol: Protocol_hash.zero *)
module FakeProto = Tezos_shell_services.Block_services.Fake_protocol

(* GENESIS - header protocol metadata *)
(* see block_directory.ml: let metadata block = *)
(* this function does not use storage, but just converts datas *)
let genesis_metadata_to_json_without_storage _context_hash _chain_id
    (genesis_protocol : Protocol_hash.t) _max_operations_ttl =
  match Registered_protocol.get genesis_protocol with
  | None ->
      fail
        (Ffi_errors.Ffi_call_error
           (Printf.sprintf
              "Uknown_protocol: %s"
              (Protocol_hash.to_b58check genesis_protocol)))
  | Some (module NextProto) ->
      let header_protocol_data = Bytes.create 0 in
      let header_protocol_data =
        decode_from_mbytes
          FakeProto.block_header_data_encoding
          header_protocol_data
      in
      let block_header_proto_metadata_bytes = Bytes.create 0 in
      let operations_proto_metadata_bytes = [] in
      let block_header_proto_json =
        to_json
          ~newline:false
          ~minify:true
          FakeProto.block_header_data_encoding
          header_protocol_data
      in
      return
        {
          block_header_proto_json;
          block_header_proto_metadata_bytes;
          operations_proto_metadata_bytes;
        }

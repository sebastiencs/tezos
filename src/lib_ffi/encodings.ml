open Tezos_base__TzPervasives

let o = "*"

type key_encoding_t =
  | Record : {
      key_pattern : string list;
      encoding : 'a Data_encoding.Encoding.t;
    }
      -> key_encoding_t

module EncodingMap = Map.Make (Protocol_hash)

let record key_pattern encoding = Record {key_pattern; encoding}

let matches pattern str =
  if String.equal pattern o then true
  else if String.equal pattern str then true
  else false

let key_matches_pattern (Record {key_pattern; _}) (key : string list) =
  if List.length key_pattern != List.length key then false
  else
    match List.for_all2 ~when_different_lengths:() matches key_pattern key with
    | Ok result -> result
    | Error _ -> false

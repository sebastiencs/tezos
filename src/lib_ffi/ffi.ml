open Ffi_converter
open Ffi_errors
open Ffi_logger

type hash = Bytes.t

module Hash_converter = Ffi_converter.Hash_converter

(* NOTE: current code uses plain bytes instead of hash types *)
type json_encode_apply_block_result_metadata_request = {
  context_hash : Context_hash.t;
  metadata_bytes : bytes;
  max_operations_ttl : int;
  protocol_hash : Protocol_hash.t;
  next_protocol_hash : Protocol_hash.t;
}

type json_encode_apply_block_operations_metadata_request = {
  chain_id : Chain_id.t;
  all_operations : Operation.t list list;
  all_operations_metadata_bytes : bytes list list;
  protocol_hash : Protocol_hash.t;
  next_protocol_hash : Protocol_hash.t;
}

type init_protocol_context_result = {
  known_protocols : Protocol_hash.t list;
  genesis_context_hash : Context_hash.t option;
}

let init_protocol_context cfg =
  let open Ffi_config.Context in
  ffi_log ~msg:(fun () ->
      let sandbox_json_patch_context =
        match cfg.sandbox_json_patch_context with
        | Some (key, json) -> Printf.sprintf "(key: %s, json: %s)" key json
        | None -> "-none-"
      in
      Printf.sprintf
        "Calling init_protocol_context with storage config: %s and genesis: %s \
         and protocol_overrides: %s, commit_genesis: %s, enable_testchain: %s, \
         readonly: %s, sandbox_json_patch_context: %s"
        (Ffi_config.Context.storage_config_description cfg.storage)
        (genesis_to_string cfg.genesis)
        (protocol_overrides_to_string cfg.protocol_overrides)
        (string_of_bool cfg.commit_genesis)
        (string_of_bool cfg.enable_testchain)
        (string_of_bool cfg.readonly)
        sandbox_json_patch_context) ;
  (* initialize protocol context *)
  Storage_facade.init_context cfg >>=? fun (_, _, genesis_context_hash) ->
  (* initialize protocol overrides *)
  Lwt.return
    (Ffi_block_validation.init_protocol_overrides cfg.protocol_overrides)
  >>=? fun () ->
  (* list known protocol *)
  let known_protocols = List.of_seq @@ Registered_protocol.seq_embedded () in
  ffi_log ~msg:(fun () ->
      let protocols =
        String.concat ", " @@ List.map Protocol_hash.to_b58check known_protocols
      in
      Printf.sprintf
        "Context runtime initialized:\n context_hash: %s, protocols: [%s]"
        (Option.value ~default:"-none-"
        @@ Option.map
             (fun ch -> Context_hash.to_b58check ch)
             genesis_context_hash)
        protocols) ;
  return {known_protocols; genesis_context_hash}

let genesis_result_data request =
  let Block_explorer.
        {context_hash; chain_id; protocol_hash; genesis_max_operations_ttl} =
    request
  in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Calling genesis_result_data with context_hash: %s, chain_id: %s, \
         protocol_hash: %s and genesis_max_operations_ttl: %d"
        (Context_hash.to_b58check context_hash)
        (Chain_id.to_b58check chain_id)
        (Protocol_hash.to_b58check protocol_hash)
        genesis_max_operations_ttl) ;
  (* collect json for metadata *)
  Block_explorer.genesis_metadata_to_json_without_storage
    context_hash
    chain_id
    protocol_hash
    genesis_max_operations_ttl
  >>=? fun result ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Result of genesis_result_data:\n\
        \ context_hash: %s, block_header_proto_json: %s"
        (Context_hash.to_b58check context_hash)
        result.block_header_proto_json) ;
  return result

let get_block_services_protocol protocol_hash =
  match Registered_protocol.get protocol_hash with
  | None ->
      if Protocol_hash.equal protocol_hash Protocol_hash.zero then
        Lwt.return_ok
          (module Block_explorer.FakeProto
          : Tezos_shell_services.Block_services.PROTO)
      else
        fail
          (Ffi_errors.Ffi_call_error
             (Printf.sprintf
                "Uknown_protocol: %s"
                (Protocol_hash.to_b58check protocol_hash)))
  | Some (module Proto) ->
      Lwt.return_ok (module Proto : Tezos_shell_services.Block_services.PROTO)

let get_registered_protocol protocol_hash =
  match Registered_protocol.get protocol_hash with
  | None ->
      fail
        (Ffi_errors.Ffi_call_error
           (Printf.sprintf
              "Uknown_protocol: %s"
              (Protocol_hash.to_b58check protocol_hash)))
  | Some proto -> Lwt.return_ok proto

let apply_block_result_metadata request =
  let {
    context_hash;
    metadata_bytes;
    max_operations_ttl;
    protocol_hash;
    next_protocol_hash;
  } =
    request
  in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Calling apply_block_result_metadata with context_hash: %s, \
         protocol_hash: %s, next_protocol_hash: %s"
        (Context_hash.to_b58check context_hash)
        (Protocol_hash.to_b58check protocol_hash)
        (Protocol_hash.to_b58check next_protocol_hash)) ;
  (* collect json for metadata *)
  get_block_services_protocol protocol_hash >>=? fun (module Proto) ->
  get_registered_protocol next_protocol_hash >>=? fun (module Next_proto) ->
  Block_explorer.metadata_to_json_without_storage
    metadata_bytes
    max_operations_ttl
    (module Proto)
    (module Next_proto)
  >>=? fun metadata_json ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Result of apply_block_result_metadata:\n\
        \ context_hash: %s, metadata_json: %s"
        (Context_hash.to_b58check context_hash)
        metadata_json) ;
  return metadata_json

let apply_block_operations_metadata request =
  let {
    chain_id;
    all_operations;
    all_operations_metadata_bytes;
    protocol_hash;
    next_protocol_hash;
  } =
    request
  in
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Calling apply_block_operations_metadata with protocol_hash: %s, \
         next_protocol_hash: %s"
        (Protocol_hash.to_b58check protocol_hash)
        (Protocol_hash.to_b58check next_protocol_hash)) ;
  (* collect json for metadata *)
  get_block_services_protocol protocol_hash >>=? fun (module Proto) ->
  get_registered_protocol next_protocol_hash >>=? fun (module Next_proto) ->
  Block_explorer.operations_to_json_without_storage
    chain_id
    all_operations
    all_operations_metadata_bytes
    (module Proto)
    (module Next_proto)
  >>=? fun metadata_json ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Result of apply_block_operations_metadata:\n  metadata_json: %s"
        metadata_json) ;
  return metadata_json

let apply_block apply_block_request =
  let Apply_block.{chain_id; block_header; max_operations_ttl; _} =
    apply_block_request
  in
  let block_header_hash = Block_header.hash block_header in
  (* log input as hex bytes *)
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Calling apply_block with:\n\
        \  chain_id: %s\n\
        \  block_header_hash: %s\n\
        \  max_operations_ttl(%d)"
        (Chain_id.to_b58check chain_id)
        (Block_hash.to_b58check block_header_hash)
        max_operations_ttl) ;
  (* apply block and operations*)
  Apply_block.apply block_header_hash apply_block_request
  >>=? fun apply_block_result ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Apply_block] Finished for block: %s" (*" with result - %s"*)
        (Block_hash.to_b58check block_header_hash)) ;
  return apply_block_result.response

let assert_encoding_for_protocol_data protocol_hash protocol_data =
  (* protocol instance *)
  match Registered_protocol.get protocol_hash with
  | None ->
      error
      @@ Ffi_call_error
           ("Unavailable_protocol " ^ Protocol_hash.to_b58check protocol_hash)
  | Some (module Proto) -> (
      (* try to prepare evaluate protocol_data *)
      try
        let _ =
          Ffi_utils.decode_from_mbytes
            Proto.block_header_data_encoding
            protocol_data
        in
        ok ()
      with exn ->
        error
        @@ Ffi_call_error
             ("Error decoding protocol data: " ^ Printexc.to_string exn))

let begin_application begin_application_request =
  ffi_log ~msg:(fun () ->
      let Validate_block.{chain_id; pred_header; block_header} =
        begin_application_request
      in
      Printf.sprintf
        "Calling begin_application with:\n\
        \  chain_id: %s\n\
        \  pred_header: %s block_header: %s"
        (Chain_id.to_b58check chain_id)
        (Block_hash.to_b58check @@ Block_header.hash pred_header)
        (Block_hash.to_b58check @@ Block_header.hash block_header)) ;
  (* begin_application *)
  Validate_block.begin_application begin_application_request
  >>=? fun begin_application_response ->
  ffi_log ~msg:(fun () ->
      let {Validate_block.chain_id; pred_header; block_header} =
        begin_application_request
      in
      Printf.sprintf
        "[Begin_application] Done for chain_id: %s for pred_header: %s \
         block_header: %s"
        (Chain_id.to_b58check chain_id)
        (Block_hash.to_b58check @@ Block_header.hash pred_header)
        (Block_hash.to_b58check @@ Block_header.hash block_header)) ;
  return begin_application_response

let begin_construction
    (begin_construction_request : Validate_block.begin_construction_request) =
  ffi_log ~msg:(fun () ->
      let Validate_block.{chain_id; predecessor; protocol_data; _} =
        begin_construction_request
      in
      Printf.sprintf
        "Calling begin_construction with:\n\
        \  chain_id: %s\n\
        \  predecessor_block_hash: %s data: %s"
        (Chain_id.to_b58check chain_id)
        (Block_hash.to_b58check @@ Block_header.hash predecessor)
        (match protocol_data with
        | Some pd -> string_of_int @@ Bytes.length pd
        | None -> "-none-")) ;
  (* begin_construction *)
  Validate_block.begin_construction begin_construction_request
  >>=? fun prevalidator ->
  ffi_log ~msg:(fun () ->
      let ({predecessor; _} : Validate_block.begin_construction_request) =
        begin_construction_request
      in
      Printf.sprintf
        "[Begin_construction] Prevalidator started for chain_id: %s for \
         predecessor block: %s with protocol: %s"
        (Chain_id.to_b58check prevalidator.chain_id)
        (Block_hash.to_b58check @@ Block_header.hash predecessor)
        (Protocol_hash.to_b58check prevalidator.protocol)) ;
  return prevalidator

let validate_operation
    (validate_operation_request : Validate_block.prevalidator_request) =
  (* log input as hex bytes *)
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "Calling validate_operation with prevalidator: chain_id/protocol %s/%s \
         for operation: %s"
        (Chain_id.to_b58check validate_operation_request.prevalidator.chain_id)
        (Protocol_hash.to_b58check
           validate_operation_request.prevalidator.protocol)
        (Operation_hash.to_b58check
        @@ Operation.hash validate_operation_request.operation)) ;
  (* begin_construction *)
  Validate_block.validate_operation validate_operation_request
  >>=? fun validation_response ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Validate_operation] Prevalidation done for chain_id/protocol %s/%s \
         for operation: %s with result: \n\
         %s"
        (Chain_id.to_b58check validate_operation_request.prevalidator.chain_id)
        (Protocol_hash.to_b58check
           validate_operation_request.prevalidator.protocol)
        (Operation_hash.to_b58check
        @@ Operation.hash validate_operation_request.operation)
        (Ffi_utils.to_json
           ~newline:false
           ~minify:true
           Validate_block.validate_operation_response_encoding
           validation_response)) ;
  return validation_response

let pre_filter_operation
  (pre_filter_operation_request : Validate_block.prevalidator_request) =
  (* log input as hex bytes *)
  ffi_log ~msg:(fun () ->
    Printf.sprintf
      "Calling validate_operation with prevalidator: chain_id/protocol %s/%s \
       for operation: %s"
      (Chain_id.to_b58check pre_filter_operation_request.prevalidator.chain_id)
      (Protocol_hash.to_b58check
      pre_filter_operation_request.prevalidator.protocol)
      (Operation_hash.to_b58check
      @@ Operation.hash pre_filter_operation_request.operation)) ;
(* begin_construction *)
Validate_block.pre_filter_operation pre_filter_operation_request
>>=? fun pre_filter_response ->
(*ffi_log ~msg:(fun () ->
    Printf.sprintf
      "[Validate_operation] Prevalidation done for chain_id/protocol %s/%s \
       for operation: %s with result: \n\
       %s"
      (Chain_id.to_b58check pre_filter_operation_request.prevalidator.chain_id)
      (Protocol_hash.to_b58check
      pre_filter_operation_request.prevalidator.protocol)
      (Operation_hash.to_b58check
      @@ Operation.hash pre_filter_operation_request.operation)
      (Ffi_utils.to_json
         ~newline:false
         ~minify:true
         Validate_block.validate_operation_response_encoding
         validation_response)) ;*)
return pre_filter_response

let change_runtime_configuration cfg =
  let open Ffi_config.RuntimeConfiguration in
  Printexc.record_backtrace true ;
  (* logging configuration *)
  Ffi_logger.set_log_enabled cfg.log_enabled ;
  Logs.set_reporter (Logs.format_reporter ()) ;
  Logs.set_level cfg.log_level ;
  (* posibility to debug GC *)
  (* Gc.set { (Gc.get ()) with Gc.verbose = 0x63 }; *)
  ()

let decode_context_data protocol_hash key data =
  try
    let protocol_hash = Hash_converter.to_protocol_hash protocol_hash in
    let json =
      Encodings_explorer.resolve_value_as_json (Some protocol_hash) key data
    in
    ok json
  with e ->
    let error_msg = Printexc.to_string e
    and stack = Printexc.get_backtrace () in
    let formatted_msg =
      Printf.sprintf "error_msg: '%s' stacktrace: '%s'" error_msg stack
    in
    ffi_log_error ~msg:(fun () ->
        Printf.sprintf "decode_context_data failed: %s" formatted_msg) ;
    error (Ffi_call_error formatted_msg)

let compute_path request =
  try
    let paths = Ffi_utils.compute_operation_path request in
    ffi_log ~msg:(fun () -> Printf.sprintf "Path calculation completed") ;
    ok paths
  with e ->
    let error_msg = Printexc.to_string e
    and stack = Printexc.get_backtrace () in
    let formatted_msg =
      Printf.sprintf "error_msg: '%s' stacktrace: '%s'" error_msg stack
    in
    ffi_log_error ~msg:(fun () ->
        Printf.sprintf "compute_path failed: %s" formatted_msg) ;
    error (Ffi_call_error formatted_msg)

let helpers_preapply_operations request =
  let open Ffi_rpc_structs in
  let {block_header; chain_id; request; _} = request in
  Ffi_rpc_service.call_preapply_operations chain_id block_header request

let helpers_preapply_block request =
  let open Ffi_rpc_structs in
  let {
    protocol_rpc_request;
    predecessor_block_metadata_hash;
    predecessor_ops_metadata_hash;
    predecessor_max_operations_ttl;
  } =
    request
  in
  let {block_header; chain_id; request; _} = protocol_rpc_request in
  Ffi_rpc_service.call_preapply_block
    chain_id
    block_header
    request
    predecessor_block_metadata_hash
    predecessor_ops_metadata_hash
    predecessor_max_operations_ttl

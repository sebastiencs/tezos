type string_b58check = string

type forced_protocol_upgrades = (int32 * string_b58check) list

type voted_protocol_overrides = (string_b58check * string_b58check) list

type protocol_overrides = forced_protocol_upgrades * voted_protocol_overrides

type key_json = (string * string) option

module RuntimeConfiguration = struct
  type t = {log_enabled : bool; log_level : Logs.level option}
end

module Context = struct
  module Irmin = struct
    type t = {data_dir : string}
  end

  module Tezedge = Tezos_tezedge_context.Tcontext.Config

  type storage =
    | Irmin_only of Irmin.t
    | Tezedge_only of Tezedge.t
    | Both of Irmin.t * Tezedge.t

  type t = {
    storage : storage;
    genesis : string * string * string;  (** (time, block, protocol) *)
    protocol_overrides : protocol_overrides;
        (** struct for protocol_overrides see ffi_block_validation.ml *)
    commit_genesis : bool;
    enable_testchain : bool;
    readonly : bool;
    sandbox_json_patch_context : key_json;
    context_stats_db_path : string option;
  }

  let rec storage_config_description = function
    | Irmin_only {data_dir} -> Printf.sprintf "Irmin{data_dir=%s}" data_dir
    | Tezedge_only {backend; _} ->
        Printf.sprintf "TezEdge(%s)" (Tezedge.show_backend backend)
    | Both (icfg, tcfg) ->
        Printf.sprintf
          "%s + %s"
          (storage_config_description (Irmin_only icfg))
          (storage_config_description (Tezedge_only tcfg))
end

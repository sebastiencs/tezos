(* This module provides `wrapN` helpers to wrap functions that return error traces
   in versions that post-process such error traces, and also catch any rogue exception
   which then gets converted into a regular error *)
type error_trace = {head_error_id : string; trace_json : string}

let error_name = Printexc.to_string

let rec get_field_value ~default name = function
  | (field_name, `String value) :: _ when String.equal field_name name -> value
  | _ :: rest -> get_field_value ~default name rest
  | _ -> default

(* Starting with environment V2 traces are opaque, but we can convert them to JSON. *)
(* Here the trace is converted into it's JSON representation and the 'id' of the head *)
(* error is extracted so that it can be matched with ease on Tezedge *)
let postprocess_trace = function
  | Ok _ as ok -> ok
  | Error trace ->
      let json =
        Data_encoding.Json.construct Error_monad.trace_encoding trace
      in
      let trace_json = Ezjsonm.value_to_string json in
      let head_error_id =
        match json with
        | `A (`O pairs :: _) ->
            get_field_value ~default:"ffi.unknown" "id" pairs
        | _ -> "ffi.unkown_error"
      in
      Error {head_error_id; trace_json}

let lwt_exception_protect f =
  Lwt.catch f (function
      | Tezos_tezedge_context.Errors.RustFailure msg as exn ->
          let description = Printf.sprintf "%s: %s" (error_name exn) msg in
          fail (Ffi_errors.Ffi_call_exception description)
      | Tezos_tezedge_context.Errors.UnimplementedFFIFunction name as exn ->
          let description = Printf.sprintf "%s: %s" (error_name exn) name in
          fail (Ffi_errors.Ffi_call_exception description)
      | Tezos_tezedge_context.Errors.UnimplementedXcontext name as exn ->
          let description = Printf.sprintf "%s: %s" (error_name exn) name in
          fail (Ffi_errors.Ffi_call_exception description)
      | Tezos_tezedge_context.Errors.InconsistencyFailure msg as exn ->
          let description = Printf.sprintf "%s: %s" (error_name exn) msg in
          fail (Ffi_errors.Ffi_call_exception description)
      | exn ->
          let backtrace = Printexc.get_backtrace () in
          let description =
            Printf.sprintf "%s: %s" (error_name exn) backtrace
          in
          fail (Ffi_errors.Ffi_call_exception description))

let exception_protect f =
  try f () with
  | Tezos_tezedge_context.Errors.RustFailure msg as exn ->
      let description = Printf.sprintf "%s: %s" (error_name exn) msg in
      error (Ffi_errors.Ffi_call_exception description)
  | Tezos_tezedge_context.Errors.UnimplementedFFIFunction name as exn ->
      let description = Printf.sprintf "%s: %s" (error_name exn) name in
      error (Ffi_errors.Ffi_call_exception description)
  | Tezos_tezedge_context.Errors.UnimplementedXcontext name as exn ->
      let description = Printf.sprintf "%s: %s" (error_name exn) name in
      error (Ffi_errors.Ffi_call_exception description)
  | Tezos_tezedge_context.Errors.InconsistencyFailure msg as exn ->
      let description = Printf.sprintf "%s: %s" (error_name exn) msg in
      error (Ffi_errors.Ffi_call_exception description)
  | exn ->
      let backtrace = Printexc.get_backtrace () in
      let description = Printf.sprintf "%s: %s" (error_name exn) backtrace in
      error (Ffi_errors.Ffi_call_exception description)

let wrap1 f arg = exception_protect (fun () -> f arg) |> postprocess_trace

let wrap2 f arg1 arg2 =
  exception_protect (fun () -> f arg1 arg2) |> postprocess_trace

let wrap3 f arg1 arg2 arg3 =
  exception_protect (fun () -> f arg1 arg2 arg3) |> postprocess_trace

let wrap4 f arg1 arg2 arg3 arg4 =
  exception_protect (fun () -> f arg1 arg2 arg3 arg4) |> postprocess_trace

let wrap5 f arg1 arg2 arg3 arg4 arg5 =
  exception_protect (fun () -> f arg1 arg2 arg3 arg4 arg5) |> postprocess_trace

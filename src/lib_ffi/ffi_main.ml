let echo x : string = x

module WithErrorHandler = Ffi_error_wrapper

let export_ffi () =
  (* for tests purposes *)
  Callback.register "echo" echo ;
  Callback.register "chain_id_roundtrip" Ffi_test_roundtrips.chain_id_roundtrip ;
  Callback.register
    "block_header_roundtrip"
    Ffi_test_roundtrips.block_header_roundtrip ;
  Callback.register
    "block_header_with_hash_roundtrip"
    Ffi_test_roundtrips.block_header_with_hash_roundtrip ;
  Callback.register
    "operation_roundtrip"
    Ffi_test_roundtrips.operation_roundtrip ;
  Callback.register
    "operations_list_list_roundtrip"
    Ffi_test_roundtrips.operations_list_list_roundtrip ;
  Callback.register
    "apply_block_params_roundtrip"
    Ffi_test_roundtrips.apply_block_params_roundtrip ;
  Callback.register
    "setup_benchmark_apply_block_response"
    Ffi_test_roundtrips.setup_benchmark_apply_block_response ;
  Callback.register
    "apply_block_request_decoded_roundtrip"
    Ffi_test_roundtrips.apply_block_request_decoded_roundtrip ;
  Callback.register
    "context_callback_roundtrip"
    Ffi_test_roundtrips.context_callback_roundtrip ;
  Callback.register
    "block_header_struct_roundtrip"
    Ffi_test_roundtrips.block_header_struct_roundtrip ;
  (* for struct mapping tests *)
  Callback.register
    "construct_and_compare_hash"
    Ffi_test_roundtrips.construct_and_compare_hash ;
  Callback.register
    "construct_and_compare_apply_block_request"
    Ffi_test_roundtrips.construct_and_compare_apply_block_request ;
  Callback.register
    "construct_and_compare_cycle_rolls_owner_snapshot"
    Ffi_test_roundtrips.construct_and_compare_cycle_rolls_owner_snapshot ;
  Callback.register
    "construct_and_compare_apply_block_response"
    Ffi_test_roundtrips.construct_and_compare_apply_block_response ;
  Callback.register
    "construct_and_compare_block_header"
    Ffi_test_roundtrips.construct_and_compare_block_header ;
  Callback.register
    "construct_and_compare_begin_construction_request"
    Ffi_test_roundtrips.construct_and_compare_begin_construction_request ;
  Callback.register
    "construct_and_compare_validate_operation_request"
    Ffi_test_roundtrips.construct_and_compare_validate_operation_request ;
  Callback.register
    "construct_and_compare_rpc_request"
    Ffi_test_roundtrips.construct_and_compare_rpc_request ;
  Callback.register
    "construct_and_compare_protocol_rpc_request"
    Ffi_test_roundtrips.construct_and_compare_protocol_rpc_request ;
  Callback.register
    "construct_and_compare_operation"
    Ffi_test_roundtrips.construct_and_compare_operation ;
  Callback.register
    "construct_and_compare_prevalidator_wrapper"
    Ffi_test_roundtrips.construct_and_compare_prevalidator_wrapper ;
  (* ffi *)
  Callback.register
    "decode_context_data"
    (WithErrorHandler.wrap3 Ffi.decode_context_data) ;
  Callback.register "ffi_server_loop" (WithErrorHandler.wrap1 Ffi_server.main) ;
  Callback.register "ffi_apply_encoded_message" Ffi_server.apply_encoded_message

let () =
  Ffi.change_runtime_configuration
    Ffi_config.RuntimeConfiguration.{log_enabled = false; log_level = None} ;
  export_ffi () ;
  Ffi_storage.export_tezos_context_ffi () ;
  Encodings_explorer.explore () ;
  Protocol_registerer.register ()

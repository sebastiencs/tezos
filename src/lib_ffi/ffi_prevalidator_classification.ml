(* The contents on this file were copied and adapted from lib_shell/prevalidator_classification.ml *)

type error_classification =
  [ `Branch_delayed of tztrace
  | `Branch_refused of tztrace
  | `Refused of tztrace
  | `Outdated of tztrace ]

type classification = [`Applied | `Prechecked | error_classification]

let classification_encoding : classification Data_encoding.t =
  let open Data_encoding in
  let branch_encoding name obj =
    conv
      (fun x -> ((), x))
      (fun ((), x) -> x)
      (merge_objs (obj1 (req "classification" (constant name))) obj)
  in
  def
    "Ffi_prevalidator_classification.classification_encoding"
    ~description:"Operation classification"
  @@ union
       ~tag_size:`Uint8
       [
         case
           (Tag 0)
           ~title:"Branch_delayed"
           (branch_encoding
              "Branch_delayed"
              (obj1 (req "error" Error_monad.trace_encoding)))
           (function `Branch_delayed trace -> Some trace | _ -> None)
           (fun trace -> `Branch_delayed trace);
         case
           (Tag 1)
           ~title:"Branch_refused"
           (branch_encoding
              "Branch_refused"
              (obj1 (req "error" Error_monad.trace_encoding)))
           (function `Branch_refused trace -> Some trace | _ -> None)
           (fun trace -> `Branch_refused trace);
         case
           (Tag 2)
           ~title:"Refused"
           (branch_encoding
              "Refused"
              (obj1 (req "error" Error_monad.trace_encoding)))
           (function `Refused trace -> Some trace | _ -> None)
           (fun trace -> `Refused trace);
         case
           (Tag 3)
           ~title:"Outdated"
           (branch_encoding
              "Outdated"
              (obj1 (req "error" Error_monad.trace_encoding)))
           (function `Outdated trace -> Some trace | _ -> None)
           (fun trace -> `Outdated trace);
         case
           (Tag 4)
           ~title:"Applied"
           (branch_encoding "Applied" empty)
           (function `Applied -> Some () | _ -> None)
           (fun () -> `Applied);
         case
           (Tag 5)
           ~title:"Prechecked"
           (branch_encoding "Prechecked" empty)
           (function `Prechecked -> Some () | _ -> None)
           (fun () -> `Prechecked);
       ]

open Ffi_rpc_structs
module Bytes_converter = Ffi_converter.Bytes_bytes_converter

exception Internal_error of error trace

(* Service definitions for handling `../context/raw/bytes/*` *)
module Raw_context_service = struct
  let read_partial_context =
    let init = Block_services.Dir TzString.Map.empty in
    fun context path depth ->
      if depth = 0 then Lwt.return Block_services.Cut
      else
        (* According to the documentation of Context.fold,
           "[f] is never called with an empty key for values; i.e.,
             folding over a value is a no-op".
           Therefore, we first need to check that whether its a value.
        *)
        Context.find context path >>= function
        | Some v -> Lwt.return (Block_services.Key v)
        | None ->
            (* try to read as directory *)
            Context.fold
              ~depth:(`Le depth)
              context
              path
              ~order:`Sorted
              ~init
              ~f:(fun k tree acc ->
                let open Block_services in
                if List.compare_length_with k depth >= 0 then
                  (* only [=] case is possible because [~depth] is [(`Le depth)] *)
                  Lwt.return (raw_context_insert (k, Cut) acc)
                else
                  Context.Tree.to_value tree >|= function
                  | None -> acc
                  | Some v -> raw_context_insert (k, Key v) acc)

  let handler rpc_context path q () =
    let depth = Option.value ~default:max_int q#depth in
    (* [depth] is defined as a [uint] not an [int] *)
    assert (depth >= 0) ;
    let context =
      Shell_context.unwrap_disk_context
        rpc_context.Tezos_protocol_environment.context
    in
    Context.mem context path >>= fun mem ->
    Context.mem_tree context path >>= fun dir_mem ->
    if not (mem || dir_mem) then Lwt.fail Not_found
    else read_partial_context context path depth >>= Lwt.return_ok

  let raw_context_encoding =
    let open Data_encoding in
    let open Tezos_shell_services.Block_services in
    mu "raw_context" (fun encoding ->
        union
          [
            case
              (Tag 0)
              bytes
              ~title:"Key"
              (function Key k -> Some k | _ -> None)
              (fun k -> Key k);
            case
              (Tag 1)
              (assoc encoding)
              ~title:"Dir"
              (function
                | Dir map -> Some (TzString.Map.bindings map) | _ -> None)
              (fun bindings ->
                Dir
                  (List.fold_left
                     (fun wip_map (k, v) -> TzString.Map.add k v wip_map)
                     TzString.Map.empty
                     bindings));
            case
              (Tag 2)
              null
              ~title:"Cut"
              (function Cut -> Some () | _ -> None)
              (fun () -> Cut);
          ])

  let context_path_arg : string RPC_arg.t =
    let name = "context_path" in
    let descr = "A path inside the context" in
    let construct s = s in
    let destruct s = Ok s in
    RPC_arg.make ~name ~descr ~construct ~destruct ()

  let raw_context_query : < depth : int option > RPC_query.t =
    let open RPC_query in
    query (fun depth ->
        object
          method depth = depth
        end)
    |+ opt_field "depth" RPC_arg.uint (fun t -> t#depth)
    |> seal

  let base_path = RPC_path.open_root

  let read =
    RPC_service.get_service
      ~description:"Returns the raw context."
      ~query:raw_context_query
      ~output:raw_context_encoding
      RPC_path.(base_path / "context" / "raw" / "bytes" /:* context_path_arg)

  let adjust_rpc_context = RPC_directory.map (fun c -> Lwt.return ((), c))

  let directory =
    adjust_rpc_context
      (RPC_directory.register2 RPC_directory.empty read handler)
end

(* Dynamic directory to obtain protocol services from context *)
module Protocol_service = struct
  let handler rpc_context =
    let predecessor_context = rpc_context.Tezos_protocol_environment.context in
    Context.get_protocol (Shell_context.unwrap_disk_context predecessor_context)
    >|= fun protocol_hash_from_context ->
    match Registered_protocol.get protocol_hash_from_context with
    | Some (module Next_proto : Registered_protocol.T) ->
        (* according to block_directory.ml
           1. register protocol rpc throught Plugin_registrer throught Prevalidator_filters
           2. or direct Proto.rpc_services *)
        let proto_services =
          match Prevalidator_filters.find Next_proto.hash with
          | Some (module Filters) -> Filters.RPC.rpc_services
          | None -> Next_proto.rpc_services
        in
        let proto_services_with_cache =
          RPC_directory.map
            (fun rpc_context ->
              let chain_id =
                Chain_id.of_block_hash
                  rpc_context.Tezos_protocol_environment.block_hash
              in
              let Block_header.
                    {
                      timestamp = predecessor_timestamp;
                      level = predecessor_level;
                      fitness = predecessor_fitness;
                      _;
                    } =
                rpc_context.block_header
              in
              let timestamp = Time.System.to_protocol (Ptime_clock.now ()) in
              (Next_proto.value_of_key
                 ~chain_id
                 ~predecessor_context:
                   rpc_context.Tezos_protocol_environment.context
                 ~predecessor_timestamp
                 ~predecessor_level
                 ~predecessor_fitness
                 ~predecessor:rpc_context.block_hash
                 ~timestamp
               >>= function
               | Error trace -> Lwt.fail (Internal_error trace)
               | Ok value_of_key ->
                   Tezos_protocol_environment.Context.load_cache
                     rpc_context.block_hash
                     predecessor_context
                     `Lazy
                     value_of_key)
              >>= function
              | Error trace -> Lwt.fail (Internal_error trace)
              | Ok context ->
                  let result = {rpc_context with context} in
                  Lwt.return result)
            proto_services
        in
        (* We merge it here with the handler for `../context/raw/bytes/*` *)
        RPC_directory.merge
          proto_services_with_cache
          Raw_context_service.directory
    | None -> RPC_directory.empty

  let directory =
    RPC_directory.register_dynamic_directory
      RPC_directory.empty
      RPC_path.open_root
      handler
end

(* We want to convert the block header into a proper context to be used by the handlers. *)
let adjust_rpc_context directory =
  RPC_directory.map
    (fun ((block_header, _chain_id), _block_id) ->
      Ffi_rpc_service.checkout_context_or_fail
        block_header.Block_header.shell.context
      >>= function
      | Error trace -> Lwt.fail (Internal_error trace)
      | Ok context ->
          Lwt.return
          @@ Tezos_protocol_environment.
               {
                 block_hash = Block_header.hash block_header;
                 block_header = block_header.shell;
                 context = Shell_context.wrap_disk_context context;
               })
    directory

(* Dummy root directory with /chains/$chain_id/blocks/$block_id/ prefix that is going to resolve
   to the protocol's RPC services directory (if an empty directory if the checkout fails) *)
let root_directory =
  let path =
    RPC_path.(
      open_root / "chains" /: RPC_arg.string / "blocks" /: RPC_arg.string)
  in
  RPC_directory.prefix path (adjust_rpc_context Protocol_service.directory)

(* TODO: use content_type value if provided *)
let input_encoding_from_content_type = function
  | None -> Tezos_rpc_http.Media_type.json
  | Some _content_type -> Tezos_rpc_http.Media_type.json

(* TODO: use accept value if provided *)
let output_encoding_from_accept = function
  | None -> Tezos_rpc_http.Media_type.json
  | Some _accept -> Tezos_rpc_http.Media_type.json

let parse_query query_type query =
  let query = List.map (fun (k, l) -> (k, String.concat "," l)) query in
  match RPC_query.parse query_type query with
  | exception RPC_query.Invalid s -> Error (RPC_Error_Cannot_parse_query s)
  | query -> ok query

(* TODO: assumes the streams returns a single element *)
let consume_stream {RPC_answer.next; shutdown} =
  next () >>= function
  | None ->
      shutdown () ;
      Lwt.return_none
  | Some s ->
      shutdown () ;
      Lwt.return_some s

(* TODO: remove once ocaml-interop adds support for polymorphic variants*)
module FFI_http_method = struct
  let from_resto = function
    | `DELETE -> DELETE
    | `GET -> GET
    | `PATCH -> PATCH
    | `POST -> POST
    | `PUT -> PUT

  let to_resto = function
    | DELETE -> `DELETE
    | GET -> `GET
    | PATCH -> `PATCH
    | POST -> `POST
    | PUT -> `PUT
end

(* TODO: remove once ocaml-interop adds support for polymorphic variants*)
module FFI_rpc_error = struct
  let from_resto err =
    match err with
    | `Cannot_parse_body s -> RPC_Error_Cannot_parse_body s
    | `Cannot_parse_path (p, d, s) -> RPC_Error_Cannot_parse_path (p, d, s)
    | `Cannot_parse_query s -> RPC_Error_Cannot_parse_query s
    | `Method_not_allowed ms ->
        RPC_Error_Method_not_allowed (List.map FFI_http_method.from_resto ms)
    | `Not_found -> RPC_Error_Service_Not_found
end

(* TODO: remove once ocaml-interop adds support for polymorphic variants*)
let return_rpc_error err = Lwt.return_error (FFI_rpc_error.from_resto err)

let route_protocol_rpc request =
  let {block_header; request; _} = request in
  let meth = FFI_http_method.to_resto request.meth in
  let uri = Uri.of_string request.context_path in
  (* NOTE: pct_decode necessary at the moment it is not done on the Rust side *)
  let path = uri |> Uri.path |> String.split_path |> List.map Uri.pct_decode in
  let query = request.context_path |> Uri.of_string |> Uri.query in
  let input_encoding = input_encoding_from_content_type request.content_type in
  let output_encoding = output_encoding_from_accept request.accept in
  RPC_directory.lookup root_directory block_header meth path >>= function
  | Error error -> return_rpc_error error
  | Ok (Service service) -> (
      match parse_query service.types.query query with
      | Error error -> Lwt.return_error error
      | Ok query -> (
          let decode_input = input_encoding.destruct in
          let encode_output = output_encoding.construct service.types.output in
          let encode_error = output_encoding.construct service.types.error in
          let encode_error_opt = Option.map encode_error in
          (match service.types.input with
          | RPC_service.No_input -> service.handler query () >>= return
          | RPC_service.Input input -> (
              match decode_input input request.body with
              | Error s -> Lwt.return_error (`Cannot_parse_body s)
              | Ok body -> service.handler query body >>= return))
          >>= function
          | Error error -> return_rpc_error error
          | Ok result -> (
              match result with
              (* TODO: Lwt.return_error in some of these? *)
              | `Unauthorized _ -> return RPC_Unauthorized
              | `Gone err -> return (RPC_Gone (encode_error_opt err))
              | `Error err -> return (RPC_Error (encode_error_opt err))
              | `Not_found err -> return (RPC_Not_found (encode_error_opt err))
              | `Forbidden err -> return (RPC_Forbidden (encode_error_opt err))
              | `Created s -> return (RPC_Created s)
              | `Conflict err -> return (RPC_Conflict (encode_error_opt err))
              | `No_content -> return RPC_No_content
              | `Ok result -> return (RPC_Ok (encode_output result))
              | `OkStream stream -> (
                  (* NOTE: not used at the moment *)
                  consume_stream stream
                  >>= function
                  | None ->
                      return RPC_No_content
                      (* TODO: what is the correct return value here? can it happen? *)
                  | Some result -> return (RPC_Ok (encode_output result)))
              | `OkChunk chunks ->
                  (* TODO: maybe look into streaming this, right now we conver the seq to a string *)
                  let buf = Buffer.create 4096 in
                  let chunks_seq =
                    output_encoding.construct_seq service.types.output chunks
                  in
                  Seq.iter_s
                    (fun (bytes, ofs, len) ->
                      Buffer.add_subbytes buf bytes ofs len ;
                      Lwt_unix.yield ())
                    chunks_seq
                  >>= fun () ->
                  let output = Buffer.contents buf in
                  return (RPC_Ok output))))

let route_protocol_rpc request =
  Lwt.catch
    (fun () -> route_protocol_rpc request)
    (function
      | Not_found ->
          let backtrace = Printexc.get_backtrace () in
          Printf.eprintf
            "Not_found exception when serving an RPC %s, backtrace: %s\n"
            request.request.context_path
            backtrace ;
          return (RPC_Not_found None)
      | Internal_error trace ->
          let json =
            Data_encoding.Json.construct Error_monad.trace_encoding trace
          in
          let trace_json = Ezjsonm.value_to_string json in
          return (RPC_Error (Some trace_json))
      | exn -> Lwt.fail exn)

let call_protocol_rpc request =
  Lwt.catch
    (fun () -> route_protocol_rpc request)
    (fun exn ->
      let name = Printexc.to_string exn in
      let backtrace = Printexc.get_backtrace () in
      let description = Printf.sprintf "%s: %s" name backtrace in
      return (RPC_Error (Some description)))

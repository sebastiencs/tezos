(* Mapping into regular variants so that ocaml-interop can handle these values.
   Once ocaml-interop gains support for polymorphic variants this can be removed *)
type rpc_method = DELETE | GET | PATCH | POST | PUT

type rpc_response =
  | RPC_Conflict of string option  (** 409 *)
  | RPC_Created of string option  (** 201*)
  | RPC_Error of string option  (** 500 *)
  | RPC_Forbidden of string option  (** 403 *)
  | RPC_Gone of string option  (** 410 *)
  | RPC_No_content  (** 204 *)
  | RPC_Not_found of string option  (** 404 *)
  | RPC_Ok of string  (** 200 *)
  | RPC_Unauthorized  (** 401 *)

type rpc_response_error =
  | RPC_Error_Cannot_parse_body of string  (** 400 *)
  | RPC_Error_Cannot_parse_path of string list * RPC_arg.descr * string
      (** 400 *)
  | RPC_Error_Cannot_parse_query of string  (** 400 *)
  | RPC_Error_Method_not_allowed of rpc_method list  (** 405 *)
  | RPC_Error_Service_Not_found  (** 404 *)

type rpc_request = {
  body : string;
  context_path : string;
  meth : rpc_method;
  content_type : string option;
  accept : string option;
}

type helpers_preapply_response = {body : string}

type protocol_rpc_request = {
  block_header : Block_header.t;
  chain_id : Chain_id.t;
  (* see Block_services.parse_chain - chain from rpc url e.g. /chains/main... *)
  chain_arg : string;
  request : rpc_request;
}

type helpers_preapply_block_request = {
  protocol_rpc_request : protocol_rpc_request;
  predecessor_block_metadata_hash : Block_metadata_hash.t option;
  predecessor_ops_metadata_hash : Operation_metadata_list_list_hash.t option;
  predecessor_max_operations_ttl: int;
}

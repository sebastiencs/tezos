module Context_init = struct
  let ensure_dir_exists dir =
    match Sys.is_directory dir with
    | exception Sys_error _ -> Unix.mkdir dir 0o755
    | false -> Unix.mkdir dir 0o755
    | true -> ()

  type patch_context_key_json = string * Data_encoding.json

  let init_chain_genesis genesis_params : Genesis.t =
    let (genesis_time, genesis_block, genesis_protocol) = genesis_params in
    let chain_genesis : Genesis.t =
      {
        time = Time.Protocol.of_notation_exn genesis_time;
        block = Block_hash.of_b58check_exn genesis_block;
        protocol = Protocol_hash.of_b58check_exn genesis_protocol;
      }
    in
    chain_genesis

  let init_context ?irmin_config ?tezedge_config data_dir genesis
      sandbox_json_patch_context =
    if Option.is_some irmin_config then ensure_dir_exists data_dir ;
    let context_root = Filename.concat data_dir "context" in
    (* according to state.ml State.init *)
    let chain_genesis = init_chain_genesis genesis in
    let chain_id = Chain_id.of_block_hash chain_genesis.block in
    let sandbox_json_patch_context =
      match sandbox_json_patch_context with
      | Some (key, json) ->
          Some (key, (Ezjsonm.from_string json :> Data_encoding.json))
      | None -> None
    in
    (* initialize context for write *)
    Context.init
      ?irmin_config
      ?tezedge_config
      ~readonly:false
      ~patch_context:
        (Patch_context.patch_context chain_genesis sandbox_json_patch_context)
      context_root
    >>= fun context_index ->
    (* prepare commit_genesis promise *)
    (let commit_genesis ~chain_id ~time ~protocol =
       Context.commit_genesis context_index ~chain_id ~time ~protocol
     in
     Lwt.return (context_index, commit_genesis))
    >>= fun (context_index, commit_genesis) ->
    ( commit_genesis
        ~chain_id
        ~time:chain_genesis.time
        ~protocol:chain_genesis.protocol
    >>=? fun commit ->
      Lwt.return_ok (context_index, chain_genesis.protocol, chain_id, commit) )
    >>= function
    | Ok (context_index, _, _, context_hash) ->
        Lwt.return_ok (context_index, context_hash)
    | Error error -> Format.kasprintf Lwt.return_error "%a" pp_print_trace error
end

module WrappedContext = struct
  let run = Lwt_main.run

  let init_irmin data_dir genesis sandbox_json_patch_context =
    run
    @@ Context_init.init_context
         ~irmin_config:data_dir
         data_dir
         genesis
         sandbox_json_patch_context

  let init_tezedge data_dir genesis sandbox_json_patch_context =
    let (tezedge_config, data_dir) =
      match data_dir with
      | ":inmem:" ->
          (* FIXME: hardcoded path here, we should be able to specify it somehow *)
          let base_path = "/tmp/tezedge-inmem-context" in
          ( Tezedge_context.Config.
              {
                backend = InMem {base_path; startup_check = false};
                ipc_socket_path = None;
              },
            base_path )
      | base_path ->
          ( Tezedge_context.Config.
              {
                backend = OnDisk {base_path; startup_check = false};
                ipc_socket_path = None;
              },
            base_path )
    in
    run
    @@ Context_init.init_context
         ~tezedge_config
         data_dir
         genesis
         sandbox_json_patch_context

  let close index = run @@ Context.close index

  let mem ctxt key = run @@ Context.mem ctxt key

  let mem_tree ctxt key = run @@ Context.mem_tree ctxt key

  let find ctxt key = run @@ Context.find ctxt key

  let find_tree ctxt key = run @@ Context.find_tree ctxt key

  let add ctxt key value = run @@ Context.add ctxt key value

  let add_tree ctxt key tree = run @@ Context.add_tree ctxt key tree

  let remove ctxt key = run @@ Context.remove ctxt key

  let hash time message ctxt = Context.hash ~time ?message ctxt

  let list ctxt offset length key = Context.list ctxt ?offset ?length key

  (* TODO: fold *)

  let checkout index ctxt_hash = run @@ Context.checkout index ctxt_hash

  let commit time message ctxt = run @@ Context.commit ~time ~message ctxt

  (* Tree manipulation *)

  module Tree = struct
    let mem tree key = run @@ Context.Tree.mem tree key

    let mem_tree tree key = run @@ Context.Tree.mem_tree tree key

    let find tree key = run @@ Context.Tree.find tree key

    let find_tree tree key = run @@ Context.Tree.find_tree tree key

    let add tree key value = run @@ Context.Tree.add tree key value

    let add_tree tree key subtree =
      run @@ Context.Tree.add_tree tree key subtree

    let remove tree key = run @@ Context.Tree.remove tree key

    let empty ctxt = Context.Tree.empty ctxt

    let is_empty tree = Context.Tree.is_empty tree

    let hash tree = Context.Tree.hash tree

    let list tree offset length key = Context.Tree.list tree ?offset ?length key

    (* TODO: fold *)

    let to_string tree = Format.asprintf "%a" Context.Tree.pp tree
  end

  (* Special keys *)

  let get_protocol ctxt = run @@ Context.get_protocol ctxt

  let add_protocol ctxt proto_hash = run @@ Context.add_protocol ctxt proto_hash

  let get_test_chain ctxt = run @@ Context.get_test_chain ctxt

  let add_test_chain ctxt id = run @@ Context.add_test_chain ctxt id

  let remove_test_chain ctxt = run @@ Context.remove_test_chain ctxt
end

let export_tezos_context_ffi () =
  (* Init *)
  Callback.register "tezos_context_init_irmin" WrappedContext.init_irmin ;
  Callback.register "tezos_context_init_tezedge" WrappedContext.init_tezedge ;
  Callback.register "tezos_context_close" WrappedContext.close ;
  (* Context query *)
  Callback.register "tezos_context_mem" WrappedContext.mem ;
  Callback.register "tezos_context_mem_tree" WrappedContext.mem_tree ;
  Callback.register "tezos_context_find" WrappedContext.find ;
  Callback.register "tezos_context_find_tree" WrappedContext.find_tree ;
  Callback.register "tezos_context_add" WrappedContext.add ;
  Callback.register "tezos_context_add_tree" WrappedContext.add_tree ;
  Callback.register "tezos_context_remove" WrappedContext.remove ;
  Callback.register "tezos_context_list" WrappedContext.list ;
  Callback.register "tezos_context_hash" WrappedContext.hash ;
  (* Repository *)
  Callback.register "tezos_context_checkout" WrappedContext.checkout ;
  Callback.register "tezos_context_commit" WrappedContext.commit ;
  (* Tree *)
  Callback.register "tezos_context_tree_mem" WrappedContext.Tree.mem ;
  Callback.register "tezos_context_tree_mem_tree" WrappedContext.Tree.mem_tree ;
  Callback.register "tezos_context_tree_find" WrappedContext.Tree.find ;
  Callback.register "tezos_context_tree_find_tree" WrappedContext.Tree.find_tree ;
  Callback.register "tezos_context_tree_add" WrappedContext.Tree.add ;
  Callback.register "tezos_context_tree_add_tree" WrappedContext.Tree.add_tree ;
  Callback.register "tezos_context_tree_remove" WrappedContext.Tree.remove ;
  Callback.register "tezos_context_tree_empty" WrappedContext.Tree.empty ;
  Callback.register "tezos_context_tree_is_empty" WrappedContext.Tree.is_empty ;
  Callback.register "tezos_context_tree_list" WrappedContext.Tree.list ;
  Callback.register "tezos_context_tree_hash" WrappedContext.Tree.hash ;
  Callback.register "tezos_context_tree_to_string" WrappedContext.Tree.to_string ;
  (* Special *)
  Callback.register "tezos_context_get_protocol" WrappedContext.get_protocol ;
  Callback.register "tezos_context_add_protocol" WrappedContext.add_protocol ;
  Callback.register "tezos_context_get_test_chain" WrappedContext.get_test_chain ;
  Callback.register "tezos_context_add_test_chain" WrappedContext.add_test_chain ;
  Callback.register
    "tezos_context_remove_test_chain"
    WrappedContext.remove_test_chain

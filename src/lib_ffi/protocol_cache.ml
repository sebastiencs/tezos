(* Protocol cache, will be updated after each successful block application
   and inherited by the next block. This will be used by apply and preapply *)
let cache : Environment_context.Context.block_cache option ref = ref None

(* TODO: this should be done automatically, just by adding reigsterer to lib_ffi/dune, like:
    tezos-protocol-plugin-010-PtGRANAD-registerer
    ...
    flasg -open Tezos_protocol_plugin_010_PtGRANAD_registerer
    ...
    but for some reason, it did not work,
    so we need to register them manually
*)
let register () =
  (* 008_PtEdo2Zk *)
  (let open Tezos_protocol_plugin_008_PtEdo2Zk_registerer in
  match Prevalidator_filters.find Encodings_008_PtEdo2Zk.protocol_hash with
  | Some _ ->
      (* ok, protocol plugin already registered *)
      ()
  | None ->
      (* protocol plugin not already registered, so we register it *)
      Prevalidator_filters.register (module Plugin_registerer.Plugin)) ;

  (* 009_PsFLoren *)
  (let open Tezos_protocol_plugin_009_PsFLoren_registerer in
  match Prevalidator_filters.find Encodings_009_PsFLoren.protocol_hash with
  | Some _ ->
      (* ok, protocol plugin already registered *)
      ()
  | None ->
      (* protocol plugin not already registered, so we register it *)
      Prevalidator_filters.register (module Plugin_registerer.Plugin)) ;

  (* 010_PtGRANAD *)
  (let open Tezos_protocol_plugin_010_PtGRANAD_registerer in
  match Prevalidator_filters.find Encodings_010_PtGRANAD.protocol_hash with
  | Some _ ->
      (* ok, protocol plugin already registered *)
      ()
  | None ->
      (* protocol plugin not already registered, so we register it *)
      Prevalidator_filters.register (module Plugin_registerer.Plugin)) ;

  (* 011_PtHangz2 *)
  (let open Tezos_protocol_plugin_011_PtHangz2_registerer in
  match Prevalidator_filters.find Encodings_011_PtHangz2.protocol_hash with
  | Some _ ->
      (* ok, protocol plugin already registered *)
      ()
  | None ->
      (* protocol plugin not already registered, so we register it *)
      Prevalidator_filters.register (module Plugin_registerer.Plugin)) ;

  (* 012_Psithaca *)
  let open Tezos_protocol_plugin_012_Psithaca_registerer in
  match Prevalidator_filters.find Encodings_012_Psithaca.protocol_hash with
  | Some _ ->
      (* ok, protocol plugin already registered *)
      ()
  | None ->
      (* protocol plugin not already registered, so we register it *)
      Prevalidator_filters.register (module Plugin_registerer.Plugin)

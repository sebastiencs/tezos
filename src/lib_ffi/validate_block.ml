open Tezos_base__TzPervasives
open Ffi_errors
open Ffi_logger

type begin_application_request = {
  chain_id : Chain_id.t;
  pred_header : Block_header.t;
  block_header : Block_header.t;
}

type begin_application_response = {result : string}

type begin_construction_request = {
  chain_id : Chain_id.t;
  (* we need whole header because of update_testchain_status *)
  predecessor : Block_header.t;
  (* predecessor block hash *)
  predecessor_hash : Block_hash.t;
  protocol_data : Bytes.t option;
}

type prevalidator_wrapper = {
  chain_id : Chain_id.t;
  protocol : Protocol_hash.t;
  predecessor : Block_hash.t;
}

type prevalidator_request = {
  prevalidator : prevalidator_wrapper;
  operation_hash : Operation_hash.t;
  operation : Operation.t;
}

(* Classification with JSON strings instead of error traces *)
type operation_classification =
  [ `Applied
  | `Prechecked
  | `Branch_delayed of string
  | `Branch_refused of string
  | `Refused of string
  | `Outdated of string ]

type classified_operation = {
  classification: operation_classification;
  operation_data_json: string;
  is_endorsement: bool;
}

(* TODO: move parsed json string to pre-filter once we start using it *)
type validate_operation_result =
  | Unparseable
  | Classified of classified_operation

type validate_operation_response = {
  prevalidator : prevalidator_wrapper;
  operation_hash : Operation_hash.t;
  result : validate_operation_result;
  validate_operation_started_at : float;
  parse_operation_started_at : float;
  parse_operation_ended_at : float;
  validate_operation_ended_at : float;
}

type pre_filter_result =
  [`Unparseable | `Drop | Ffi_prevalidator_pending_operations.priority]

type pre_filter_operation_response = {
  prevalidator : prevalidator_wrapper;
  operation_hash : Operation_hash.t;
  (* operation_data_json : string; *)
  (* TODO: enable once we start using pre-filtering *)
  result : pre_filter_result;
  pre_filter_operation_started_at : float;
  parse_operation_started_at : float;
  parse_operation_ended_at : float;
  pre_filter_operation_ended_at : float;
}

let prevalidator_wrapper_encoding =
  let open Data_encoding in
  def
    "Validate_block.prevalidator_wrapper_encoding"
    ~title:"prevalidator_wrapper_encoding"
    ~description:"prevalidator_wrapper_encoding"
  @@ conv
       (fun {chain_id; protocol; predecessor} ->
         (chain_id, protocol, predecessor))
       (fun (chain_id, protocol, predecessor) ->
         {chain_id; protocol; predecessor})
       (obj3
          (req "chain_id" Chain_id.encoding)
          (req "protocol" Protocol_hash.encoding)
          (req "predecessor" Block_hash.encoding))

let validate_operation_response_encoding =
  let open Data_encoding in
  def
    "Validate_block.validate_operation_response"
    ~title:"validate_operation_response params"
    ~description:"Valdiate operation response"
  @@ conv
       (fun validate_operation_response ->
         let {
           prevalidator;
           operation_hash;
           result = _;
           validate_operation_started_at;
           parse_operation_started_at;
           parse_operation_ended_at;
           validate_operation_ended_at;
         } =
           validate_operation_response
         in
         ( prevalidator,
           operation_hash,
           validate_operation_started_at,
           parse_operation_started_at,
           parse_operation_ended_at,
           validate_operation_ended_at ))
       (fun ( prevalidator,
              operation_hash,
              validate_operation_started_at,
              parse_operation_started_at,
              parse_operation_ended_at,
              validate_operation_ended_at ) ->
         {
           prevalidator;
           operation_hash;
           result = Unparseable;
           validate_operation_started_at;
           parse_operation_started_at;
           parse_operation_ended_at;
           validate_operation_ended_at;
         })
       (obj6
          (req "prevalidator" prevalidator_wrapper_encoding)
          (req "operation_hash" Operation_hash.encoding)
          (* TODO: result *)
          (req "validate_operation_started_at" float)
          (req "parse_operation_started_at" float)
          (req "parse_operation_ended_at" float)
          (req "validate_operation_ended_at" float))

let begin_application (begin_application_request : begin_application_request) =
  let {chain_id; pred_header; block_header} = begin_application_request in
  let predecessor_context_hash = pred_header.shell.context in
  let storage = Storage_facade.global_storage in
  Storage_facade.context_index storage >>=? fun context_index ->
  Context.checkout context_index predecessor_context_hash
  >>= fun predecessor_block_context ->
  (match predecessor_block_context with
  | Some predecessor_block_context -> return predecessor_block_context
  | None ->
      fail
        (Unknown_predecessor_context
           (Context_hash.to_b58check predecessor_context_hash)))
  >>=? fun predecessor_block_context ->
  Context.get_protocol predecessor_block_context
  >>= fun protocol_hash_from_context ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Begin_application] Resolved predecessor protocol from context: %s"
        (Protocol_hash.to_b58check protocol_hash_from_context)) ;
  (* protocol instance *)
  (match Registered_protocol.get protocol_hash_from_context with
  | None ->
      fail
        (Ffi_call_error
           ("Unavailable_protocol "
           ^ Protocol_hash.to_b58check protocol_hash_from_context))
  | Some proto -> return proto)
  >>=? fun (module Proto_by_predecessor) ->
  (* prepare block_header and evaluate protocol_data *)
  let block_header_by_protocol =
    ({
       shell = block_header.shell;
       protocol_data =
         Ffi_utils.decode_from_mbytes
           Proto_by_predecessor.block_header_data_encoding
           block_header.protocol_data;
     }
      : Proto_by_predecessor.block_header)
  in
  (* block_validation.apply *)
  let predecessor_block_context =
    Shell_context.wrap_disk_context predecessor_block_context
  in
  let cache =
    match !Protocol_cache.cache with
    | None -> `Load
    | Some cache -> `Inherited (cache, predecessor_context_hash)
  in
  Proto_by_predecessor.begin_application
    ~chain_id
    ~predecessor_context:predecessor_block_context
    ~predecessor_timestamp:pred_header.shell.timestamp
    ~predecessor_fitness:pred_header.shell.fitness
    ~cache
    block_header_by_protocol
  >>=? fun _ ->
  (* if we came here, lets consider that everything is ok *)
  return {result = "OK"}

let safe_get_prevalidator_filter hash =
  match Prevalidator_filters.find hash with
  | Some filter -> return filter
  | None -> (
      match Registered_protocol.get hash with
      | None ->
          (* This should not happen: it should be handled in the validator. *)
          Error_monad.failwith
            "missing protocol '%a' for the current block."
            Protocol_hash.pp_short
            hash
      | Some protocol ->
          let (module Proto) = protocol in
          let module Filter = Prevalidator_filters.No_filter (Proto) in
          return (module Filter : Prevalidator_filters.FILTER))

let begin_construction (begin_construction_request : begin_construction_request)
    =
  let {chain_id; predecessor; predecessor_hash; protocol_data} =
    begin_construction_request
  in
  let predecessor_context_hash = predecessor.shell.context in
  let storage = Storage_facade.global_storage in
  Storage_facade.context_index storage >>=? fun context_index ->
  Context.checkout context_index predecessor_context_hash
  >>= fun predecessor_block_context ->
  (match predecessor_block_context with
  | Some predecessor_block_context -> return predecessor_block_context
  | None ->
      fail
        (Unknown_predecessor_context
           (Context_hash.to_b58check predecessor_context_hash)))
  >>=? fun predecessor_block_context ->
  Context.get_protocol predecessor_block_context
  >>= fun protocol_hash_from_context ->
  ffi_log ~msg:(fun () ->
      Printf.sprintf
        "[Begin_construction] Resolved predecessor protocol from context: %s"
        (Protocol_hash.to_b58check protocol_hash_from_context)) ;
  (* protocol instance *)
  safe_get_prevalidator_filter protocol_hash_from_context
  >>=? fun (module Filter) ->
  Ffi_prevalidator.create (module Filter) chain_id
  >>=? fun (module Prevalidator) ->
  (* start protocol prevalidator *)
  Prevalidator.launch
    ?protocol_data
    ~chain_id
    ~predecessor_context:predecessor_block_context
    ~predecessor_header:predecessor
    ~predecessor_hash
    ()
  >>=? fun () ->
  return
    {
      chain_id;
      protocol = protocol_hash_from_context;
      predecessor = predecessor_hash;
    }

let trace_to_json trace =
  let json = Data_encoding.Json.construct Error_monad.trace_encoding trace in
  Data_encoding.Json.to_string json

let jsonify_classification_errors
    (classification : Ffi_prevalidator_classification.classification) =
  match classification with
  | `Applied -> `Applied
  | `Prechecked -> `Prechecked
  | `Branch_delayed tztrace -> `Branch_delayed (trace_to_json tztrace)
  | `Branch_refused tztrace -> `Branch_refused (trace_to_json tztrace)
  | `Refused tztrace -> `Refused (trace_to_json tztrace)
  | `Outdated tztrace -> `Outdated (trace_to_json tztrace)

let validate_operation validate_operation_request =
  let validate_operation_started_at = Unix.gettimeofday () in
  let ({prevalidator = prevalidator_wrapper; operation_hash; operation}
        : prevalidator_request) =
    validate_operation_request
  in
  (* find prevalidator *)
  safe_get_prevalidator_filter prevalidator_wrapper.protocol
  >>=? fun (module Filter) ->
  Ffi_prevalidator.create (module Filter) prevalidator_wrapper.chain_id
  >>=? fun (module Prevalidator) ->
  let parse_operation_started_at = Unix.gettimeofday () in
  let parsing_result = Prevalidator.parse operation_hash operation in
  let parse_operation_ended_at = Unix.gettimeofday () in
  (match parsing_result with
  | Error _err -> return Unparseable
  | Ok parsed_operation ->
      Prevalidator.validate_operation parsed_operation
      >>=? fun (classification, operation_data_json, is_endorsement) ->
      let classification = jsonify_classification_errors classification in
      return (Classified { classification; operation_data_json; is_endorsement }))
  >>=? fun result ->
  let validate_operation_ended_at = Unix.gettimeofday () in
  let response : validate_operation_response =
    {
      prevalidator = prevalidator_wrapper;
      operation_hash;
      result;
      validate_operation_started_at;
      parse_operation_started_at;
      parse_operation_ended_at;
      validate_operation_ended_at;
    }
  in
  return response

let pre_filter_operation pre_filter_operation_request =
  let pre_filter_operation_started_at = Unix.gettimeofday () in
  let ({prevalidator = prevalidator_wrapper; operation_hash; operation}
        : prevalidator_request) =
    pre_filter_operation_request
  in
  (* find prevalidator *)
  safe_get_prevalidator_filter prevalidator_wrapper.protocol
  >>=? fun (module Filter) ->
  Ffi_prevalidator.create (module Filter) prevalidator_wrapper.chain_id
  >>=? fun (module Prevalidator) ->
  let parse_operation_started_at = Unix.gettimeofday () in
  let parsing_result = Prevalidator.parse operation_hash operation in
  let parse_operation_ended_at = Unix.gettimeofday () in
  (match parsing_result with
  | Error _err -> return `Unparseable
  | Ok parsed_operation ->
      (Prevalidator.pre_filter_operation parsed_operation
        :> pre_filter_result tzresult Lwt.t))
  >>=? fun result ->
  let pre_filter_operation_ended_at = Unix.gettimeofday () in
  let response : pre_filter_operation_response =
    {
      prevalidator = prevalidator_wrapper;
      operation_hash;
      result;
      pre_filter_operation_started_at;
      parse_operation_started_at;
      parse_operation_ended_at;
      pre_filter_operation_ended_at;
    }
  in
  return response

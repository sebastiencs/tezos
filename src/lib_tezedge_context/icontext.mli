(* This module is an alias for the original Irmin context *)
include module type of struct
  include Tezos_context_original.Context
end
#define CAML_NAME_SPACE
#include <caml/mlvalues.h>

/* An extra layer of indirection to avoid a cyclic linking dependency.
   Code that links against tezos-ffi will reference the caml_* symbols,
   while this module references ml_* symbols. Since there
   isn't currently a way to specify link-groups in rust/cargo,
   compilation will fail without this extra level of indirection */

/*** Context functions ***/

typedef value (*ml_tezedge_context_commit_f)(value,  // time
                                             value,  // message
                                             value,  // author
                                             value); // context

typedef value (*ml_tezedge_context_hash_f)(value,  // time
                                           value,  // message
                                           value,  // author
                                           value); // context

typedef value (*ml_tezedge_context_find_tree_f)(value,  // context
                                                value); // key

typedef value (*ml_tezedge_context_add_tree_f)(value,  // context
                                               value,  // key
                                               value); // tree

typedef value (*ml_tezedge_context_remove_f)(value,  // context
                                             value); // key

typedef value (*ml_tezedge_context_add_f)(value,  // context
                                          value,  // key
                                          value); // value

typedef value (*ml_tezedge_context_find_f)(value,  // context
                                           value); // key

typedef value (*ml_tezedge_context_mem_tree_f)(value,  // context
                                               value); // key

typedef value (*ml_tezedge_context_mem_f)(value,  // context
                                          value); // key

typedef value (*ml_tezedge_context_list_f)(value,  // context
                                           value,  // offset
                                           value,  // length
                                           value); // key

typedef value (*ml_tezedge_context_get_tree_f)(value); // context
typedef value (*ml_tezedge_context_set_tree_f)(value,  // context
                                               value); // tree

typedef value (*ml_tezedge_context_empty_f)(value); // index

typedef value (*ml_tezedge_context_dump_f)(value,     // string
                                           value,     // string
                                           value);    // hash
typedef value (*ml_tezedge_context_restore_f)(value,  // string
                                              value,  // string
                                              value,  // hash
                                              value); // int

static ml_tezedge_context_commit_f ml_tezedge_context_commit = NULL;
static ml_tezedge_context_hash_f ml_tezedge_context_hash = NULL;
static ml_tezedge_context_find_tree_f ml_tezedge_context_find_tree = NULL;
static ml_tezedge_context_add_tree_f ml_tezedge_context_add_tree = NULL;
static ml_tezedge_context_remove_f ml_tezedge_context_remove = NULL;
static ml_tezedge_context_add_f ml_tezedge_context_add = NULL;
static ml_tezedge_context_find_f ml_tezedge_context_find = NULL;
static ml_tezedge_context_mem_tree_f ml_tezedge_context_mem_tree = NULL;
static ml_tezedge_context_mem_f ml_tezedge_context_mem = NULL;
static ml_tezedge_context_list_f ml_tezedge_context_list = NULL;
static ml_tezedge_context_get_tree_f ml_tezedge_context_get_tree = NULL;
static ml_tezedge_context_set_tree_f ml_tezedge_context_set_tree = NULL;
static ml_tezedge_context_empty_f ml_tezedge_context_empty = NULL;
static ml_tezedge_context_dump_f ml_tezedge_context_dump = NULL;
static ml_tezedge_context_restore_f ml_tezedge_context_restore = NULL;

value call_tezedge_context_commit(value time, value message, value author,
                                  value context) {
  return ml_tezedge_context_commit(time, message, author, context);
}

value call_tezedge_context_hash(value time, value message, value author,
                                value context) {
  return ml_tezedge_context_hash(time, message, author, context);
}

value call_tezedge_context_add_tree(value context, value key, value tree) {
  return ml_tezedge_context_add_tree(context, key, tree);
}

value call_tezedge_context_find_tree(value context, value key) {
  return ml_tezedge_context_find_tree(context, key);
}

value call_tezedge_context_remove(value context, value key) {
  return ml_tezedge_context_remove(context, key);
}

value call_tezedge_context_add(value context, value key, value value) {
  return ml_tezedge_context_add(context, key, value);
}

value call_tezedge_context_find(value context, value key) {
  return ml_tezedge_context_find(context, key);
}

value call_tezedge_context_mem_tree(value context, value key) {
  return ml_tezedge_context_mem_tree(context, key);
}

value call_tezedge_context_mem(value context, value key) {
  return ml_tezedge_context_mem(context, key);
}

value call_tezedge_context_list(value context, value offset, value length,
                                value key) {
  return ml_tezedge_context_list(context, offset, length, key);
}

value call_tezedge_context_fold(value context, value depth, value key,
                                value init, value f) {
  return ml_tezedge_context_list(context, depth, key, f);
}

value call_tezedge_context_get_tree(value context) {
  return ml_tezedge_context_get_tree(context);
}

value call_tezedge_context_set_tree(value context, value tree) {
  return ml_tezedge_context_set_tree(context, tree);
}

value call_tezedge_context_empty(value index) {
  return ml_tezedge_context_empty(index);
}

value call_tezedge_context_dump(value context_path, value context_hash,
                                value dump_path) {
  return ml_tezedge_context_dump(context_path, context_hash, dump_path);
}

value call_tezedge_context_restore(value target_path, value context_dump_path,
                                   value expected_context_hash,
                                   value nb_context_elements) {
  return ml_tezedge_context_restore(target_path, context_dump_path,
                                    expected_context_hash, nb_context_elements);
}

/*** Tree functions ***/

typedef value (*ml_tezedge_tree_hash_f)(value); // tree

typedef value (*ml_tezedge_tree_find_tree_f)(value,  // tree
                                             value); // key

typedef value (*ml_tezedge_tree_add_tree_f)(value,  // tree
                                            value,  // key
                                            value); // tree

typedef value (*ml_tezedge_tree_remove_f)(value,  // tree
                                          value); // key

typedef value (*ml_tezedge_tree_add_f)(value,  // tree
                                       value,  // key
                                       value); // value

typedef value (*ml_tezedge_tree_find_f)(value,  // tree
                                        value); // key

typedef value (*ml_tezedge_tree_mem_tree_f)(value,  // tree
                                            value); // key

typedef value (*ml_tezedge_tree_mem_f)(value,  // tree
                                       value); // key

typedef value (*ml_tezedge_tree_list_f)(value,  // tree
                                        value,  // offset
                                        value,  // length
                                        value); // key

typedef value (*ml_tezedge_tree_walker_make_f)(value,  // tree
                                               value,  // depth
                                               value,  // key
                                               value); // order

typedef value (*ml_tezedge_tree_walker_next_f)(value); // tree walker

typedef value (*ml_tezedge_tree_empty_f)(value); // tree

typedef value (*ml_tezedge_tree_to_value_f)(value); // value

typedef value (*ml_tezedge_tree_of_value_f)(value,  // context
                                            value); // value

typedef value (*ml_tezedge_tree_is_empty_f)(value); // tree

typedef value (*ml_tezedge_tree_equal_f)(value,  // tree
                                         value); // tree

typedef value (*ml_tezedge_tree_kind_f)(value); // tree

static ml_tezedge_tree_hash_f ml_tezedge_tree_hash = NULL;
static ml_tezedge_tree_find_tree_f ml_tezedge_tree_find_tree = NULL;
static ml_tezedge_tree_add_tree_f ml_tezedge_tree_add_tree = NULL;
static ml_tezedge_tree_remove_f ml_tezedge_tree_remove = NULL;
static ml_tezedge_tree_add_f ml_tezedge_tree_add = NULL;
static ml_tezedge_tree_find_f ml_tezedge_tree_find = NULL;
static ml_tezedge_tree_mem_tree_f ml_tezedge_tree_mem_tree = NULL;
static ml_tezedge_tree_mem_f ml_tezedge_tree_mem = NULL;
static ml_tezedge_tree_list_f ml_tezedge_tree_list = NULL;
static ml_tezedge_tree_walker_make_f ml_tezedge_tree_walker_make = NULL;
static ml_tezedge_tree_walker_next_f ml_tezedge_tree_walker_next = NULL;
static ml_tezedge_tree_empty_f ml_tezedge_tree_empty = NULL;
static ml_tezedge_tree_to_value_f ml_tezedge_tree_to_value = NULL;
static ml_tezedge_tree_of_value_f ml_tezedge_tree_of_value = NULL;
static ml_tezedge_tree_is_empty_f ml_tezedge_tree_is_empty = NULL;
static ml_tezedge_tree_equal_f ml_tezedge_tree_equal = NULL;
static ml_tezedge_tree_kind_f ml_tezedge_tree_kind = NULL;

value call_tezedge_tree_hash(value tree) { return ml_tezedge_tree_hash(tree); }

value call_tezedge_tree_add_tree(value tree, value key, value new_tree) {
  return ml_tezedge_tree_add_tree(tree, key, new_tree);
}

value call_tezedge_tree_find_tree(value tree, value key) {
  return ml_tezedge_tree_find_tree(tree, key);
}

value call_tezedge_tree_remove(value tree, value key) {
  return ml_tezedge_tree_remove(tree, key);
}

value call_tezedge_tree_add(value tree, value key, value value) {
  return ml_tezedge_tree_add(tree, key, value);
}

value call_tezedge_tree_find(value tree, value key) {
  return ml_tezedge_tree_find(tree, key);
}

value call_tezedge_tree_mem_tree(value tree, value key) {
  return ml_tezedge_tree_mem_tree(tree, key);
}

value call_tezedge_tree_mem(value tree, value key) {
  return ml_tezedge_tree_mem(tree, key);
}

value call_tezedge_tree_list(value tree, value offset, value length,
                             value key) {
  return ml_tezedge_tree_list(tree, offset, length, key);
}

value call_tezedge_tree_fold(value tree, value depth, value key, value init,
                             value f) {
  return ml_tezedge_tree_list(tree, depth, key, f);
}

value call_tezedge_tree_walker_make(value tree, value depth, value key, value order) {
  return ml_tezedge_tree_walker_make(tree, depth, key, order);
}

value call_tezedge_tree_walker_next(value tree_walker) {
  return ml_tezedge_tree_walker_next(tree_walker);
}

value call_tezedge_tree_empty(value context) {
  return ml_tezedge_tree_empty(context);
}

value call_tezedge_tree_to_value(value tree) {
  return ml_tezedge_tree_to_value(tree);
}

value call_tezedge_tree_of_value(value context, value value) {
  return ml_tezedge_tree_of_value(context, value);
}

value call_tezedge_tree_is_empty(value tree) {
  return ml_tezedge_tree_is_empty(tree);
}

value call_tezedge_tree_equal(value tree1, value tree2) {
  return ml_tezedge_tree_equal(tree1, tree2);
}

value call_tezedge_tree_kind(value tree) { return ml_tezedge_tree_kind(tree); }

/*** Index functions ***/

typedef value (*ml_tezedge_index_patch_context_get_f)(value); // index

typedef value (*ml_tezedge_index_checkout_f)(value,  // index
                                             value); // hash

typedef value (*ml_tezedge_index_exists_f)(value,  // index
                                           value); // hash

typedef value (*ml_tezedge_index_close_f)(value); // index

typedef value (*ml_tezedge_index_block_applied_f)(
    value,  // index
    value,  // hash option
    value); // cycle_position option

typedef value (*ml_tezedge_index_init_f)(value,  // configuration
                                         value); // patch_context

typedef value (*ml_tezedge_index_latest_context_hashes_f)(value,  // index
                                                          value); // count

static ml_tezedge_index_patch_context_get_f ml_tezedge_index_patch_context_get =
    NULL;
static ml_tezedge_index_checkout_f ml_tezedge_index_checkout = NULL;
static ml_tezedge_index_exists_f ml_tezedge_index_exists = NULL;
static ml_tezedge_index_close_f ml_tezedge_index_close = NULL;
static ml_tezedge_index_block_applied_f ml_tezedge_index_block_applied = NULL;
static ml_tezedge_index_init_f ml_tezedge_index_init = NULL;
static ml_tezedge_index_latest_context_hashes_f ml_tezedge_index_latest_context_hashes = NULL;

value call_tezedge_index_patch_context_get(value index) {
  return ml_tezedge_index_patch_context_get(index);
}

value call_tezedge_index_checkout(value index, value hash) {
  return ml_tezedge_index_checkout(index, hash);
}

value call_tezedge_index_exists(value index, value hash) {
  return ml_tezedge_index_exists(index, hash);
}

value call_tezedge_index_close(value index) {
  return ml_tezedge_index_close(index);
}

value call_tezedge_index_block_applied(value index, value hash,
                                       value cycle_position) {
  return ml_tezedge_index_block_applied(index, hash, cycle_position);
}

value call_tezedge_index_init(value configuration, value patch_context) {
  return ml_tezedge_index_init(configuration, patch_context);
}

value call_tezedge_index_latest_context_hashes(value index, value count) {
  return ml_tezedge_index_latest_context_hashes(index, count);
}

/*** Timing functions ***/

typedef value (*ml_tezedge_timings_set_block_f)(value);       // hash option
typedef value (*ml_tezedge_timings_set_protocol_f)(value);    // hash
typedef value (*ml_tezedge_timings_checkout_f)(value,         // hash
                                               double,        // irmin_time
                                               double);       // tezedge_time
typedef value (*ml_tezedge_timings_set_operation_f)(value);   // hash option
typedef value (*ml_tezedge_timings_commit_f)(value,           // hash
                                             double,          // irmin_time
                                             double);         // tezedge_time
typedef value (*ml_tezedge_timings_context_action_f)(value,   // action_name
                                                     value,   // key
                                                     double,  // irmin_time
                                                     double); // tezedge_time
typedef value (*ml_tezedge_timings_init_f)(value);            // string

static ml_tezedge_timings_set_block_f ml_tezedge_timings_set_block = NULL;
static ml_tezedge_timings_set_protocol_f ml_tezedge_timings_set_protocol = NULL;
static ml_tezedge_timings_checkout_f ml_tezedge_timings_checkout = NULL;
static ml_tezedge_timings_set_operation_f ml_tezedge_timings_set_operation =
    NULL;
static ml_tezedge_timings_commit_f ml_tezedge_timings_commit = NULL;
static ml_tezedge_timings_context_action_f ml_tezedge_timings_context_action =
    NULL;
static ml_tezedge_timings_init_f ml_tezedge_timings_init = NULL;

value call_tezedge_timings_set_block(value hash) {
  return ml_tezedge_timings_set_block(hash);
}

value call_tezedge_timings_set_protocol(value hash) {
  return ml_tezedge_timings_set_protocol(hash);
}

value call_tezedge_timings_checkout(value hash, double irmin_time,
                                    double tezedge_time) {
  return ml_tezedge_timings_checkout(hash, irmin_time, tezedge_time);
}

value call_tezedge_timings_set_operation(value hash) {
  return ml_tezedge_timings_set_operation(hash);
}

value call_tezedge_timings_commit(value hash, double irmin_time,
                                  double tezedge_time) {
  return ml_tezedge_timings_commit(hash, irmin_time, tezedge_time);
}

value call_tezedge_timings_context_action(value action_name, value key,
                                          double irmin_time,
                                          double tezedge_time) {
  return ml_tezedge_timings_context_action(action_name, key, irmin_time,
                                           tezedge_time);
}

value call_tezedge_timings_init(value path) {
  return ml_tezedge_timings_init(path);
}

// Initializer

void initialize_tezedge_context_callbacks(
    ml_tezedge_context_commit_f ml_tezedge_context_commit_ptr,
    ml_tezedge_context_hash_f ml_tezedge_context_hash_ptr,
    ml_tezedge_context_find_tree_f ml_tezedge_context_find_tree_ptr,
    ml_tezedge_context_add_tree_f ml_tezedge_context_add_tree_ptr,
    ml_tezedge_context_remove_f ml_tezedge_context_remove_ptr,
    ml_tezedge_context_add_f ml_tezedge_context_add_ptr,
    ml_tezedge_context_find_f ml_tezedge_context_find_ptr,
    ml_tezedge_context_mem_tree_f ml_tezedge_context_mem_tree_ptr,
    ml_tezedge_context_mem_f ml_tezedge_context_mem_ptr,
    ml_tezedge_context_list_f ml_tezedge_context_list_ptr,
    ml_tezedge_context_get_tree_f ml_tezedge_context_get_tree_ptr,
    ml_tezedge_context_set_tree_f ml_tezedge_context_set_tree_ptr,
    ml_tezedge_context_empty_f ml_tezedge_context_empty_ptr,
    ml_tezedge_context_dump_f ml_tezedge_context_dump_ptr,
    ml_tezedge_context_restore_f ml_tezedge_context_restore_ptr) {
  ml_tezedge_context_commit = ml_tezedge_context_commit_ptr;
  ml_tezedge_context_hash = ml_tezedge_context_hash_ptr;
  ml_tezedge_context_find_tree = ml_tezedge_context_find_tree_ptr;
  ml_tezedge_context_add_tree = ml_tezedge_context_add_tree_ptr;
  ml_tezedge_context_remove = ml_tezedge_context_remove_ptr;
  ml_tezedge_context_add = ml_tezedge_context_add_ptr;
  ml_tezedge_context_find = ml_tezedge_context_find_ptr;
  ml_tezedge_context_mem_tree = ml_tezedge_context_mem_tree_ptr;
  ml_tezedge_context_mem = ml_tezedge_context_mem_ptr;
  ml_tezedge_context_list = ml_tezedge_context_list_ptr;
  ml_tezedge_context_get_tree = ml_tezedge_context_get_tree_ptr;
  ml_tezedge_context_set_tree = ml_tezedge_context_set_tree_ptr;
  ml_tezedge_context_empty = ml_tezedge_context_empty_ptr;
  ml_tezedge_context_dump = ml_tezedge_context_dump_ptr;
  ml_tezedge_context_restore = ml_tezedge_context_restore_ptr;
}

void initialize_tezedge_tree_callbacks(
    ml_tezedge_tree_hash_f ml_tezedge_tree_hash_ptr,
    ml_tezedge_tree_find_tree_f ml_tezedge_tree_find_tree_ptr,
    ml_tezedge_tree_add_tree_f ml_tezedge_tree_add_tree_ptr,
    ml_tezedge_tree_remove_f ml_tezedge_tree_remove_ptr,
    ml_tezedge_tree_add_f ml_tezedge_tree_add_ptr,
    ml_tezedge_tree_find_f ml_tezedge_tree_find_ptr,
    ml_tezedge_tree_mem_tree_f ml_tezedge_tree_mem_tree_ptr,
    ml_tezedge_tree_mem_f ml_tezedge_tree_mem_ptr,
    ml_tezedge_tree_list_f ml_tezedge_tree_list_ptr,
    ml_tezedge_tree_walker_make_f ml_tezedge_tree_walker_make_ptr,
    ml_tezedge_tree_walker_next_f ml_tezedge_tree_walker_next_ptr,
    ml_tezedge_tree_empty_f ml_tezedge_tree_empty_ptr,
    ml_tezedge_tree_to_value_f ml_tezedge_tree_to_value_ptr,
    ml_tezedge_tree_of_value_f ml_tezedge_tree_of_value_ptr,
    ml_tezedge_tree_is_empty_f ml_tezedge_tree_is_empty_ptr,
    ml_tezedge_tree_equal_f ml_tezedge_tree_equal_ptr,
    ml_tezedge_tree_kind_f ml_tezedge_tree_kind_ptr) {
  ml_tezedge_tree_hash = ml_tezedge_tree_hash_ptr;
  ml_tezedge_tree_find_tree = ml_tezedge_tree_find_tree_ptr;
  ml_tezedge_tree_add_tree = ml_tezedge_tree_add_tree_ptr;
  ml_tezedge_tree_remove = ml_tezedge_tree_remove_ptr;
  ml_tezedge_tree_add = ml_tezedge_tree_add_ptr;
  ml_tezedge_tree_find = ml_tezedge_tree_find_ptr;
  ml_tezedge_tree_mem_tree = ml_tezedge_tree_mem_tree_ptr;
  ml_tezedge_tree_mem = ml_tezedge_tree_mem_ptr;
  ml_tezedge_tree_list = ml_tezedge_tree_list_ptr;
  ml_tezedge_tree_walker_make = ml_tezedge_tree_walker_make_ptr;
  ml_tezedge_tree_walker_next = ml_tezedge_tree_walker_next_ptr;
  ml_tezedge_tree_empty = ml_tezedge_tree_empty_ptr;
  ml_tezedge_tree_to_value = ml_tezedge_tree_to_value_ptr;
  ml_tezedge_tree_of_value = ml_tezedge_tree_of_value_ptr;
  ml_tezedge_tree_is_empty = ml_tezedge_tree_is_empty_ptr;
  ml_tezedge_tree_equal = ml_tezedge_tree_equal_ptr;
  ml_tezedge_tree_kind = ml_tezedge_tree_kind_ptr;
}

void initialize_tezedge_index_callbacks(
    ml_tezedge_index_patch_context_get_f ml_tezedge_index_patch_context_get_ptr,
    ml_tezedge_index_checkout_f ml_tezedge_index_checkout_ptr,
    ml_tezedge_index_exists_f ml_tezedge_index_exists_ptr,
    ml_tezedge_index_close_f ml_tezedge_index_close_ptr,
    ml_tezedge_index_block_applied_f ml_tezedge_index_block_applied_ptr,
    ml_tezedge_index_init_f ml_tezedge_index_init_ptr,
    ml_tezedge_index_latest_context_hashes_f ml_tezedge_index_latest_context_hashes_ptr) {
  ml_tezedge_index_patch_context_get = ml_tezedge_index_patch_context_get_ptr;
  ml_tezedge_index_checkout = ml_tezedge_index_checkout_ptr;
  ml_tezedge_index_exists = ml_tezedge_index_exists_ptr;
  ml_tezedge_index_close = ml_tezedge_index_close_ptr;
  ml_tezedge_index_block_applied = ml_tezedge_index_block_applied_ptr;
  ml_tezedge_index_init = ml_tezedge_index_init_ptr;
  ml_tezedge_index_latest_context_hashes = ml_tezedge_index_latest_context_hashes_ptr;
}

void initialize_tezedge_timing_callbacks(
    ml_tezedge_timings_set_block_f ml_tezedge_timings_set_block_ptr,
    ml_tezedge_timings_set_protocol_f ml_tezedge_timings_set_protocol_ptr,
    ml_tezedge_timings_checkout_f ml_tezedge_timings_checkout_ptr,
    ml_tezedge_timings_set_operation_f ml_tezedge_timings_set_operation_ptr,
    ml_tezedge_timings_commit_f ml_tezedge_timings_commit_ptr,
    ml_tezedge_timings_context_action_f ml_tezedge_timings_context_action_ptr,
    ml_tezedge_timings_init_f ml_tezedge_timings_init_ptr) {
  ml_tezedge_timings_set_block = ml_tezedge_timings_set_block_ptr;
  ml_tezedge_timings_set_protocol = ml_tezedge_timings_set_protocol_ptr;
  ml_tezedge_timings_checkout = ml_tezedge_timings_checkout_ptr;
  ml_tezedge_timings_set_operation = ml_tezedge_timings_set_operation_ptr;
  ml_tezedge_timings_commit = ml_tezedge_timings_commit_ptr;
  ml_tezedge_timings_context_action = ml_tezedge_timings_context_action_ptr;
  ml_tezedge_timings_init = ml_tezedge_timings_init_ptr;
}

// For IPC

typedef value (*ml_tezedge_ipc_encode_response_f)(value); // response
typedef value (*ml_tezedge_ipc_decode_request_f)(value);  // bytes

static ml_tezedge_ipc_encode_response_f ml_tezedge_ipc_encode_response = NULL;
static ml_tezedge_ipc_decode_request_f ml_tezedge_ipc_decode_request = NULL;

value call_tezedge_ipc_encode_response(value response) {
  return ml_tezedge_ipc_encode_response(response);
}

value call_tezedge_ipc_decode_request(value bytes) {
  return ml_tezedge_ipc_decode_request(bytes);
}

void initialize_tezedge_ipc_callbacks(
    ml_tezedge_ipc_encode_response_f ml_tezedge_ipc_encode_response_ptr,
    ml_tezedge_ipc_decode_request_f ml_tezedge_ipc_decode_message_ptr) {
  ml_tezedge_ipc_encode_response = ml_tezedge_ipc_encode_response_ptr;
  ml_tezedge_ipc_decode_request = ml_tezedge_ipc_decode_message_ptr;
}
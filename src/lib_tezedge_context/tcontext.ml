type t

type context = t

type index

type key = Icontext.key

type value = Icontext.value

type tree

open Errors

module Config = struct
  type ondisk_options = {base_path : string; startup_check : bool}

  type backend =
    | ReadOnlyIpc
    | InMem of ondisk_options
    | OnDisk of ondisk_options

  type t = {backend : backend; ipc_socket_path : string option}

  let show_backend = function
    | ReadOnlyIpc -> "ReadOnlyIpc"
    | InMem {base_path; startup_check} ->
        Printf.sprintf
          "InMem(base_path=%s, startup_check=%b)"
          base_path
          startup_check
    | OnDisk {base_path; startup_check} ->
        Printf.sprintf
          "OnDisk(base_path=%s, startup_check=%b)"
          base_path
          startup_check
end

module TezedgeApi = struct
  type time = int64

  type 'a tezedge_result = ('a, string) result

  module Tree = struct
    external empty : context -> tree = "call_tezedge_tree_empty"

    external to_value : tree -> value option = "call_tezedge_tree_to_value"

    external of_value : context -> value -> tree tezedge_result
      = "call_tezedge_tree_of_value"

    external is_empty : tree -> bool = "call_tezedge_tree_is_empty"

    external equal : tree -> tree -> bool tezedge_result
      = "call_tezedge_tree_equal"

    external kind : tree -> [`Value | `Tree] = "call_tezedge_tree_kind"

    external mem : tree -> key -> bool tezedge_result = "call_tezedge_tree_mem"

    external mem_tree : tree -> key -> bool = "call_tezedge_tree_mem_tree"

    external find : tree -> key -> value option tezedge_result
      = "call_tezedge_tree_find"

    external add : tree -> key -> value -> tree tezedge_result
      = "call_tezedge_tree_add"

    external remove : tree -> key -> tree tezedge_result
      = "call_tezedge_tree_remove"

    external find_tree : tree -> key -> tree option tezedge_result
      = "call_tezedge_tree_find_tree"

    external add_tree : tree -> key -> tree -> tree tezedge_result
      = "call_tezedge_tree_add_tree"

    external hash : tree -> Context_hash.t tezedge_result
      = "call_tezedge_tree_hash"

    external list :
      tree ->
      int option ->
      int option ->
      key ->
      (string * tree) list tezedge_result = "call_tezedge_tree_list"

    module Walker = struct
      type t

      external make :
        tree ->
        [`Eq of int | `Le of int | `Lt of int | `Ge of int | `Gt of int] option ->
        key ->
        [`Sorted | `Undefined] ->
        t tezedge_result = "call_tezedge_tree_walker_make"

      external next : t -> (key * tree) option = "call_tezedge_tree_walker_next"
    end
  end

  module Index = struct
    external init :
      Config.t ->
      (context -> context tzresult Lwt.t) option ->
      index tezedge_result = "call_tezedge_index_init"

    external close : index -> unit = "call_tezedge_index_close"

    external exists : index -> Context_hash.t -> bool tezedge_result
      = "call_tezedge_index_exists"

    external checkout : index -> Context_hash.t -> context option tezedge_result
      = "call_tezedge_index_checkout"

    external patch_context : index -> (context -> context tzresult Lwt.t) option
      = "call_tezedge_index_patch_context_get"

    external block_applied :
      index ->
      Context_hash.t option ->
      Int32.t * int option ->
      unit tezedge_result = "call_tezedge_index_block_applied"

    external latest_context_hashes :
      index -> int -> Context_hash.t list tezedge_result
      = "call_tezedge_index_latest_context_hashes"
  end

  (*external commit_genesis : index -> Chain_id.t -> time -> Protocol_hash.t -> (Context_hash.t, string) result = "call_tezedge_context_commit_genesis" *)
  (*external commit_test_chain_genesis : context -> Block_header.t -> Block_header.t = "call_tezedge_context_commit_test_chain_genesis" *)

  module Context = struct
    external empty : index -> context = "call_tezedge_context_empty"

    external get_tree : context -> tree = "call_tezedge_context_get_tree"

    external _set_tree : context -> tree -> context
      = "call_tezedge_context_set_tree"

    external mem : context -> key -> bool tezedge_result
      = "call_tezedge_context_mem"

    external mem_tree : context -> key -> bool = "call_tezedge_context_mem_tree"

    external find : context -> key -> value option tezedge_result
      = "call_tezedge_context_find"

    external add : context -> key -> value -> context tezedge_result
      = "call_tezedge_context_add"

    external remove : context -> key -> context tezedge_result
      = "call_tezedge_context_remove"

    external hash :
      time -> string -> string -> context -> Context_hash.t tezedge_result
      = "call_tezedge_context_hash"

    external commit :
      time -> string -> string -> context -> Context_hash.t tezedge_result
      = "call_tezedge_context_commit"

    external find_tree : context -> key -> tree option tezedge_result
      = "call_tezedge_context_find_tree"

    external add_tree : context -> key -> tree -> context tezedge_result
      = "call_tezedge_context_add_tree"

    external list :
      context ->
      int option ->
      int option ->
      key ->
      (string * tree) list tezedge_result = "call_tezedge_context_list"
  end

  (*external set_head : index -> Chain_id.t -> Context_hash.t -> unit = "call_tezedge_context_set_head"*)
  (*external set_master : index -> Context_hash.t -> unit = "call_tezedge_context_set_master"*)
end

module type S = Tezos_context_sigs.Context.S

let lwt_return_or_fail = function
  | Ok v -> Lwt.return v
  | Error msg -> Stdlib.raise (RustFailure msg)

let or_fail = function Ok v -> v | Error msg -> Stdlib.raise (RustFailure msg)

(*-- Version Access and Update -----------------------------------------------*)

let current_protocol_key = ["protocol"]

let current_test_chain_key = ["test_chain"]

let current_data_key = ["data"]

let current_predecessor_block_metadata_hash_key =
  ["predecessor_block_metadata_hash"]

let current_predecessor_ops_metadata_hash_key =
  ["predecessor_ops_metadata_hash"]

let sync _index = Lwt.return_unit

let exists index key = lwt_return_or_fail (TezedgeApi.Index.exists index key)

let checkout index key =
  lwt_return_or_fail (TezedgeApi.Index.checkout index key)

let checkout_exn index key =
  checkout index key >>= function
  | None -> Lwt.fail Not_found
  | Some p -> Lwt.return p

let commit ~time ?(message = "") context =
  let date = Time.Protocol.to_seconds time in
  let author = "Tezos" in
  (* TODO: get parents? *)
  let commit = TezedgeApi.Context.commit date message author context in
  lwt_return_or_fail commit

let hash ~time ?(message = "") context =
  let date = Time.Protocol.to_seconds time in
  let author = "Tezos" in
  let hash = TezedgeApi.Context.hash date message author context in
  or_fail hash

(*-- Generic Store Primitives ------------------------------------------------*)

let data_key key = current_data_key @ key

module Tree = struct
  let unimplemented name = Stdlib.raise (UnimplementedFFIFunction name)

  let mem t key = lwt_return_or_fail (TezedgeApi.Tree.mem t key)

  let mem_tree t key = Lwt.return (TezedgeApi.Tree.mem_tree t key)

  let find t key = lwt_return_or_fail (TezedgeApi.Tree.find t key)

  let list t ?offset ?length key =
    lwt_return_or_fail (TezedgeApi.Tree.list t offset length key)

  let add t key value = lwt_return_or_fail (TezedgeApi.Tree.add t key value)

  let remove t key = lwt_return_or_fail (TezedgeApi.Tree.remove t key)

  let find_tree t key = lwt_return_or_fail (TezedgeApi.Tree.find_tree t key)

  let add_tree t key new_tree =
    lwt_return_or_fail (TezedgeApi.Tree.add_tree t key new_tree)

  let clear ?depth:_ _tree = ()

  (* TODO *)
  let pp _ _ = unimplemented "Tree.pp"

  let empty ctxt = TezedgeApi.Tree.empty ctxt

  let equal t1 t2 = or_fail (TezedgeApi.Tree.equal t1 t2)

  let is_empty t = TezedgeApi.Tree.is_empty t

  let hash t = or_fail (TezedgeApi.Tree.hash t)

  let kind t = TezedgeApi.Tree.kind t

  let to_value t = Lwt.return (TezedgeApi.Tree.to_value t)

  let of_value ctxt v = lwt_return_or_fail (TezedgeApi.Tree.of_value ctxt v)

  let fold ?depth t k ~order ~init ~f =
    let walker = or_fail (TezedgeApi.Tree.Walker.make t depth k order) in
    let rec aux acc =
      match TezedgeApi.Tree.Walker.next walker with
      | None -> Lwt.return acc
      | Some (key, tree) -> f key tree acc >>= aux
    in
    aux init

  (* TODO: Point directly to Icontext.raw_encoding? we use the same representation *)
  type raw = [`Value of bytes | `Tree of raw TzString.Map.t]

  (*type concrete*)

  (*let rec raw_of_concrete : type a. (raw -> a) -> concrete -> a =
     fun _k -> function _ -> unimplemented ()

    and raw_of_node :
        type a. ((string * raw) Seq.t -> a) -> (string * concrete) list -> a =
     fun k -> function
      | [] ->
          k Seq.empty
      | (n, v) :: t ->
          raw_of_concrete
            (fun v ->
              raw_of_node (fun t -> k (fun () -> Seq.Cons ((n, v), t))) t)
            v*)

  (* TODO *)
  let to_raw _t = unimplemented "Tree.to_raw"

  (*let rec concrete_of_raw : type a. (concrete -> a) -> raw -> a =
     fun _k -> function _ -> unimplemented ()

    and concrete_of_node :
        type a. ((string * concrete) list -> a) -> (string * raw) Seq.t -> a =
     fun k seq ->
      match seq () with
      | Nil ->
          k []
      | Cons ((n, v), t) ->
          concrete_of_raw
            (fun v -> concrete_of_node (fun t -> k ((n, v) :: t)) t)
            v*)

  (* TODO *)
  let of_raw _ = unimplemented "Tree.of_raw"

  (* TODO: Point directly to Icontext.raw_encoding? we use the same representation *)
  let raw_encoding : raw Data_encoding.t =
    let open Data_encoding in
    mu "Tree.raw" (fun encoding ->
        let map_encoding =
          conv
            TzString.Map.bindings
            (fun bindings -> TzString.Map.of_seq (List.to_seq bindings))
            (list (tup2 string encoding))
        in
        union
          [
            case
              ~title:"tree"
              (Tag 0)
              map_encoding
              (function `Tree t -> Some t | `Value _ -> None)
              (fun t -> `Tree t);
            case
              ~title:"value"
              (Tag 1)
              bytes
              (function `Value v -> Some v | `Tree _ -> None)
              (fun v -> `Value v);
          ])

  (* TODO: implement these, used by proxy *)
  type repo = unit

  let make_repo () = unimplemented "Tree.make_repo"

  let shallow _repo _kinded_hash = unimplemented "Tree.shallow"
end

let mem ctxt key =
  lwt_return_or_fail (TezedgeApi.Context.mem ctxt (data_key key))

let mem_tree ctxt key =
  Lwt.return (TezedgeApi.Context.mem_tree ctxt (data_key key))

let raw_find ctxt key = lwt_return_or_fail (TezedgeApi.Context.find ctxt key)

let list ctxt ?offset ?length key =
  lwt_return_or_fail (TezedgeApi.Context.list ctxt offset length key)

let find ctxt key = raw_find ctxt (data_key key)

let raw_add ctxt key value =
  lwt_return_or_fail (TezedgeApi.Context.add ctxt key value)

let add ctxt key value = raw_add ctxt (data_key key) value

let raw_remove ctxt key =
  lwt_return_or_fail (TezedgeApi.Context.remove ctxt key)

let remove ctxt key = raw_remove ctxt (data_key key)

let find_tree ctxt key =
  lwt_return_or_fail (TezedgeApi.Context.find_tree ctxt (data_key key))

let add_tree ctxt key tree =
  lwt_return_or_fail (TezedgeApi.Context.add_tree ctxt (data_key key) tree)

let fold ?depth t k ~order ~init ~f =
  let tree = TezedgeApi.Context.get_tree t in
  Tree.fold ?depth tree (data_key k) ~order ~init ~f

(*-- Predefined Fields -------------------------------------------------------*)

let get_protocol v =
  raw_find v current_protocol_key >>= function
  | None -> assert false
  | Some data -> Lwt.return (Protocol_hash.of_bytes_exn data)

let add_protocol v key =
  let key = Protocol_hash.to_bytes key in
  raw_add v current_protocol_key key

let get_test_chain v =
  raw_find v current_test_chain_key >>= function
  | None -> Lwt.fail (Failure "Unexpected error (Context.get_test_chain)")
  | Some data -> (
      match Data_encoding.Binary.of_bytes Test_chain_status.encoding data with
      | Error re ->
          Format.kasprintf
            (fun s -> Lwt.fail (Failure s))
            "Error in Context.get_test_chain: %a"
            Data_encoding.Binary.pp_read_error
            re
      | Ok r -> Lwt.return r)

let add_test_chain v id =
  let id = Data_encoding.Binary.to_bytes_exn Test_chain_status.encoding id in
  raw_add v current_test_chain_key id

let remove_test_chain v = raw_remove v current_test_chain_key

let fork_test_chain v ~protocol ~expiration =
  add_test_chain v (Forking {protocol; expiration})

let find_predecessor_block_metadata_hash v =
  raw_find v current_predecessor_block_metadata_hash_key >>= function
  | None -> Lwt.return_none
  | Some data -> (
      match
        Data_encoding.Binary.of_bytes_opt Block_metadata_hash.encoding data
      with
      | None ->
          Lwt.fail
            (Failure
               "Unexpected error (Context.get_predecessor_block_metadata_hash)")
      | Some r -> Lwt.return_some r)

let add_predecessor_block_metadata_hash v hash =
  let data =
    Data_encoding.Binary.to_bytes_exn Block_metadata_hash.encoding hash
  in
  raw_add v current_predecessor_block_metadata_hash_key data

let find_predecessor_ops_metadata_hash v =
  raw_find v current_predecessor_ops_metadata_hash_key >>= function
  | None -> Lwt.return_none
  | Some data -> (
      match
        Data_encoding.Binary.of_bytes_opt
          Operation_metadata_list_list_hash.encoding
          data
      with
      | None ->
          Lwt.fail
            (Failure
               "Unexpected error (Context.get_predecessor_ops_metadata_hash)")
      | Some r -> Lwt.return_some r)

let add_predecessor_ops_metadata_hash v hash =
  let data =
    Data_encoding.Binary.to_bytes_exn
      Operation_metadata_list_list_hash.encoding
      hash
  in
  raw_add v current_predecessor_ops_metadata_hash_key data

(*-- Initialisation ----------------------------------------------------------*)

let init ?patch_context ?readonly:_ config =
  lwt_return_or_fail (TezedgeApi.Index.init config patch_context)

let close index = Lwt.return (TezedgeApi.Index.close index)

let get_branch chain_id = Format.asprintf "%a" Chain_id.pp chain_id

(* TODO: revise and verify *)
let commit_genesis index ~chain_id:_ ~time ~protocol =
  let ctxt = TezedgeApi.Context.empty index in
  (match TezedgeApi.Index.patch_context index with
  | None -> return ctxt
  | Some patch_context -> patch_context ctxt)
  >>=? fun ctxt ->
  add_protocol ctxt protocol >>= fun ctxt ->
  add_test_chain ctxt Not_running >>= fun ctxt ->
  commit ~time ~message:"Genesis" ctxt >>= fun commit_hash ->
  (* TODO: branches? *)
  (* Store.Branch.set index.repo (get_branch chain_id) commit *)
  return commit_hash

let compute_testchain_chain_id genesis =
  let genesis_hash = Block_hash.hash_bytes [Block_hash.to_bytes genesis] in
  Chain_id.of_block_hash genesis_hash

let compute_testchain_genesis forked_block =
  let genesis = Block_hash.hash_bytes [Block_hash.to_bytes forked_block] in
  genesis

let commit_test_chain_genesis ctxt (forked_header : Block_header.t) =
  let message =
    Format.asprintf "Forking testchain at level %ld." forked_header.shell.level
  in
  commit ~time:forked_header.shell.timestamp ~message ctxt
  >>= fun commit_hash ->
  let faked_shell_header : Block_header.shell_header =
    {
      forked_header.shell with
      proto_level = succ forked_header.shell.proto_level;
      predecessor = Block_hash.zero;
      validation_passes = 0;
      operations_hash = Operation_list_list_hash.empty;
      context = commit_hash;
    }
  in
  let forked_block = Block_header.hash forked_header in
  let genesis_hash = compute_testchain_genesis forked_block in
  let chain_id = compute_testchain_chain_id genesis_hash in
  let genesis_header : Block_header.t =
    {
      shell = {faked_shell_header with predecessor = genesis_hash};
      protocol_data = Bytes.create 0;
    }
  in
  let _branch = get_branch chain_id in
  (* TODO: branches? *)
  (* Store.Branch.set ctxt.index.repo branch commit *)
  Lwt.return_unit >>= fun () -> Lwt.return genesis_header

(* TODO: do we have branches? *)
let clear_test_chain _index _chain_id = Lwt.return_unit

(* TODO: do we have branches? *)
let set_head _index _chain_id _commit = Lwt.return_unit

(* TODO: do we have branches? *)
let set_master _index _commit = Lwt.return_unit

module Log = Internal_event.Legacy_logging.Make (struct
  let name = "tcontext"
end)

let empty_context index = TezedgeApi.Context.empty index

let block_applied index hash level cycle_position =
  or_fail (TezedgeApi.Index.block_applied index hash (level, cycle_position))

let latest_context_hashes index count =
  Lwt.return @@ TezedgeApi.Index.latest_context_hashes index count
(** Tezos - Versioned, block indexed (key x value) store *)

(** {2 Generic interface} *)

module type S = sig
  (** @inline *)
  include Tezos_context_sigs.Context.S
end

include S

type context = t

(** A block-indexed (key x value) store directory.  *)
type index

module Config : sig
  type ondisk_options = {base_path : string; startup_check : bool}

  type backend =
    | ReadOnlyIpc
    | InMem of ondisk_options
    | OnDisk of ondisk_options

  type t = {backend : backend; ipc_socket_path : string option}

  val show_backend : backend -> string
end

(** Open or initialize a versioned store at a given path. *)
val init :
  ?patch_context:(context -> context tzresult Lwt.t) ->
  ?readonly:bool ->
  Config.t ->
  index Lwt.t

(** Close the index. Does not fail when the context is already closed. *)
val close : index -> unit Lwt.t

(** Sync the context with disk. Only useful for read-only instances.
    Does not fail when the context is not in read-only mode. *)
val sync : index -> unit Lwt.t

val compute_testchain_chain_id : Block_hash.t -> Chain_id.t

val compute_testchain_genesis : Block_hash.t -> Block_hash.t

val commit_genesis :
  index ->
  chain_id:Chain_id.t ->
  time:Time.Protocol.t ->
  protocol:Protocol_hash.t ->
  Context_hash.t tzresult Lwt.t

val commit_test_chain_genesis :
  context -> Block_header.t -> Block_header.t Lwt.t

(** {2 Accessing and Updating Versions} *)

val exists : index -> Context_hash.t -> bool Lwt.t

val checkout : index -> Context_hash.t -> context option Lwt.t

val checkout_exn : index -> Context_hash.t -> context Lwt.t

val hash : time:Time.Protocol.t -> ?message:string -> t -> Context_hash.t

val commit :
  time:Time.Protocol.t -> ?message:string -> context -> Context_hash.t Lwt.t

val set_head : index -> Chain_id.t -> Context_hash.t -> unit Lwt.t

val set_master : index -> Context_hash.t -> unit Lwt.t

(** {2 Predefined Fields} *)

val get_protocol : context -> Protocol_hash.t Lwt.t

val add_protocol : context -> Protocol_hash.t -> context Lwt.t

val get_test_chain : context -> Test_chain_status.t Lwt.t

val add_test_chain : context -> Test_chain_status.t -> context Lwt.t

val remove_test_chain : context -> context Lwt.t

val fork_test_chain :
  context ->
  protocol:Protocol_hash.t ->
  expiration:Time.Protocol.t ->
  context Lwt.t

val clear_test_chain : index -> Chain_id.t -> unit Lwt.t

val find_predecessor_block_metadata_hash :
  context -> Block_metadata_hash.t option Lwt.t

val add_predecessor_block_metadata_hash :
  context -> Block_metadata_hash.t -> context Lwt.t

val find_predecessor_ops_metadata_hash :
  context -> Operation_metadata_list_list_hash.t option Lwt.t

val add_predecessor_ops_metadata_hash :
  context -> Operation_metadata_list_list_hash.t -> context Lwt.t

(* Extra API *)

val empty_context : index -> context

val block_applied :
  index -> Context_hash.t option -> Int32.t -> int option -> unit

val latest_context_hashes :
  index -> int -> (Context_hash.t list, string) result Lwt.t

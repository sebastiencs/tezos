let lwt_opt_iter o f = match o with Some x -> f x | None -> Lwt.return_unit

module I = Icontext
module T = Tcontext
open Errors

type error +=
  | Cannot_create_file of string
  | Cannot_open_file of string
  | Cannot_find_protocol
  | Suspicious_file of int

module Log = Internal_event.Legacy_logging.Make (struct
  let name = "context"
end)

module ListUtils = struct
  let may_cons xs x = match x with None -> xs | Some x -> x :: xs

  let merge_filter2 ?(finalize = List.rev) ?(compare = compare) ~f l1 l2 =
    let sort = List.sort compare in
    let rec merge_aux acc = function
      | ([], []) -> finalize acc
      | (r1, []) ->
          finalize acc @ List.filter_map (fun x1 -> f (Some x1) None) r1
      | ([], r2) ->
          finalize acc @ List.filter_map (fun x2 -> f None (Some x2)) r2
      | ((h1 :: t1 as r1), (h2 :: t2 as r2)) ->
          if compare h1 h2 > 0 then
            merge_aux (may_cons acc (f None (Some h2))) (r1, t2)
          else if compare h1 h2 < 0 then
            merge_aux (may_cons acc (f (Some h1) None)) (t1, r2)
          else
            (* m1 = m2 *)
            merge_aux (may_cons acc (f (Some h1) (Some h2))) (t1, t2)
    in
    merge_aux [] (sort l1, sort l2)

  let merge2 ?finalize ?compare ?(f = fun x1 _x1 -> x1) l1 l2 =
    merge_filter2
      ?finalize
      ?compare
      ~f:(fun x1 x2 ->
        match (x1, x2) with
        | (None, None) -> assert false
        | (Some x1, None) -> Some x1
        | (None, Some x2) -> Some x2
        | (Some x1, Some x2) -> Some (f x1 x2))
      l1
      l2
end

(** A (key x value) store for a given block. *)
type inner_t = Irmin_only of I.t | Tezedge_only of T.t | Both of I.t * T.t

type t = {timings : bool; inner : inner_t}

type context = t

let of_irmin_context ic = {inner = Irmin_only ic; timings = false}

let of_tezedge_context tc = {inner = Tezedge_only tc; timings = false}

let to_irmin_context_exn = function
  | {inner = Both (ic, _); _} -> ic
  | {inner = Irmin_only ic; _} -> ic
  | {inner = Tezedge_only _; _} ->
      Stdlib.failwith
        "to_irmin_context_exn: attempted to extract Irmin context from a \
         TezEdge-only context"

let to_tezedge_context_exn = function
  | {inner = Both (_, tc); _} -> tc
  | {inner = Irmin_only _; _} ->
      Stdlib.failwith
        "to_tezedge_context_exn: attempted to extract TezEdge context from a \
         Irmin-only context"
  | {inner = Tezedge_only tc; _} -> tc

(** A block-indexed (key x value) store directory.  *)
type index = {
  i : I.index option;
  t : T.index option;
  original_patch_context : (context -> context tzresult Lwt.t) option;
  hacked_patch_context : context -> context tzresult Lwt.t;
  timings_enabled : bool;
}

let get_tezedge_index i = i.t

let get_irmin_index i = i.i

(* FIXME - timings: this type should include timings flag too *)
type tree =
  | Irmin_only_tree of I.tree
  | Tezedge_only_tree of T.tree
  | Both_tree of (I.tree * T.tree)

module type S = Tezos_context_sigs.Context.S

module Timings = struct
  let enabled = ref true

  let wrap i = {i with timings_enabled = !enabled}

  external call_tezedge_timings_init : string -> unit
    = "call_tezedge_timings_init"
    [@@noalloc]

  let initialize path = call_tezedge_timings_init path

  (* TODO: option to make all these noops? *)
  external set_block : Block_hash.t option -> unit
    = "call_tezedge_timings_set_block"
    [@@noalloc]

  external set_protocol : Protocol_hash.t -> unit
    = "call_tezedge_timings_set_protocol"
    [@@noalloc]

  external set_operation : Operation_hash.t option -> unit
    = "call_tezedge_timings_set_operation"
    [@@noalloc]

  let mark_block hash = if !enabled then set_block hash

  let mark_protocol hash = if !enabled then set_protocol hash

  let mark_operation hash = if !enabled then set_operation hash

  external checkout :
    Context_hash.t -> (float[@unboxed]) -> (float[@unboxed]) -> unit
    = "" "call_tezedge_timings_checkout"
    [@@noalloc]

  let checkout hash itime ttime = if !enabled then checkout hash itime ttime

  external commit :
    Context_hash.t -> (float[@unboxed]) -> (float[@unboxed]) -> unit
    = "" "call_tezedge_timings_commit"
    [@@noalloc]

  let commit hash itime ttime = if !enabled then commit hash itime ttime

  type query_kind = Mem | MemTree | Find | FindTree | Add | AddTree | Remove

  external context_action :
    query_kind -> string list -> (float[@unboxed]) -> (float[@unboxed]) -> unit
    = "" "call_tezedge_timings_context_action"
    [@@noalloc]

  let context_action name path itime ttime =
    if !enabled then context_action name path itime ttime

  (* TODO: tree_action *)

  let time_call f =
    let start_t = Unix.gettimeofday () in
    f () >|= fun result ->
    let end_t = Unix.gettimeofday () in
    (result, end_t -. start_t)

  (* Like time_call but these versions avoid the need to allocate a closure *)
  let time_call2 f arg1 arg2 =
    let start_t = Unix.gettimeofday () in
    f arg1 arg2 >|= fun result ->
    let end_t = Unix.gettimeofday () in
    (result, end_t -. start_t)

  let time_call3 f arg1 arg2 arg3 =
    let start_t = Unix.gettimeofday () in
    f arg1 arg2 arg3 >|= fun result ->
    let end_t = Unix.gettimeofday () in
    (result, end_t -. start_t)

  let notime = -1.0
end

(** Open or initialize a versioned store at a given path. *)
let init ?irmin_config ?tezedge_config ?patch_context:original_patch_context
    ?readonly ?(collect_stats = false) context_dir =
  Timings.enabled := collect_stats ;
  (* If both irmin and tezedge configs are missing, we default to an irmin config with context_dir,
     because that is how it would work with the un-extended module interface. *)
  (* Note: from context facade we know that there will always be at least one config available,
     this work being done here is to make this module work with the interface used by the node *)
  let irmin_config =
    match (irmin_config, tezedge_config) with
    | (None, None) ->
        let data_dir = Filename.dirname context_dir in
        Some data_dir
    | _ -> irmin_config
  in
  (* [patch_context] is used only at [commit_genesis] against
     the empty context.  The code assumes this property. *)
  let patched_context = ref (None : context option) in
  (* NOTE: this hacked_patch_context is here so that the underlying contexts can
     be patched when the patch function uses xcontext functions *)
  let hacked_patch_context c =
    assert (!patched_context = None) ;
    (match original_patch_context with None -> return c | Some f -> f c)
    >>=? fun c ->
    patched_context := Some c ;
    return c
  in
  (if Option.is_none irmin_config && Option.is_none tezedge_config then
   Lwt.fail
     (Failure "At least one context (either Irmin or TezEdge) is required")
  else Lwt.return ())
  >>= fun () ->
  (match irmin_config with
  | Some data_dir ->
      let context_dir = Filename.concat data_dir "context" in
      I.init
        ~patch_context:(fun _ ->
          match !patched_context with
          | None -> assert false
          | Some {inner = Irmin_only ic | Both (ic, _); _} -> return ic
          | Some {inner = Tezedge_only _; _} -> assert false)
        ?readonly
        context_dir
      >|= fun ii -> Some ii
  | None ->
      Log.warn "Running the node without Irmin context" ;
      Lwt.return None)
  >>= fun iiopt ->
  (match tezedge_config with
  | Some cfg ->
      T.init
        ~patch_context:(fun _ ->
          match !patched_context with
          | Some {inner = Tezedge_only tc | Both (_, tc); _} -> return tc
          | _ -> assert false)
        ?readonly
        cfg
      >|= fun ti -> Some ti
  | None ->
      Log.warn "Running the node without TezEdge context" ;
      Lwt.return None)
  >>= fun tiopt ->
  Lwt.return
    {
      i = iiopt;
      t = tiopt;
      hacked_patch_context;
      original_patch_context;
      timings_enabled = false;
    }

let close {i; t; _} =
  (match i with None -> Lwt.return () | Some ii -> I.close ii) >>= fun () ->
  match t with None -> Lwt.return () | Some ti -> T.close ti

(* Using implementation from Irmin's context because both versions are the same *)
let compute_testchain_chain_id = I.compute_testchain_chain_id

(* Using implementation from Irmin's context because both versions are the same *)
let compute_testchain_genesis = I.compute_testchain_genesis

let inconsistency_fail fmt =
  let stack = Printexc.(raw_backtrace_to_string (get_callstack 20)) in
  Format.kasprintf
    (fun s ->
      Printf.printf
        "\n\t+++ INCONSISTENCY ERROR: %s\n\n==== STACKTRACE:\n%s\n\n%!"
        s
        stack ;
      (* NOTE: in Rust runner this must be caugh at the FFI boundary *)
      Stdlib.raise (InconsistencyFailure s))
    fmt

let debug_mode = ref false

let debug_counter = ref 0

let debug_output fmt =
  Format.kasprintf
    (fun s ->
      if !debug_mode then (
        Printf.printf "[xcontext debug]: %s -- %d\n%!" s !debug_counter ;
        incr debug_counter))
    fmt

(* Checks if we can take the fast path to skip timings and debug code altogheter to avoid overhead *)
let can_take_fast_path ctxt = not (ctxt.timings || !debug_mode)

let checkout_can_take_fast_path index =
  not (index.timings_enabled || !debug_mode)

let empty_context {i = iiopt; t = tiopt; _} =
  match (iiopt, tiopt) with
  | (Some ii, None) ->
      {inner = Irmin_only (I.empty_context ii); timings = false}
  | (Some ii, Some ti) ->
      {inner = Both (I.empty_context ii, T.empty_context ti); timings = false}
  | (None, Some pi) ->
      {inner = Tezedge_only (T.empty_context pi); timings = false}
  | (None, None) -> assert false

let commit_genesis ({i = iiopt; t = tiopt; hacked_patch_context; _} as index)
    ~chain_id ~time ~protocol =
  hacked_patch_context (empty_context index) >>=? fun _ ->
  (match iiopt with
  | Some ii ->
      I.commit_genesis ii ~chain_id ~time ~protocol >>=? fun hash ->
      return (Some hash)
  | None -> return None)
  (* TODO - TE-261: re-enable this as an optional check
      (* To be able to validate the commit genesis hash, we initialize
         a dummy Irmin context to commit the genesis there and get the hash *)
      (* TODO: use better tmp dir *)
      let path = "/tmp" in
      let path_to_context = Filename.concat path "context" in
      Log.lwt_log_notice
        "Using a temp context at %s to get the genesis Icontext hash"
        path
      >>= fun () ->
      (* Ugly hack to fix the Timings.enabled override caused by the dummy context
         initialized to get the genesis hash *)
      let old_timings_enabled = !Timings.enabled in
      (* The hacked_patch_context function can only be called once, use original here *)
      init
        ~irmin_config:path_to_context
        ?patch_context:original_patch_context
        path_to_context
      >>= fun i ->
      commit_genesis i ~chain_id ~time ~protocol
      >>=? fun ch ->
      Timings.enabled := old_timings_enabled ;
      Log.lwt_log_notice "Genesis Icontext hash: %a@." Context_hash.pp ch
      >>= fun () -> return ch )
  *)
  >>=?
  fun ch1 ->
  match tiopt with
  | Some ti -> (
      T.commit_genesis ti ~chain_id ~time ~protocol >>=? fun ch2 ->
      Option.iter
        (fun ch1 ->
          if ch1 <> ch2 then
            inconsistency_fail
              "commit_genesis hash mismatch I=%s T=%s"
              (Context_hash.to_b58check ch1)
              (Context_hash.to_b58check ch2))
        ch1 ;
      (* TODO: inconsistency_fail instead? *)
      (* Never fails *)
      (*debug_output "\n\t+++++ COMMIT GENESIS HASHES MATCH +++++\n" ;*)
      T.checkout ti ch2 >>= function
      | None -> assert false
      | Some _ -> return ch2)
  | None -> ( match ch1 with None -> assert false | Some ch1 -> return ch1)

let commit_test_chain_genesis context bh =
  match context.inner with
  | Tezedge_only tc -> T.commit_test_chain_genesis tc bh
  | Irmin_only ic -> I.commit_test_chain_genesis ic bh
  | Both (ic, tc) ->
      I.commit_test_chain_genesis ic bh >>= fun bh1 ->
      T.commit_test_chain_genesis tc bh >>= fun bh2 ->
      if bh1 <> bh2 then inconsistency_fail "commit_test_chain_genesis" ;
      Lwt.return bh2

type key = string list

let string_of_key key = String.concat "/" @@ "" :: key

let pp_key fmt key = Format.pp_print_string fmt (string_of_key key)

type value = Bytes.t

let show_depth depth =
  match depth with
  | None -> "None"
  | Some d -> (
      match d with
      | `Eq n -> Printf.sprintf "Eq %d" n
      | `Ge n -> Printf.sprintf "Ge %d" n
      | `Gt n -> Printf.sprintf "Gt %d" n
      | `Le n -> Printf.sprintf "Le %d" n
      | `Lt n -> Printf.sprintf "Lt %d" n)

(* Verifies that same keys were obtained in the same order *)
let verify_fold_consistency depth order key iitems titems =
  if order = `Sorted && iitems <> titems then
    let detail =
      [
        Format.sprintf
          "folding: %s (%s)@."
          (string_of_key key)
          (show_depth depth);
        Format.sprintf "Irmin@.";
      ]
      @ List.map (fun x -> Format.sprintf ".. %s@." (string_of_key x)) iitems
      @ [Format.sprintf "TezEdge@."]
      @ List.map (fun x -> Format.sprintf ".. %s@." (string_of_key x)) titems
    in
    inconsistency_fail
      "fold: %s (%s)\n%s"
      (string_of_key key)
      (show_depth depth)
      (String.concat "\n" detail)
  else ()

let fold_irmin f key tree acc =
  let tree = Irmin_only_tree tree in
  f key tree acc

let fold_tezedge f key tree acc =
  let tree = Tezedge_only_tree tree in
  f key tree acc

let collect_keys key _tree acc : key list Lwt.t = Lwt.return (key :: acc)

module Tree = struct
  let unimplemented name = Stdlib.raise (UnimplementedXcontext name)

  let map_tree tree if_ tf : tree Lwt.t =
    match tree with
    | Irmin_only_tree it -> if_ it >|= fun it -> Irmin_only_tree it
    | Tezedge_only_tree tt -> tf tt >|= fun tt -> Tezedge_only_tree tt
    | Both_tree (it, tt) ->
        if_ it >>= fun it ->
        tf tt >|= fun tt -> Both_tree (it, tt)

  let do_tree tree if_ tf check =
    match tree with
    | Irmin_only_tree it -> if_ it
    | Tezedge_only_tree tt -> tf tt
    | Both_tree (it, tt) ->
        if_ it >>= fun ires ->
        tf tt >|= fun tres ->
        check ires tres ;
        ires

  let mem t key =
    do_tree
      t
      (fun it -> I.Tree.mem it key)
      (fun tt -> T.Tree.mem tt key)
      (fun b1 b2 -> if b1 <> b2 then inconsistency_fail "Tree.mem")

  let mem_tree t key =
    do_tree
      t
      (fun it -> I.Tree.mem_tree it key)
      (fun tt -> T.Tree.mem_tree tt key)
      (fun b1 b2 -> if b1 <> b2 then inconsistency_fail "Tree.mem_tree")

  let find t key =
    debug_output "Tree.FIND %a" pp_key key ;
    do_tree
      t
      (fun it -> I.Tree.find it key)
      (fun tt -> T.Tree.find tt key)
      (fun res1 res2 ->
        if res1 <> res2 then
          inconsistency_fail
            "get %s i=%S t=%S"
            (string_of_key key)
            (match res1 with None -> "NONE" | Some v -> Bytes.to_string v)
            (match res2 with None -> "NONE" | Some v -> Bytes.to_string v))

  let compare_key (k1, _t1) (k2, _t2) = String.compare k1 k2

  let merge_both (k, t1) (_, t2) =
    match (t1, t2) with
    | (Irmin_only_tree it, Tezedge_only_tree tt) -> (k, Both_tree (it, tt))
    | (Tezedge_only_tree tt, Irmin_only_tree it) -> (k, Both_tree (it, tt))
    | _ -> assert false

  let to_irmin_only_tree (k, v) = (k, Irmin_only_tree v)

  let to_tezedge_only_tree (k, v) = (k, Tezedge_only_tree v)

  let list t ?offset ?length key =
    debug_output "Tree.LIST %a" pp_key key ;
    match t with
    | Irmin_only_tree it ->
        I.Tree.list it ?offset ?length key >|= List.map to_irmin_only_tree
    | Tezedge_only_tree tt ->
        T.Tree.list tt ?offset ?length key >|= List.map to_tezedge_only_tree
    | Both_tree (it, tt) ->
        I.Tree.list it ?offset ?length key >>= fun ires ->
        T.Tree.list tt ?offset ?length key >|= fun tres ->
        let ires = List.map to_irmin_only_tree ires in
        let tres = List.map to_tezedge_only_tree tres in
        ListUtils.merge2 ~compare:compare_key ~f:merge_both ires tres

  let add t key v =
    debug_output "Tree.ADD %a" pp_key key ;
    map_tree t (fun it -> I.Tree.add it key v) (fun tt -> T.Tree.add tt key v)

  let remove t key =
    debug_output "Tree.REMOVE %a" pp_key key ;
    map_tree t (fun it -> I.Tree.remove it key) (fun tt -> T.Tree.remove tt key)

  let find_tree t key =
    debug_output "Tree.FIND_TREE %a" pp_key key ;
    match t with
    | Irmin_only_tree it ->
        I.Tree.find_tree it key >|= Option.map (fun it -> Irmin_only_tree it)
    | Tezedge_only_tree tt ->
        T.Tree.find_tree tt key >|= Option.map (fun tt -> Tezedge_only_tree tt)
    | Both_tree (it, tt) -> (
        I.Tree.find_tree it key >>= fun ires ->
        T.Tree.find_tree tt key >|= fun tres ->
        match (ires, tres) with
        | (None, None) -> None
        | (Some it, Some tt) -> Some (Both_tree (it, tt))
        | _ -> inconsistency_fail "Tree.find_tree")

  (* TODO: maybe add more info here, which one was found? *)

  (* TODO: For Both_tree case, compare results? *)
  let add_tree t key new_tree =
    debug_output "Tree.ADD_TREE %a" pp_key key ;
    match (t, new_tree) with
    | (Irmin_only_tree it, Irmin_only_tree new_tree) ->
        I.Tree.add_tree it key new_tree >|= fun tree -> Irmin_only_tree tree
    | (Tezedge_only_tree tt, Tezedge_only_tree new_tree) ->
        T.Tree.add_tree tt key new_tree >|= fun tree -> Tezedge_only_tree tree
    | (Both_tree (it, tt), Both_tree (new_itree, new_ttree)) ->
        I.Tree.add_tree it key new_itree >>= fun itree ->
        T.Tree.add_tree tt key new_ttree >|= fun ttree ->
        Both_tree (itree, ttree)
    | _ -> assert false

  let clear ?depth tree =
    match tree with
    | Irmin_only_tree it -> I.Tree.clear ?depth it
    | Tezedge_only_tree tt -> T.Tree.clear ?depth tt
    | Both_tree (it, tt) ->
        I.Tree.clear ?depth it ;
        T.Tree.clear ?depth tt

  let pp fmt tree =
    match tree with
    | Irmin_only_tree it -> I.Tree.pp fmt it
    | Tezedge_only_tree tt -> T.Tree.pp fmt tt
    | Both_tree (it, _) -> I.Tree.pp fmt it

  let empty ctxt =
    match ctxt.inner with
    | Irmin_only ci -> Irmin_only_tree (I.Tree.empty ci)
    | Tezedge_only ct -> Tezedge_only_tree (T.Tree.empty ct)
    | Both (ci, ct) -> Both_tree (I.Tree.empty ci, T.Tree.empty ct)

  let equal t1 t2 =
    match (t1, t2) with
    | (Irmin_only_tree it1, Irmin_only_tree it2) -> I.Tree.equal it1 it2
    | (Tezedge_only_tree tt1, Tezedge_only_tree tt2) -> T.Tree.equal tt1 tt2
    | (Both_tree (it1, tt1), Both_tree (it2, tt2)) ->
        let ires = I.Tree.equal it1 it2 in
        let tres = T.Tree.equal tt1 tt2 in
        if ires <> tres then inconsistency_fail "Tree.equal" else ires
    | _ -> assert false

  let is_empty t =
    match t with
    | Irmin_only_tree it -> I.Tree.is_empty it
    | Tezedge_only_tree tt -> T.Tree.is_empty tt
    | Both_tree (it, tt) ->
        let ires = I.Tree.is_empty it in
        let tres = T.Tree.is_empty tt in
        if ires <> tres then inconsistency_fail "Tree.is_empty" else ires

  let hash t =
    match t with
    | Irmin_only_tree ti -> I.Tree.hash ti
    | Tezedge_only_tree tt -> T.Tree.hash tt
    | Both_tree (ti, tt) ->
        let ihash = I.Tree.hash ti in
        let thash = T.Tree.hash tt in
        if ihash <> thash then inconsistency_fail "Tree.hash" else ihash

  let kind t =
    match t with
    | Irmin_only_tree ti -> I.Tree.kind ti
    | Tezedge_only_tree tt -> T.Tree.kind tt
    | Both_tree (ti, tt) ->
        let ikind = I.Tree.kind ti in
        let tkind = T.Tree.kind tt in
        if ikind <> tkind then inconsistency_fail "Tree.kind" else ikind

  let to_value t =
    match t with
    | Irmin_only_tree ti -> I.Tree.to_value ti
    | Tezedge_only_tree tt -> T.Tree.to_value tt
    | Both_tree (ti, tt) ->
        let ivalue = I.Tree.to_value ti in
        let tvalue = T.Tree.to_value tt in
        if ivalue <> tvalue then inconsistency_fail "Tree.to_value" else ivalue

  let of_value ctxt v =
    match ctxt.inner with
    | Irmin_only ic -> I.Tree.of_value ic v >|= fun ti -> Irmin_only_tree ti
    | Tezedge_only tc -> T.Tree.of_value tc v >|= fun tt -> Tezedge_only_tree tt
    | Both (ic, tc) ->
        I.Tree.of_value ic v >>= fun ti ->
        T.Tree.of_value tc v >|= fun tt -> Both_tree (ti, tt)

  (* TODO *)
  let fold ?depth t key ~order ~init ~f =
    debug_output "Tree.FOLD %a" pp_key key ;
    match t with
    | Irmin_only_tree it ->
        I.Tree.fold ?depth it key ~order ~init ~f:(fold_irmin f)
    | Tezedge_only_tree tt ->
        T.Tree.fold ?depth tt key ~order ~init ~f:(fold_tezedge f)
    | Both_tree (it, tt) ->
        I.Tree.fold ?depth it key ~order ~init:[] ~f:(fold_irmin collect_keys)
        >>= fun iitems ->
        T.Tree.fold ?depth tt key ~order ~init:[] ~f:(fold_tezedge collect_keys)
        >>= fun titems ->
        verify_fold_consistency depth order key iitems titems ;
        (* Fold only using Irmin, no need to fold twice *)
        I.Tree.fold ?depth it key ~order ~init ~f:(fold_irmin f)

  type raw = [`Value of bytes | `Tree of raw TzString.Map.t]

  (*type concrete

    let rec raw_of_concrete : type a. (raw -> a) -> concrete -> a =
     fun _k -> function _ -> unimplemented ()

    and raw_of_node :
        type a. ((string * raw) Seq.t -> a) -> (string * concrete) list -> a =
     fun k -> function
      | [] ->
          k Seq.empty
      | (n, v) :: t ->
          raw_of_concrete
            (fun v ->
              raw_of_node (fun t -> k (fun () -> Seq.Cons ((n, v), t))) t)
            v*)

  (* TODO *)
  let to_raw _t = unimplemented "Tree.to_raw"

  (*let rec concrete_of_raw : type a. (concrete -> a) -> raw -> a =
     fun _k -> function _ -> unimplemented ()

    and concrete_of_node :
        type a. ((string * concrete) list -> a) -> (string * raw) Seq.t -> a =
     fun k seq ->
      match seq () with
      | Nil ->
          k []
      | Cons ((n, v), t) ->
          concrete_of_raw
            (fun v -> concrete_of_node (fun t -> k ((n, v) :: t)) t)
            v*)

  (* TODO *)
  let of_raw _ = unimplemented "Tree.of_raw"

  let raw_encoding : raw Data_encoding.t =
    let open Data_encoding in
    mu "Tree.raw" (fun encoding ->
        let map_encoding =
          conv
            TzString.Map.bindings
            (fun bindings -> TzString.Map.of_seq (List.to_seq bindings))
            (list (tup2 string encoding))
        in
        union
          [
            case
              ~title:"tree"
              (Tag 0)
              map_encoding
              (function `Tree t -> Some t | `Value _ -> None)
              (fun t -> `Tree t);
            case
              ~title:"value"
              (Tag 1)
              bytes
              (function `Value v -> Some v | `Tree _ -> None)
              (fun v -> `Value v);
          ])

  (* TODO: implement these, used by proxy *)

  type repo = unit

  let make_repo () = unimplemented "Tree.make_repo"

  let shallow _repo _kinded_hash = unimplemented "Tree.shallow"
end

let mem_fast_path ctxt key =
  match ctxt.inner with
  | Irmin_only ic -> I.mem ic key
  | Tezedge_only tc -> T.mem tc key
  | Both (ic, tc) ->
      I.mem ic key >>= fun ib ->
      T.mem tc key >|= fun tb ->
      if ib <> tb then
        inconsistency_fail "mem %s I=%b T=%b" (string_of_key key) ib tb ;
      ib

let mem_slow_path ctxt key =
  debug_output "MEM %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ic ->
      Timings.time_call2 I.mem ic key >|= fun (ib, itime) ->
      Timings.context_action Mem key itime Timings.notime ;
      ib
  | Tezedge_only tc ->
      Timings.time_call2 T.mem tc key >|= fun (tb, ttime) ->
      Timings.context_action Mem key Timings.notime ttime ;
      tb
  | Both (ic, tc) ->
      Timings.time_call2 I.mem ic key >>= fun (ib, itime) ->
      Timings.time_call2 T.mem tc key >|= fun (tb, ttime) ->
      if ib <> tb then
        inconsistency_fail "mem %s I=%b T=%b" (string_of_key key) ib tb ;
      Timings.context_action Mem key itime ttime ;
      ib

let mem ctxt key =
  if can_take_fast_path ctxt then mem_fast_path ctxt key
  else mem_slow_path ctxt key

let mem_tree_fast_path ctxt key =
  match ctxt.inner with
  | Irmin_only ic -> I.mem_tree ic key
  | Tezedge_only tc -> T.mem_tree tc key
  | Both (ic, tc) ->
      I.mem_tree ic key >>= fun ib ->
      T.mem_tree tc key >|= fun tb ->
      if ib <> tb then
        inconsistency_fail "mem_tree %s I=%b T=%b" (string_of_key key) ib tb ;
      ib

let mem_tree_slow_path ctxt key =
  debug_output "MEM_TREE %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ic ->
      Timings.time_call2 I.mem_tree ic key >|= fun (ib, itime) ->
      Timings.context_action MemTree key itime Timings.notime ;
      ib
  | Tezedge_only tc ->
      Timings.time_call2 T.mem_tree tc key >|= fun (tb, ttime) ->
      Timings.context_action MemTree key Timings.notime ttime ;
      tb
  | Both (ic, tc) ->
      Timings.time_call2 I.mem_tree ic key >>= fun (ib, itime) ->
      Timings.time_call2 T.mem_tree tc key >|= fun (tb, ttime) ->
      if ib <> tb then
        inconsistency_fail "mem_tree %s I=%b T=%b" (string_of_key key) ib tb ;
      Timings.context_action MemTree key itime ttime ;
      ib

let mem_tree ctxt key =
  if can_take_fast_path ctxt then mem_tree_fast_path ctxt key
  else mem_tree_slow_path ctxt key

let list ctxt ?offset ?length key =
  debug_output "LIST %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ic ->
      I.list ic ?offset ?length key >|= List.map Tree.to_irmin_only_tree
  | Tezedge_only tc ->
      T.list tc ?offset ?length key >|= List.map Tree.to_tezedge_only_tree
  | Both (ic, tc) ->
      I.list ic ?offset ?length key >>= fun ires ->
      T.list tc ?offset ?length key >|= fun tres ->
      let ires = List.map Tree.to_irmin_only_tree ires in
      let tres = List.map Tree.to_tezedge_only_tree tres in
      ListUtils.merge2 ~compare:Tree.compare_key ~f:Tree.merge_both ires tres

let find_fast_path ctxt key =
  match ctxt.inner with
  | Irmin_only ic ->
      Timings.time_call2 I.find ic key >|= fun (ires, itime) ->
      Timings.context_action Find key itime Timings.notime ;
      ires
  | Tezedge_only tc ->
      Timings.time_call2 T.find tc key >|= fun (tres, ttime) ->
      Timings.context_action Find key Timings.notime ttime ;
      tres
  | Both (ic, tc) ->
      Timings.time_call2 I.find ic key >>= fun (ires, itime) ->
      Timings.time_call2 T.find tc key >|= fun (tres, ttime) ->
      if ires <> tres then
        inconsistency_fail
          "get %s i=%S t=%S"
          (string_of_key key)
          (match ires with None -> "NONE" | Some v -> Bytes.to_string v)
          (match tres with None -> "NONE" | Some v -> Bytes.to_string v) ;
      Timings.context_action Find key itime ttime ;
      ires

let find_slow_path ctxt key =
  debug_output "FIND %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ic ->
      Timings.time_call2 I.find ic key >|= fun (ires, itime) ->
      Timings.context_action Find key itime Timings.notime ;
      ires
  | Tezedge_only tc ->
      Timings.time_call2 T.find tc key >|= fun (tres, ttime) ->
      Timings.context_action Find key Timings.notime ttime ;
      tres
  | Both (ic, tc) ->
      Timings.time_call2 I.find ic key >>= fun (ires, itime) ->
      Timings.time_call2 T.find tc key >|= fun (tres, ttime) ->
      if ires <> tres then
        inconsistency_fail
          "get %s i=%S t=%S"
          (string_of_key key)
          (match ires with None -> "NONE" | Some v -> Bytes.to_string v)
          (match tres with None -> "NONE" | Some v -> Bytes.to_string v) ;
      Timings.context_action Find key itime ttime ;
      ires

let find ctxt key =
  if can_take_fast_path ctxt then find_fast_path ctxt key
  else find_slow_path ctxt key

let add_fast_path ctxt key v =
  match ctxt.inner with
  | Irmin_only ic ->
      I.add ic key v >|= fun ic -> {ctxt with inner = Irmin_only ic}
  | Tezedge_only tc ->
      T.add tc key v >|= fun tc -> {ctxt with inner = Tezedge_only tc}
  | Both (ic, tc) ->
      I.add ic key v >>= fun ic ->
      T.add tc key v >|= fun tc -> {ctxt with inner = Both (ic, tc)}

let add_slow_path ctxt key v =
  debug_output "ADD %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ic ->
      Timings.time_call3 I.add ic key v >|= fun (ic, itime) ->
      Timings.context_action Add key itime Timings.notime ;
      {ctxt with inner = Irmin_only ic}
  | Tezedge_only tc ->
      Timings.time_call3 T.add tc key v >|= fun (tc, ttime) ->
      Timings.context_action Add key Timings.notime ttime ;
      {ctxt with inner = Tezedge_only tc}
  | Both (ic, tc) ->
      Timings.time_call3 I.add ic key v >>= fun (ic, itime) ->
      Timings.time_call3 T.add tc key v >|= fun (tc, ttime) ->
      Timings.context_action Add key itime ttime ;
      {ctxt with inner = Both (ic, tc)}

let add ctxt key v =
  if can_take_fast_path ctxt then add_fast_path ctxt key v
  else add_slow_path ctxt key v

let remove_fast_path ctxt key =
  match ctxt.inner with
  | Irmin_only ic ->
      I.remove ic key >|= fun ic -> {ctxt with inner = Irmin_only ic}
  | Tezedge_only tc ->
      T.remove tc key >|= fun tc -> {ctxt with inner = Tezedge_only tc}
  | Both (ic, tc) ->
      I.remove ic key >>= fun ic ->
      T.remove tc key >|= fun tc -> {ctxt with inner = Both (ic, tc)}

let remove_slow_path ctxt key =
  debug_output "REMOVE %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ic ->
      Timings.time_call2 I.remove ic key >|= fun (ic, itime) ->
      Timings.context_action Remove key itime Timings.notime ;
      {ctxt with inner = Irmin_only ic}
  | Tezedge_only tc ->
      Timings.time_call2 T.remove tc key >|= fun (tc, ttime) ->
      Timings.context_action Remove key Timings.notime ttime ;
      {ctxt with inner = Tezedge_only tc}
  | Both (ic, tc) ->
      Timings.time_call2 I.remove ic key >>= fun (ic, itime) ->
      Timings.time_call2 T.remove tc key >|= fun (tc, ttime) ->
      Timings.context_action Remove key itime ttime ;
      {ctxt with inner = Both (ic, tc)}

let remove ctxt key =
  if can_take_fast_path ctxt then remove_fast_path ctxt key
  else remove_slow_path ctxt key

let find_tree_fast_path ctxt key =
  match ctxt.inner with
  | Irmin_only ci ->
      I.find_tree ci key >|= fun it ->
      Option.map (fun it -> Irmin_only_tree it) it
  | Tezedge_only ct ->
      T.find_tree ct key >|= fun tt ->
      Option.map (fun tt -> Tezedge_only_tree tt) tt
  | Both (it, tt) -> (
      I.find_tree it key >>= fun ires ->
      T.find_tree tt key >|= fun tres ->
      match (ires, tres) with
      | (None, None) -> None
      | (Some it, Some tt) -> Some (Both_tree (it, tt))
      | (oit, ott) ->
          inconsistency_fail
            "find_tree I=%b T=%b"
            (Option.is_some oit)
            (Option.is_some ott))

let find_tree_slow_path ctxt key =
  debug_output "FIND_TREE %a" pp_key key ;
  match ctxt.inner with
  | Irmin_only ci ->
      Timings.time_call2 I.find_tree ci key >|= fun (it, itime) ->
      Timings.context_action FindTree key itime Timings.notime ;
      Option.map (fun it -> Irmin_only_tree it) it
  | Tezedge_only ct ->
      Timings.time_call2 T.find_tree ct key >|= fun (tt, ttime) ->
      Timings.context_action FindTree key Timings.notime ttime ;
      Option.map (fun tt -> Tezedge_only_tree tt) tt
  | Both (it, tt) -> (
      Timings.time_call2 I.find_tree it key >>= fun (ires, itime) ->
      Timings.time_call2 T.find_tree tt key >|= fun (tres, ttime) ->
      Timings.context_action FindTree key itime ttime ;
      match (ires, tres) with
      | (None, None) -> None
      | (Some it, Some tt) -> Some (Both_tree (it, tt))
      | (oit, ott) ->
          inconsistency_fail
            "find_tree I=%b T=%b"
            (Option.is_some oit)
            (Option.is_some ott))

let find_tree ctxt key =
  if can_take_fast_path ctxt then find_tree_fast_path ctxt key
  else find_tree_slow_path ctxt key

let add_tree_fast_path ctxt key tree =
  match (ctxt.inner, tree) with
  | (Irmin_only ci, Irmin_only_tree new_tree) ->
      I.add_tree ci key new_tree >|= fun ci -> {ctxt with inner = Irmin_only ci}
  | (Tezedge_only ct, Tezedge_only_tree new_tree) ->
      T.add_tree ct key new_tree >|= fun ct ->
      {ctxt with inner = Tezedge_only ct}
  | (Both (ci, ct), Both_tree (new_itree, new_ttree)) ->
      I.add_tree ci key new_itree >>= fun ci ->
      T.add_tree ct key new_ttree >|= fun ct ->
      {ctxt with inner = Both (ci, ct)}
  | _ -> assert false

let add_tree_slow_path ctxt key tree =
  debug_output "ADD_TREE %a" pp_key key ;
  match (ctxt.inner, tree) with
  | (Irmin_only ci, Irmin_only_tree new_tree) ->
      Timings.time_call3 I.add_tree ci key new_tree >|= fun (ci, itime) ->
      Timings.context_action AddTree key itime Timings.notime ;
      {ctxt with inner = Irmin_only ci}
  | (Tezedge_only ct, Tezedge_only_tree new_tree) ->
      Timings.time_call3 T.add_tree ct key new_tree >|= fun (ct, ttime) ->
      Timings.context_action AddTree key Timings.notime ttime ;
      {ctxt with inner = Tezedge_only ct}
  | (Both (ci, ct), Both_tree (new_itree, new_ttree)) ->
      Timings.time_call3 I.add_tree ci key new_itree >>= fun (ci, itime) ->
      Timings.time_call3 T.add_tree ct key new_ttree >|= fun (ct, ttime) ->
      Timings.context_action AddTree key itime ttime ;
      {ctxt with inner = Both (ci, ct)}
  | _ -> assert false

let add_tree ctxt key tree =
  if can_take_fast_path ctxt then add_tree_fast_path ctxt key tree
  else add_tree_slow_path ctxt key tree

(* val fold :
    ?depth:[ `Eq of int | `Ge of int | `Gt of int | `Le of int | `Lt of int ] ->
    t -> key -> init:'a -> f:(key -> tree -> 'a -> 'a Lwt.t) -> 'a Lwt.t *)
let fold ?depth context key ~order ~init ~f =
  debug_output "FOLD %a" pp_key key ;
  match context.inner with
  | Irmin_only ic -> I.fold ?depth ic key ~order ~init ~f:(fold_irmin f)
  | Tezedge_only tc -> T.fold ?depth tc key ~order ~init ~f:(fold_tezedge f)
  | Both (ic, tc) ->
      I.fold ?depth ic key ~order ~init:[] ~f:(fold_irmin collect_keys)
      >>= fun iitems ->
      T.fold ?depth tc key ~order ~init:[] ~f:(fold_tezedge collect_keys)
      >>= fun titems ->
      verify_fold_consistency depth order key iitems titems ;
      (* Fold only using Irmin, no need to fold twice *)
      I.fold ?depth ic key ~order ~init ~f:(fold_irmin f)

let sync {i = iiopt; t = tiopt; _} =
  match (iiopt, tiopt) with
  | (None, None) -> assert false
  | (Some ii, None) -> I.sync ii
  | (None, Some ti) -> T.sync ti
  | (Some ii, Some ti) -> Lwt.join [I.sync ii; T.sync ti]

(* TODO: check tezedge too *)
(* TODO: timing *)
let exists {i = iiopt; t = tiopt; _} ch =
  debug_output "EXISTS" ;
  match iiopt with
  | Some ii -> I.exists ii ch
  | None -> (
      match tiopt with Some ti -> T.exists ti ch | None -> assert false)

let checkout_fast_path {i = iiopt; t = tiopt; timings_enabled = timings; _} ch =
  match (iiopt, tiopt) with
  | (None, None) -> assert false
  | (Some ii, None) ->
      I.checkout ii ch >|= fun ico ->
      Option.map (fun ic -> {inner = Irmin_only ic; timings}) ico
  | (None, Some ti) ->
      T.checkout ti ch >|= fun tco ->
      Option.map (fun tc -> {inner = Tezedge_only tc; timings}) tco
  | (Some ii, Some ti) -> (
      I.checkout ii ch >>= fun ico ->
      T.checkout ti ch >|= fun tco ->
      match (ico, tco) with
      | (Some ic, Some tc) -> Some {inner = Both (ic, tc); timings}
      | (None, None) -> None
      | (None, Some _) ->
          inconsistency_fail
            "CHECKOUT %s worked on TezEdge but not Irmin"
            (Context_hash.to_b58check ch)
      | (Some _, None) ->
          inconsistency_fail
            "CHECKOUT %s worked on Irmin but not TezEdge"
            (Context_hash.to_b58check ch))

let checkout_slow_path {i = iiopt; t = tiopt; timings_enabled = timings; _} ch =
  debug_output "CHECKOUT" ;
  match (iiopt, tiopt) with
  | (None, None) -> assert false
  | (Some ii, None) ->
      Timings.time_call2 I.checkout ii ch >|= fun (ico, itime) ->
      Timings.checkout ch itime Timings.notime ;
      Option.map (fun ic -> {inner = Irmin_only ic; timings}) ico
  | (None, Some ti) ->
      Timings.time_call2 T.checkout ti ch >|= fun (tco, ttime) ->
      Timings.checkout ch Timings.notime ttime ;
      Option.map (fun tc -> {inner = Tezedge_only tc; timings}) tco
  | (Some ii, Some ti) -> (
      Timings.time_call2 I.checkout ii ch >>= fun (ico, itime) ->
      Timings.time_call2 T.checkout ti ch >|= fun (tco, ttime) ->
      Timings.checkout ch itime ttime ;
      match (ico, tco) with
      | (Some ic, Some tc) -> Some {inner = Both (ic, tc); timings}
      | (None, None) -> None
      | (None, Some _) ->
          inconsistency_fail
            "CHECKOUT %s worked on TezEdge but not Irmin"
            (Context_hash.to_b58check ch)
      | (Some _, None) ->
          inconsistency_fail
            "CHECKOUT %s worked on Irmin but not TezEdge"
            (Context_hash.to_b58check ch))

(* FIXME: *)
let checkout i ch =
  if checkout_can_take_fast_path i then checkout_fast_path i ch
  else checkout_slow_path i ch

let checkout_exn i ch =
  checkout i ch >>= function
  | None -> Lwt.fail Not_found
  | Some x -> Lwt.return x

let get_hash_version _c = Context_hash.Version.of_int 0

let set_hash_version c v =
  if Context_hash.Version.(of_int 0 = v) then return c
  else fail (Tezos_context_helpers.Context.Unsupported_context_hash_version v)

let hash ~time ?message context =
  match context.inner with
  | Irmin_only ic -> I.hash ~time ?message ic
  | Tezedge_only tc -> T.hash ~time ?message tc
  | Both (ic, tc) ->
      let ich = I.hash ~time ?message ic in
      let tch = T.hash ~time ?message tc in
      if ich <> tch then
        inconsistency_fail
          "hash: %s != %s"
          (Context_hash.to_b58check ich)
          (Context_hash.to_b58check tch)
      else tch

let commit ~time ?message context =
  debug_output "COMMIT" ;
  match context.inner with
  | Irmin_only ic ->
      Timings.time_call (fun () -> I.commit ~time ?message ic)
      >|= fun (ich, itime) ->
      Timings.commit ich itime Timings.notime ;
      ich
  | Tezedge_only tc ->
      Timings.time_call (fun () -> T.commit ~time ?message tc)
      >|= fun (tch, ttime) ->
      Timings.commit tch Timings.notime ttime ;
      tch
  | Both (ic, tc) ->
      Timings.time_call (fun () -> I.commit ~time ?message ic)
      >>= fun (ich, itime) ->
      Timings.time_call (fun () -> T.commit ~time ?message tc)
      >>= fun (tch, ttime) ->
      Timings.commit ich itime ttime ;
      if ich <> tch then inconsistency_fail "commit" else Lwt.return ich

let set_head {i = iiopt; t = tiopt; _} cid ch =
  (* XXX NOP in T.set_head *)
  lwt_opt_iter iiopt (fun ii -> I.set_head ii cid ch) >>= fun () ->
  lwt_opt_iter tiopt (fun ti -> T.set_head ti cid ch)

let set_master {i = iiopt; t = tiopt; _} ch =
  (* XXX T.set_master fails.  This is never used. *)
  lwt_opt_iter iiopt (fun ii -> I.set_master ii ch) >>= fun () ->
  lwt_opt_iter tiopt (fun ti -> T.set_master ti ch)

(* FIXME - timings: this functino is ignoring the timings flag *)
let do_context ctxt if_ tf check =
  match ctxt.inner with
  | Irmin_only ic -> if_ ic
  | Tezedge_only tc -> tf tc
  | Both (ic, tc) ->
      if_ ic >>= fun ires ->
      tf tc >|= fun tres ->
      check ires tres ;
      ires

let merkle_tree ctxt leaf_kind key =
  match ctxt.inner with
  | Irmin_only ic -> I.merkle_tree ic leaf_kind key
  | Tezedge_only _tc -> assert false
  | Both (ic, _tc) -> I.merkle_tree ic leaf_kind key

(*-- Predefined Fields -------------------------------------------------------*)

let get_protocol context =
  do_context context I.get_protocol T.get_protocol (fun ph1 ph2 ->
      if ph1 <> ph2 then inconsistency_fail "get_protocol")

let map_context ctxt if_ tf : context Lwt.t =
  match ctxt.inner with
  | Irmin_only ic -> if_ ic >|= fun ic -> {ctxt with inner = Irmin_only ic}
  | Tezedge_only tc -> tf tc >|= fun tc -> {ctxt with inner = Tezedge_only tc}
  | Both (ic, tc) ->
      if_ ic >>= fun ic ->
      tf tc >|= fun tc -> {ctxt with inner = Both (ic, tc)}

let add_protocol context ph =
  map_context
    context
    (fun ic -> I.add_protocol ic ph)
    (fun tc -> T.add_protocol tc ph)

let get_test_chain context =
  do_context context I.get_test_chain T.get_test_chain (fun tcs1 tcs2 ->
      if tcs1 <> tcs2 then inconsistency_fail "get_test_chain")

let add_test_chain context tcs =
  map_context
    context
    (fun ic -> I.add_test_chain ic tcs)
    (fun tc -> T.add_test_chain tc tcs)

let remove_test_chain context =
  map_context context I.remove_test_chain T.remove_test_chain

let fork_test_chain context ~protocol ~expiration =
  map_context
    context
    (fun ic -> I.fork_test_chain ic ~protocol ~expiration)
    (fun tc -> T.fork_test_chain tc ~protocol ~expiration)

let clear_test_chain {i = iiopt; t = tiopt; _} cid =
  lwt_opt_iter iiopt (fun ii -> I.clear_test_chain ii cid) >>= fun () ->
  lwt_opt_iter tiopt (fun ti -> T.clear_test_chain ti cid)

let find_predecessor_block_metadata_hash context =
  do_context
    context
    I.find_predecessor_block_metadata_hash
    T.find_predecessor_block_metadata_hash
    (fun ibmh tbmh ->
      if ibmh <> tbmh then
        inconsistency_fail "get_predecessor_block_metadata_hash")

let add_predecessor_block_metadata_hash context hash =
  map_context
    context
    (fun ic -> I.add_predecessor_block_metadata_hash ic hash)
    (fun tc -> T.add_predecessor_block_metadata_hash tc hash)

let find_predecessor_ops_metadata_hash context =
  do_context
    context
    I.find_predecessor_ops_metadata_hash
    T.find_predecessor_ops_metadata_hash
    (fun ipomh tpomh ->
      if ipomh <> tpomh then
        inconsistency_fail "get_predecessor_ops_metadata_hash")

let add_predecessor_ops_metadata_hash context hash =
  map_context
    context
    (fun ic -> I.add_predecessor_ops_metadata_hash ic hash)
    (fun tc -> T.add_predecessor_ops_metadata_hash tc hash)

(*let dump_contexts {i = iiopt; t = tiopt; _} datas ~filename =
  match (iiopt, tiopt) with
  | (None, _) ->
      (* Currently not supported by Tezedge, needs Irmin *)
      assert false
  | (Some ii, _) ->
      I.dump_contexts ii datas ~filename*)

(* Protocol data *)

let retrieve_commit_info {i = iiopt; t = tiopt; _} block_header =
  match (iiopt, tiopt) with
  | (None, _) ->
      (* Currently not supported by Tezedge, needs Irmin *)
      assert false
  | (Some ii, _) -> I.retrieve_commit_info ii block_header

let check_protocol_commit_consistency {i = iiopt; t = tiopt; _}
    ~expected_context_hash ~given_protocol_hash ~author ~message ~timestamp
    ~test_chain_status ~predecessor_block_metadata_hash
    ~predecessor_ops_metadata_hash ~data_merkle_root ~parents_contexts =
  match (iiopt, tiopt) with
  | (None, _) ->
      (* Currently not supported by Tezedge, needs Irmin *)
      assert false
  | (Some ii, _) ->
      I.check_protocol_commit_consistency
        ii
        ~expected_context_hash
        ~given_protocol_hash
        ~author
        ~message
        ~timestamp
        ~test_chain_status
        ~predecessor_block_metadata_hash
        ~predecessor_ops_metadata_hash
        ~data_merkle_root
        ~parents_contexts

(* Context dumper *)

let dump_context {i = iiopt; t = tiopt; _} data ~fd =
  match (iiopt, tiopt) with
  | (None, _) ->
      (* Currently not supported by Tezedge, needs Irmin *)
      assert false
  | (Some ii, _) -> I.dump_context ii data ~fd

let restore_context {i = iiopt; t = tiopt; _} ~expected_context_hash
    ~nb_context_elements ~fd =
  match (iiopt, tiopt) with
  | (None, _) ->
      (* Currently not supported by Tezedge, needs Irmin *)
      assert false
  | (Some ii, _) ->
      I.restore_context ii ~expected_context_hash ~nb_context_elements ~fd

(* Irmin required *)

module Checks = I.Checks

(* No need to implement Tezedge version *)

(*
let tezedge_index i = i.t

let irmin_index i = i.i

let tezedge_context = function
  | Tezedge_only tc | Both (_, tc) ->
      Some tc
  | _ ->
      None

let irmin_context = function
  | Irmin_only ic | Both (ic, _) ->
      Some ic
  | _ ->
      None

let drop_irmin i =
  match i.t with
  | None ->
      Stdlib.failwith "You cannot drop Irmin context from Irmin only context"
  | Some _ ->
      {i with i = None}
*)

let block_applied {t = tiopt; _} hash level cycle_position =
  match tiopt with
  | Some ti -> Tcontext.block_applied ti hash level cycle_position
  | None -> ()



;
;        /!\ /!\ Do not modify this file /!\ /!\
;
; but the original template in `tezos-protocol-compiler`
;


(rule
 (targets environment.ml)
 (action
  (write-file %{targets}
              "module Name = struct let name = \"genesis-Ps6mwMrF\" end
include Tezos_protocol_environment.MakeV0(Name)()
module CamlinternalFormatBasics = struct include CamlinternalFormatBasics end
")))

(rule
 (targets registerer.ml)
 (deps
   data.ml
   services.ml
   main.mli main.ml
   (:src_dir TEZOS_PROTOCOL))
 (action
  (with-stdout-to %{targets}
                  (chdir %{workspace_root} (run %{bin:tezos-embedded-protocol-packer} "%{src_dir}" "genesis_Ps6mwMrF")))))

(rule
 (targets functor.ml)
 (deps
   data.ml
   services.ml
   main.mli main.ml
   (:src_dir TEZOS_PROTOCOL))
 (action (with-stdout-to %{targets}
                         (chdir %{workspace_root}
                                (run %{bin:tezos-protocol-compiler.tezos-protocol-packer} %{src_dir})))))

(rule
 (targets protocol.ml)
 (deps
   data.ml
   services.ml
   main.mli main.ml)
 (action
  (write-file %{targets}
    "module Environment = Tezos_protocol_environment_genesis_Ps6mwMrF.Environment
let hash = Tezos_crypto.Protocol_hash.of_b58check_exn \"Ps6mwMrF2ER2s51cp9yYpjDcuzQjsc2yAz8bQsRgdaRxw4Fk95H\"
let name = Environment.Name.name
include Tezos_raw_protocol_genesis_Ps6mwMrF
include Tezos_raw_protocol_genesis_Ps6mwMrF.Main
")))

(library
 (name tezos_protocol_environment_genesis_Ps6mwMrF)
 (public_name tezos-protocol-genesis-Ps6mwMrF.environment)
 (library_flags (:standard -linkall))
 (libraries tezos-protocol-environment)
 (modules Environment))

(library
 (name tezos_raw_protocol_genesis_Ps6mwMrF)
 (public_name tezos-protocol-genesis-Ps6mwMrF.raw)
 (libraries tezos_protocol_environment_genesis_Ps6mwMrF)
 (library_flags (:standard -linkall))
 (flags (:standard -nopervasives -nostdlib
                   -w +a-4-6-7-9-29-32-40..42-44-45-48
                   -warn-error +a
                   -open Tezos_protocol_environment_genesis_Ps6mwMrF__Environment
                   -open Pervasives
                   -open Error_monad))
 (modules
   Data
   Services
   Main))

(install
 (section lib)
 (package tezos-protocol-genesis-Ps6mwMrF)
 (files (TEZOS_PROTOCOL as raw/TEZOS_PROTOCOL)))

(library
 (name tezos_protocol_genesis_Ps6mwMrF)
 (public_name tezos-protocol-genesis-Ps6mwMrF)
 (libraries
      tezos-protocol-environment
      tezos-protocol-environment-sigs
      tezos_raw_protocol_genesis_Ps6mwMrF)
 (flags -w "+a-4-6-7-9-29-40..42-44-45-48"
        -warn-error "+a"
        -nopervasives)
 (modules Protocol))

(library
 (name tezos_protocol_genesis_Ps6mwMrF_functor)
 (public_name tezos-protocol-genesis-Ps6mwMrF.functor)
 (libraries
      tezos-protocol-environment
      tezos-protocol-environment-sigs
      tezos_raw_protocol_genesis_Ps6mwMrF)
 (flags -w "+a-4-6-7-9-29-40..42-44-45-48"
        -warn-error "+a"
        -nopervasives)
 (modules Functor))

(library
 (name tezos_embedded_protocol_genesis_Ps6mwMrF)
 (public_name tezos-embedded-protocol-genesis-Ps6mwMrF)
 (library_flags (:standard -linkall))
 (libraries tezos-protocol-genesis-Ps6mwMrF
            tezos-protocol-updater
            tezos-protocol-environment)
 (flags (:standard -w +a-4-6-7-9-29-32-40..42-44-45-48
                   -warn-error +a))
 (modules Registerer))

(rule
 (alias runtest_compile_protocol)
 (deps
   data.ml
   services.ml
   main.mli main.ml
  (:src_dir TEZOS_PROTOCOL))
 (action (run %{bin:tezos-protocol-compiler} -no-hash-check .)))

(rule
 (alias runtest_sandbox)
 (deps .tezos_protocol_genesis_Ps6mwMrF.objs/native/tezos_protocol_genesis_Ps6mwMrF.cmx)
 (action (progn)))

(rule
 (alias runtest)
 (package tezos-protocol-genesis-Ps6mwMrF)
 (deps (alias runtest_sandbox))
 (action (progn)))
